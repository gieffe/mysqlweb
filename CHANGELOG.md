# Changelog

## [Unreleased]

### Added
- Compatibility with python 3.13
- Black code style formatting
- code checked with Flake8

### Fixed
- Sistemando il codice per flake8 avevo introdotto un bug nell'export dei dati su excel


## [24.12] - 2024-12-29

### Added

#### Nella funzione *Content Viewer*

- Il programma registra sui dati di sessione dell'utente l'ultimo filtro eseguito su ogni tabella. In questo modo l'utente entrando nella funzione o selezionando la tabella si ritrova nello stesso stato dell'ultima accesso alla funzione/tabella.
- Aggiunta la visualizzazione del numero di righe nella tabella se il DB è MySQL.
- Aggiunta la possibilità di visualizzare il codice SQL che è stato o verrà eseguito in base ai filtri specificati.
- Il codice SQL viene visualizzato in un *info box* evidenziando la sintassi (*syntax coloring*)
- Dalla *info box* è possibile copiare sql nella *clipboard* utilzzando apposito pulsante
- Aggiunta la possibiltà di utilizzare diversi operatori nei filtri di ricerca:
    +  `=`
    +  `<>`
    +  `like`
    +  `<`
    +  `>`
    +  `<=`
    +  `>=`
    +  `IN`
    +  `NOT IN`
    +  `IS NULL`
    +  `IS NOT NULL`
    +  `BETWEEN`
    +  `NOT BETWEEN`
    +  `LIKE`
- e se il DB è MySQL anche:
    + `REGEXP`
    + `NOT REGEXP`
- In base all'operatore specificato nel filtro viene modificato il *placeholder* del campo di input in cui l'utente specifica il criterio di ricerca.

#### Nella funzione *User preferences* (funzione di gestione delle connessioni a DB)

- Aggiunta la possibilità di visualizzare i dati di sessione registrati per ogni DB presente della lista.
- Aggiunta la possibiltà di cancellare tutti i dati di sessione registrati per il DB. Questa funzionalità potrebbe risultare utile qualora i dati di sessione registrati, per qualche motivo, dovessoro mandare il programma in condizione di errore (quelli che mi sono venuti in mente, ho cercato di gestirli diversamente).

#### Nella funzione *Query*

- È possibile copiare il contenuto della *Query area* nella *clipboard* utilzzando l'apposito pulsante nella toolbar.

#### Nel comando *mysqlweb*

- Aggiunto la possibilità di visualizzare la versione del programma

```
% mysqlweb -v
version: 24.12
```

### Changed

- Sistemata la disposizione della *Content Toolbar* nella funzione *Content Viewer*
- La prima volta che si accede ad una tabella di un database nella funzione *Content Viewer* non vengono applicati filtri di ricerca (`select * from tabella limit 100`). E' possibile aggiunge filtri di ricerca cliccando sul pulsante *Filter*.
- Nella funzione *Content Viewer* è possibile eliminare i filtri di ricerca e rimuoverli dalla *Content Toolbar* utilizzando il pulsante *Reset Filter*.
- Nella funzione *Content Viewer* cliccando sul pulsante di *sblocco delle modifiche* all'interno della *Content Toolbar*, se c'è una riga selezionata si entra direttamente in modalità di editing della stessa.
- Nella funzione *User Preferences* il pulsante *Connect* manda alla funzione *Content Viewer* e non alla funzione di *Query*

### Fixed

- Nella funzione *Content Viewer* gestito eventuale errore nell'ingresso alla funzione nel caso il contenuto del filtro abbia degli errori o la tabella non sia più presente nel database.
- Nella funzione *Content Viewer* gestito eventuale errore causato dalle clausole dei filtri specificate male. Il programma mostra un'alert con il messaggio d'errore.
- Nella funzione *Content Viewer* nen veniva eseguito escape nei campi contenenti i valori dei filtri di ricerca.

