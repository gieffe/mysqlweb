import os
import sys
import signal
import socket
import pprint
import urllib.request, urllib.parse, urllib.error
import time

from twisted.internet import reactor, defer
from twisted.internet import threads
from twisted.web import resource, static, server, http

import config
import driver
import qlite
import tunnel


def get_header(request):
    d = {}
    header = request.getAllHeaders()
    for k in header:
        kk = str(k, "utf8")
        http_key = "HTTP_%s" % kk.upper().replace("-", "_")
        d[http_key] = header[k]
    d["REMOTE_ADDR"] = request.getClientAddress().host
    if d["REMOTE_ADDR"] == "localhost":
        d["REMOTE_ADDR"] = "127.0.0.1"
    return d


class MyRequestHandler(resource.Resource):

    def __init__(self, query=""):
        resource.Resource.__init__(self)
        self.query = query
        self.isLeaf = True

    def render(self, request):
        self.dthread = threads.deferToThread(self.myHandler, request)
        self.dthread.addCallback(self.send_result, request).addErrback(
            self.send_error, request
        )
        return server.NOT_DONE_YET

    def myHandler(self, request):
        pard = {}
        pard["ERROR"] = ""
        pard["html"] = ""
        pard["file_upload"] = []
        pard["header"] = "Content-type: text/html\n\n"
        request.setHeader("Content-Type", "text/html")

        pard.update(get_header(request))

        if request.prepath[0] != "":
            cookie_sid = request.getCookie(b"cookie_sid")
            pard["path_alias"] = request.prepath[0]
            # print('cookie_sid', cookie_sid)
            if cookie_sid:
                pard["sid"] = cookie_sid
                pard["module"] = "qq"
                query = request.path[len(request.prepath[0]) + 2 :]
                if query and query[-1] == b"/":
                    query = query[:-1]
                if query:
                    query = ensure_str(query)
                    pard["query"] = urllib.parse.unquote_plus(query)
                    pard["query_area"] = pard["query"]
                    pard["action"] = "run_query"

        form = request.args
        for k in list(form.keys()):
            if k not in pard:  # disable environment key overwrite
                if (k[0:12] == b"file_upload_") and (form[k].filename != ""):
                    pard["file_upload"].append(
                        {
                            "filename": ensure_str(form[k].filename),
                            "content": form.getvalue(k, ""),
                            "form_id": k,
                        }
                    )
                else:
                    pard[k] = form[k]
                    if isinstance(pard[k], list) and len(pard[k]) == 1:
                        pard[k] = ensure_str(pard[k][0])

        pard = k_to_str(pard)

        pard["HTTP_REFERER"] = pard.get("HTTP_REFERER", "")
        pard["REMOTE_ADDR"] = pard.get("REMOTE_ADDR", "")
        pard["HTTP_X_FORWARDED_FOR"] = pard.get("HTTP_X_FORWARDED_FOR", "")
        if pard["HTTP_X_FORWARDED_FOR"]:
            pard["REMOTE_IP_ADDR"] = pard["HTTP_X_FORWARDED_FOR"]
        else:
            pard["REMOTE_IP_ADDR"] = pard["REMOTE_ADDR"]

        driver_result = driver.driver(pard)
        # pprint.pprint(driver_result)
        if driver_result["header"] == "Content-type: text/html\n\n":
            pass
        else:
            for item in driver_result["header"]:
                request.setHeader(item[0], item[1])
        result = driver_result["html"]
        if isinstance(result, str):
            result = result.encode("utf8")
        return result

    def send_result(self, result, request):
        request.write(result)
        request.finish()

    def send_error(self, failure, request):
        request.setResponseCode(http.INTERNAL_SERVER_ERROR)
        print(failure.getTraceback())
        request.write("D'OH!")
        # request.write("<pre>Error fetching posts: %s</pre>" % failure.getTraceback())
        request.finish()


class ServeDir(resource.Resource):

    def __init__(self, dir_path, valid_extension):
        resource.Resource.__init__(self)

        self.dir_list = []
        ll = os.listdir(dir_path)
        for fn in ll:
            if fn[-3:].lower() in valid_extension:
                self.dir_list.append(fn)

        for fn in self.dir_list:
            self.putChild(
                fn.encode(), static.File(str.encode("%s/%s" % (dir_path, fn)))
            )


# class QueryDir(resource.Resource):
#
# 	def __init__(self):
# 		resource.Resource.__init__(self)
#
# 	def getChild(self, path, request):
# 		return MyRequestHandler(path)
#
# 	def render(self, request):
# 		request.redirect(request.path + '/')
# 		return ''


class OpenSearchDir(resource.Resource):

    def __init__(self, appserver):
        resource.Resource.__init__(self)
        self.appserver = appserver

    def getChild(self, path, request):
        return OpenSearchHandler(self.appserver)

    def render(self, request):
        request.redirect(request.path + "/")
        return ""


class OpenSearchHandler(resource.Resource):

    def __init__(self, appserver):
        resource.Resource.__init__(self)
        self.appserver = appserver

    def render(self, request):
        try:
            opensearch_file = request.prepath[1]
            alias, ext = opensearch_file.split(".")
            if ext != "xml":
                raise ValueError("Invalid Extension")
            xml = """<?xml version="1.0" encoding="UTF-8"?>
				<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
					<ShortName>MySQL Query Navigator: %s</ShortName>
					<Description>Query %s MySQL DataBase</Description>
					<Url type="text/html" method="GET" template="%s/%s/{searchTerms}" />
					<InputEncoding>UTF-8</InputEncoding>
				</OpenSearchDescription>
				""" % (
                alias,
                alias,
                self.appserver,
                alias,
            )
            xml = xml.replace("\t", "")
            request.setHeader("Content-Type", "text/xml")
            return xml.encode("utf8")
        except:
            request.setResponseCode(http.BAD_REQUEST)
            request.write(b"<pre>Bad Request</pre>")
            return ""


class MyResource(resource.Resource):

    def __init__(self, query=""):
        resource.Resource.__init__(self)

    def getChildWithDefault(self, path, request):
        # Tutti i path definiti vengono gestiti dagli handler
        # gli altri vengono gestiti da MyRequestHandler che avra'
        # settatto il flag isLeaf a true in modo da essere l'ultimo
        # valutato da getChildForRequest
        #
        # Una alternativa a questa soluzione era di sovrasrivere il
        # metodo getChildForRequest di Resource ...
        if path in self.children:
            return self.children[path]
        return MyRequestHandler(path)


def ensure_str(s, encoding="utf-8", errors="strict"):
    if type(s) is str:
        return s
    elif isinstance(s, bytes):
        return s.decode(encoding, errors)
    elif not isinstance(s, (str, bytes)):
        raise TypeError("not expecting type '%s'" % type(s))
    return s


def k_to_str(pard):
    spard = {}
    for k in pard:
        kk = ensure_str(k)
        value = pard[k]
        if isinstance(value, list) and k[0:12] != b"file_upload_":
            value = [ensure_str(x) for x in value]
        else:
            value = ensure_str(value)
        spard[kk] = value
    return spard


# -------------------------------------------------------------------------- #
# Deamon Handler
# -------------------------------------------------------------------------- #
# def session_detach():
# 	if os.getppid() == 1:
# 		pass
# 	else:
# 		signal.signal(signal.SIGTTOU, signal.SIG_IGN)
# 		signal.signal(signal.SIGTTIN, signal.SIG_IGN)
# 		signal.signal(signal.SIGTSTP, signal.SIG_IGN)
# 		if os.fork() != 0:
# 			sys.exit(0)
# 		os.setpgrp()
# 		signal.signal(signal.SIGHUP, signal.SIG_IGN)
# 		if os.fork() != 0:
# 			sys.exit(0)
# 	os.close(sys.stdin.fileno())
# 	os.close(sys.stdout.fileno())
# 	os.close(sys.stderr.fileno())
# 	os.umask(0)
#
#
# def do_stop(pard):
# 	is_death=do_status(pard)
# 	if is_death:
# 		print(is_death)
# 		return 0
# 	pid=open(pard['PID_FILE']).read()
# 	os.kill(int(pid), signal.SIGTERM)
# 	print('Stopping driver . .', end=' ')
# 	while not is_death:
# 		print('.', end=' ')
# 		os.kill(int(pid), signal.SIGTERM)
# 		time.sleep(0.5)
# 		is_death=do_status(pard)
# 	print('ok')
# 	return 0
#
#
# def do_status(pard):
# 	if not os.path.isfile(pard['PID_FILE']):
# 		return 'Pidfile not found `%(PID_FILE)s`' % pard
# 	pid = open(pard['PID_FILE'], 'r').read()
# 	try:
# 		os.kill(int(pid), 0)
# 	except OSError:
# 		return 'No process found but pidfile exists `%(PID_FILE)s`' % pard
# 	# is alive
# 	return 0
#
#
# def do_start(pard):
# 	is_death=do_status(pard)
# 	if not is_death:
# 		print('Driver already running')
# 		return 0
#
# 	print('Starting driver . . . ok')
# 	session_detach()
# 	open(pard['PID_FILE'], 'w').write(str(os.getpid()))
# 	sys.stdout = open(pard['APPLICATION_LOG_FILE'], 'a', 0)
# 	sys.stderr = open(pard['APPLICATION_LOG_FILE'], 'a', 0)
#
# 	def handle_stop(signum, stackframe):
# 		os.remove(pard['PID_FILE'])
# 		print('%s\tServer stopped %s\n' % (time.asctime(), '='*50))
# 		raise SystemExit
# 	signal.signal(signal.SIGTERM, handle_stop)
#
# 	## Configure and run Twisted Server
# 	root = MyResource() # Uso questa per poter sovrascivere il metodo getChildWithDefault
# 	root.putChild('', MyRequestHandler())
# 	root.putChild('img', ServeDir(pard['IMG_DIR'], ('jpg', 'gif', 'png')))
# 	root.putChild('icons', ServeDir(pard['ICON_DIR'], ('png',)))
# 	root.putChild('javascripts', ServeDir('%(HTML_DIR)s/javascripts' % pard, ('.js',)))
# 	buf = open('%(HTML_DIR)s/sqlweb.css' % pard, 'r').read()
# 	root.putChild('sqlweb.css', static.Data(buf, 'text/css'))
# 	root.putChild('OpenSearch', OpenSearchDir(pard['APPSERVER']))
# 	root.putChild('ColorList.css', static.File('%(HTML_DIR)s/ColorList.css' % pard))
# 	root.putChild('favicon.ico', static.File('%(HTML_DIR)s/favicon.ico' % pard))
# 	site = server.Site(root, logPath='%(LOG_DIR)s/access.log' % pard)
# 	reactor.listenTCP(pard['HTTP_PORT'], site, interface=pard['IP_ADDRESS'])
# 	reactor.run()


def do_run(pard):
    ## Configure and run Twisted Server
    root = (
        MyResource()
    )  # Uso questa per poter sovrascivere il metodo getChildWithDefault
    root.putChild(b"", MyRequestHandler())
    root.putChild(b"img", ServeDir(pard["IMG_DIR"], ("jpg", "gif", "png")))
    root.putChild(b"icons", ServeDir(pard["ICON_DIR"], ("png",)))
    root.putChild(b"javascripts", ServeDir("%(HTML_DIR)s/javascripts" % pard, (".js",)))
    buf = open("%(HTML_DIR)s/sqlweb.css" % pard, "r").read()
    root.putChild(b"sqlweb.css", static.Data(buf.encode(), "text/css"))
    root.putChild(b"OpenSearch", OpenSearchDir(pard["APPSERVER"]))
    root.putChild(b"ColorList.css", static.File("%(HTML_DIR)s/ColorList.css" % pard))
    root.putChild(b"favicon.ico", static.File("%(HTML_DIR)s/favicon.ico" % pard))
    site = server.Site(root, logPath="%(LOG_DIR)s/access.log" % pard)
    reactor.listenTCP(pard["HTTP_PORT"], site, interface=pard["IP_ADDRESS"])
    print(pard["IP_ADDRESS"], pard["HTTP_PORT"])
    pard["sqlite"] = qlite.DB(pard["SQLITE"])
    tunnel.close_all_open_tunnel(pard)
    reactor.run()
    tunnel.close_all_open_tunnel(pard)
    pard["sqlite"].close()
    print("Esco")


def do_test(pard):
    print("test!")


if __name__ == "__main__":
    # config.build_default_config()
    pard = config.global_parameters({})
    config.check_dir_tree(pard)
    config.check_db(pard)
    if len(sys.argv) == 2 and sys.argv[1] == "run":
        do_run(pard)
    elif len(sys.argv) == 2 and sys.argv[1] == "test":
        do_test(pard)
    else:
        print("usage: %s {run|test}" % sys.argv[0])
