import os
import shelve
import time
import random
import http.cookies
import json
import pprint

import db
import qq
import tools
import prefs
import tunnel


# ---------------------------------------------------------------------------
def load_user_auth(pard):
    pard.setdefault("user_data", {})
    pard.setdefault("alias", {})
    if not pard["user_data"] or not pard["alias"]:
        return False
    pard["mw_user"] = str(pard["user_data"]["user_id"])
    args = {"mw_user": pard["mw_user"], "alias": pard["alias"]}
    rec = prefs.get_by_alias(pard, args)
    if not rec:
        return False
    pard["USER"] = rec["user"]
    pard["PASSWORD"] = rec["passw"]
    pard["HOST"] = rec["host"]
    pard["DB"] = rec["db"]
    pard["ALIAS"] = rec["alias"]
    pard["COLOR"] = rec["color"]
    pard["CONN_TYPE"] = rec["conn_type"]
    pard["ENGINE"] = rec["engine"]
    pard["TUNNEL_ID"] = rec["tunnel_id"]

    ## Don't allow access to sqlite local DB if user is not administrator
    if pard["ENGINE"] == "sqlite" and pard["user_data"]["admin"] != "Y":
        return False

    return True


# ---------------------------------------------------------------------------
def js_create_cookie(pard):
    pard["ALIAS"] = pard.get("ALIAS", "")
    if pard["ALIAS"] == "":
        return ""
    c = http.cookies.SimpleCookie()
    c["cookie_sid"] = pard["sid"]
    c["cookie_sid"]["path"] = "/%(ALIAS)s" % pard
    tomorrow = time.time() + 86400
    expGMT = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime(tomorrow))
    c["cookie_sid"]["expires"] = expGMT
    # c['cookie_sid']['samesite'] = 'Strict'
    # print(c.js_output())
    return c.js_output()


def login(pard, rec=None):
    pard["login_div"] = render_login_div(pard, rec)
    pard["html"] = (
        """
	<html>
	<head>
		%(CSS)s
		%(javascript)s
	</head>
	<body>
	<form action="%(APPSERVER)s" id="login_form" method="post">
	<input type="hidden" name="module" value="auth">
	<input type="hidden" name="program" value="check_authentication">
	<input type="hidden" name="tmp_sid" value="%(tmp_sid)s">
	<table width="100%%" class="header">
		<tr>
			<td class="left">Welcome to MySQL <em>Query Navigator</em></td>
		</tr>
	</table>
	
	<table width="100%%">
	<tr>
		<td width="70%%" style="vertical-align: top; line-height: 170%%;">
			<h2>MySQLweb is a web application that run queries, create and edit data, and view and export results.</h2>
			
			<img src="/static/icons/database_gear.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>DataBase Structure</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Database Structure Browser, provides instant access to database schema and objects.
			<br /><br />
			
			<img src="/static/icons/text_list_bullets.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Connection Manager</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;The Database Connections Panel enables developers to create, organize, and manage database connections
			<br /><br />
			
			<img src="/static/icons/clock.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>History Panel</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement.
			<br /><br />

			<img src="/static/icons/lightning.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Run current query</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/static/icons/lightning_go.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Run selected query</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/static/icons/page_white_code.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Comment selection</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/static/icons/script_lightning.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Exec Procedures</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon)
			<br /><br />

			<img src="/static/img/save.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Save SQL</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Developers can save and easily reuse <img src="/static/icons/folder_page.png" border="0"> common Queries
			<br /><br />

			<img src="/static/icons/page_white_put.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Dump SQL</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Dumps results of a query to a file contains SQL insert statements.
                        These stataments can be used for backup or transfer data to another SQL server.
			<br /><br />

			<img src="/static/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Query Autocomplete</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Press "esc" when you are typing queries and enjoy with <em>Autocoplete-query</em> feature!
			<br /><br />
			
			<img src="/static/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Query Wizard</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Type <code>wsel tablename [,tablename 2, ...]</code> and <em>run query</em>: MySQLweb prepares for you select statement to query the specified tables.<br />
			&nbsp;&nbsp;Type <code>wins tablename</code> and <em>run query</em>: MySQLweb prepares for you insert statement.
			<br /><br />
			
			<img src="/static/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Silk Icons Search</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Type <code>silk icon-pattern</code> and <em>run query</em>: MySQLweb search <em>Silk</em> icon that matches the pattern specified.
			<br /><br />
									
		</td>
		<td width="30%%" style="vertical-align: top;">
		<div style="width: 100%%;" id="login_div">%(login_div)s</div>
		</td>
	</tr>
	</table>

	<table width="100%%" class="header">
		<tr>
			<td class="center">GF&copy;</td>
		</tr>
	</table>
	</form>
	</body>
	</html>
	"""
        % pard
    )

    return pard


def render_login_div(pard, rec=None):
    if not rec:
        rec = prefs.get_form(pard)

    pard.setdefault("login_error", "")
    pard["javascript"] = javascript(pard)
    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    if "path_alias" in pard and pard["path_alias"]:
        res = prefs.get_by_alias(
            pard, {"alias": pard["path_alias"], "mw_user": pard["mw_user"]}
        )
        if res:
            rec = res
    elif not pard["login_error"]:
        rec = load_last_connection(pard, rec)

    rec["login_error"] = pard["login_error"] if pard["login_error"] else "&nbsp;"

    ## Connection preferences dropdown
    pref_list = prefs.get_list(pard, {"mw_user": pard["mw_user"], "status": "active"})
    ll = [("%(alias)s" % x, "%(alias)s (%(engine)s)" % x) for x in pref_list]
    ll.insert(0, ("", "Load from saved connection"))
    rec["conn_drop_down"] = tools.make_pop(
        "alias", ll, rec["alias"], '''onChange="load_prefs(this.value);"'''
    )
    #'alias', ll, rec['alias'], '''onChange="location = '/' + this.value;"''')

    rec["pop_conn_type"] = tools.make_pop(
        "conn_type",
        [(x, x) for x in prefs.conn_type_list],
        rec["conn_type"],
        'onChange="change_conn_type();"',
    )

    rec["pop_engine"] = tools.make_pop(
        "engine", [(x, x) for x in prefs.engine_list], rec["engine"]
    )

    tunnel_list = tunnel.get_list(pard)
    ll = [
        (el["id"], "[%(name)s] Server:%(ssh_server)s - DB Host:%(db_host)s" % el)
        for el in tunnel_list
    ]
    ll.insert(0, (0, "&nbsp;"))
    if rec["conn_type"] == "Standard":
        disabled = "disabled"
    else:
        disabled = ""

    if not rec["tunnel_id"]:
        rec["tunnel_id"] = 0
    rec["tunnel_id"] = int(rec["tunnel_id"])
    rec["pop_tunnel"] = tools.make_pop(
        "tunnel_id",
        ll,
        rec["tunnel_id"],
        f'{disabled} onChange="change_tunnel_id(this.value);"',
    )

    rec["APPSERVER"] = pard["APPSERVER"]

    h = (
        """			
	<table width="100%%" id="login_table">
	<tr>
		<td colspan="2" style="font-weight: bold; color: #2a68c8; text-align: center;">
		DataBase Login
		<span style="float: right;">
			<a href="%(APPSERVER)s?module=prefs" title="Connection Preferences">
			<img src="/static/icons/application_view_list.png" border="0"></a>
		</span>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" align="center" style="padding: 8px;">
			%(conn_drop_down)s
		</td>
	</tr>

	<tr>
		<td><b>Type:</b></td>
		<td>%(pop_conn_type)s</td>
	</tr>

	<tr>
		<td><b>Engine:</b></td>
		<td>%(pop_engine)s</td>
	</tr>

	<tr>
		<td><b>Tunnel:</b></td>
		<td>%(pop_tunnel)s</td>
	</tr>
	
	<tr>
		<td><b>User:</b></td>
		<td><input type="text" name="user" id="user" value="%(user)s"></td>
	</tr>
	<tr>
		<td><b>Password:</b></td>
		<td><input type="password" name="passw" id="passw" value="%(passw)s"></td>
	</tr>
	<tr>
		<td><b>Host:</b></td>
		<td><input type="text" id="host" name="host" id="host" value="%(host)s"></td>
	</tr>
	<tr>
		<td><b>DB:</b></td>
		<td><input type="text" name="db" id="db" value="%(db)s"></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="login" value="Login" class="btn"></td>
	</tr>
	<tr>
		<td colspan="2" class="err">%(login_error)s</td>
	</tr>
	</table>
	"""
        % rec
    )
    return h


def javascript(pard):
    javascript = """
	<script>	
		
		let xhr;
				
		const change_conn_type = function () {
			let pop_tunnel_id = document.getElementById('tunnel_id');
			let pop_conn_type = document.getElementById('conn_type');
			if (pop_conn_type.value == 'SSH') {
				pop_tunnel_id.disabled = false;
				change_tunnel_id(pop_tunnel_id.value)
				}
			else {
				pop_tunnel_id.disabled = true;
				pop_tunnel_id.value = 0;
				change_tunnel_id(0);
				}
		};
		
		const change_tunnel_id = function (tunnel_id) {
			let host = document.getElementById('host');
			if (tunnel_id == 0) {
				host.readOnly = false;
				host.value = '';
				return false
			}
			else {
				host.readOnly = true;
			}
			xhr = new XMLHttpRequest();
			if (!xhr) {
				alert('Giving up :( Cannot create an XMLHTTP instance');
			  	return false;
			}
			xhr.onload = function () {
				let host = document.getElementById('host');
				host.value = this.responseText;
			};
	
			let query = []
			query.push(encodeURIComponent('module') + '=' + encodeURIComponent('prefs'));
			query.push(encodeURIComponent('action') + '=' + encodeURIComponent('get_tunnel_host'));
			query.push(encodeURIComponent('tunnel_id') + '=' + encodeURIComponent(tunnel_id));
			let qq = query.join('&');
		
			let url = '/?' + qq;
			xhr.open('GET', url, true);
			xhr.send();

		};


		const load_prefs = function (alias) {
			event.preventDefault();
			xhr = new XMLHttpRequest();
			if (!xhr) {
				alert('Giving up :( Cannot create an XMLHTTP instance');
			  	return false;
			}
			xhr.onload = function () {
				let result = JSON.parse(this.responseText);
				if (result['alias'] !== undefined) {
					//console.log(result);
					document.getElementById('conn_type').value = result['conn_type'];
					change_conn_type();
					document.getElementById('engine').value = result['engine'];
					document.getElementById('tunnel_id').value = result['tunnel_id'];
					document.getElementById('host').value = result['host'];
					document.getElementById('user').value = result['user'];
					document.getElementById('passw').value = result['passw'];
					document.getElementById('db').value = result['db'];
				    };
			};
			xhr.overrideMimeType("application/json");
			let query = []
			query.push(encodeURIComponent('module') + '=' + encodeURIComponent('prefs'));
			query.push(encodeURIComponent('action') + '=' + encodeURIComponent('load_prefs'));
			query.push(encodeURIComponent('alias') + '=' + encodeURIComponent(alias));
			let qq = query.join('&');
		
			let url = '/?' + qq;
			xhr.open('GET', url, true);
			xhr.send();

		};
		
		
	(function() {

		const live = function(event_name, selector, callback, allow_default) {
			document.addEventListener(event_name, function(event){
				let element = event.target.closest(selector);
				if (element) {
					if(!allow_default) event.preventDefault();
					callback(element, event);
				}
			});
		};
		
		const reset_alias = function (el, ev) {
			if (el.name !== 'alias') {
				document.getElementById('alias').value = '';
			}
		};
		
		live('change', '#login_div input', reset_alias, true);
		live('change', '#login_div select', reset_alias, true);

	})();
	</script>
	"""
    return javascript


def check_authentication(pard):
    pard["CONN_TYPE"] = pard["conn_type"]
    pard["ENGINE"] = pard["engine"]
    pard["HOST"] = pard["host"]
    pard["USER"] = pard["user"]
    pard["PASSWORD"] = pard["passw"]
    pard["DB"] = pard["db"]
    pard["ALIAS"] = pard["alias"]
    pard["TUNNEL_ID"] = pard.get("tunnel_id", "")
    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    pard["COLOR"] = pard["DEFAULT_COLOR"]
    if pard["ALIAS"]:
        rec_prefs = prefs.get_by_alias(
            pard, {"alias": pard["alias"], "mw_user": pard["mw_user"]}
        )
        if rec_prefs:
            pard["COLOR"] = rec_prefs["color"]
        else:
            pard["ALIAS"] = ""
    try:
        conn = db.db_access.connect(pard)
        conn.close()
        flag = "OK"
        sid_file = sid_get(pard)
        sid_file["sid_user"] = pard["USER"]
        sid_file["sid_password"] = pard["PASSWORD"]
        sid_file["sid_host"] = pard["HOST"]
        sid_file["sid_db"] = pard["DB"]
        sid_file["sid_alias"] = pard["ALIAS"]
        sid_file["sid_color"] = pard["COLOR"]
        sid_file["sid_conn_type"] = pard["CONN_TYPE"]
        sid_file["sid_engine"] = pard["ENGINE"]
        sid_file["tunnel_id"] = pard["TUNNEL_ID"]
        sid_file["sid_time"] = time.time()
        sid_file["sid_buffer"] = {}
        try:
            result = sid_set(pard, sid_file)
            pard = load_sid_data(pard)
            flag = "OK"
        except:
            pard["tmp_sid"] = calc_sid(pard)
            flag = "STOP"
        set_last_conn(pard)

    except:
        pard["login_error"] = tools.get_last_exception()
        flag = "STOP"
    if flag == "OK":
        pard = qq.main(pard)
    else:
        pard = login(pard)
    return pard


def calc_sid(pard):
    sid = repr(time.time()).replace(".", "") + str(random.randint(0, 9999))
    return sid


def load_sid_data(pard):
    sid_file = sid_get(pard)
    try:
        pard["USER"] = sid_file["sid_user"]
        pard["PASSWORD"] = sid_file["sid_password"]
        pard["HOST"] = sid_file["sid_host"]
        pard["DB"] = sid_file["sid_db"]
        pard["ALIAS"] = sid_file["sid_alias"]
        pard["COLOR"] = sid_file["sid_color"]
        pard["ENGINE"] = sid_file["sid_engine"]
        pard["TUNNEL_ID"] = sid_file["tunnel_id"]
        pard["CONN_TYPE"] = sid_file["sid_conn_type"]
        pard["sid_buffer"] = sid_file["sid_buffer"]
    except:
        pass
    return pard


def check_sid(pard):
    try:
        if pard["HTTP_X_FORWARDED_FOR"]:
            ip = pard["HTTP_X_FORWARDED_FOR"].split(",")[0].strip()
        else:
            ip = pard["REMOTE_ADDR"]
        if ip not in pard["AUTH_IP"]:
            pard["flag_sid"] = "KO"
            return pard
    except:
        pard["flag_sid"] = "KO"
        return pard

    timeout = 1
    start = time.time()
    while 1:
        stop = time.time()
        if stop - start > timeout:
            break
        try:
            if "tmp_sid" in pard:
                pard["sid"] = pard["tmp_sid"]
                flag_sid = "OK"
            elif "sid" in pard:
                sid_file = sid_get(pard)
                if (time.time() - sid_file["sid_time"]) < pard["SID_TIMEOUT"]:
                    sid_file["sid_time"] = time.time()
                    flag_sid = "OK"
                else:
                    flag_sid = "NEW"
                try:
                    sid_file["menu_selected"] = pard["menu_id"]
                except:
                    pass
                result = sid_set(pard, sid_file)
            else:
                flag_sid = "NEW"
            pard["flag_sid"] = flag_sid
            break
        except:
            pard["flag_sid"] = "NEW"
    return pard


def sid_get(pard):
    d = {}
    sid_file = shelve.open(pard["SID_DIR"] + "/" + pard["sid"] + ".sid")
    for key in list(sid_file.keys()):
        d[key] = sid_file[key]
    sid_file.close()
    return d


def sid_set(pard, dict):
    sid_file = shelve.open(pard["SID_DIR"] + "/" + pard["sid"] + ".sid")
    for key in list(dict.keys()):
        sid_file[key] = dict[key]
    sid_file.close()


def load_last_connection(pard, rec):
    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    fname = "%(DATA_DIR)s/%(mw_user)s/last_conn.json" % pard
    dirname = "%(DATA_DIR)s/%(mw_user)s" % pard
    try:
        fp = open(fname, "r")
        buf = fp.read()
        rec = json.loads(buf)
        fp.close()
    except FileNotFoundError:
        if not os.path.isdir(dirname):
            os.mkdir(dirname, 0o770)
    return rec


def set_last_conn(pard):
    rec = {}
    rec["alias"] = pard["ALIAS"]
    rec["conn_type"] = pard["CONN_TYPE"]
    rec["engine"] = pard["ENGINE"]
    rec["tunnel_id"] = pard["TUNNEL_ID"]
    rec["user"] = pard["USER"]
    rec["passw"] = pard["PASSWORD"]
    rec["host"] = pard["HOST"]
    rec["db"] = pard["DB"]
    rec["color"] = pard["COLOR"]
    buf = json.dumps(rec)

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    fname = "%(DATA_DIR)s/%(mw_user)s/last_conn.json" % pard
    open(fname, "w").write(buf)

    ## Save shortcut to connection preferences
    # if not rec['alias']:
    res = prefs.get_by_value(pard, rec)
    if not res:
        rec["alias"] = ""
        prefs.insert(pard, rec)


def error_manager(pard):
    if pard["ERROR"] == "MISSING_SID":
        pard["error_message"] = "<b>Page not found</b>"
    else:
        pard["error_message"] = "<b>Error.</b>"
    errtext = pard["ERROR"].split("\n")[-2]
    errtext = errtext.replace("<B>", "")
    errtext = errtext.replace("</B>", "")
    pard["errtext"] = errtext
    pard["html"] = (
        """
		<html>
		<head>
		%(CSS)s
		<title>%(TITLE)s - ERROR</title>
		</head>
		<center>
		<body>	
		<!--
		%(ERROR)s
		-->
		%(error_message)s
		<br>
		<br>
		<b>%(errtext)s</b>
		</center>
		</body>
		</html>
	"""
        % pard
    )
    return pard


def build_not_authorized(pard):
    if pard["flag_sid"] == "KO":
        pard["msg"] = "Not authorized!"
    else:
        pard["msg"] = "Page not found."
    html = """
	<html>
	<head>
	<title>%(TITLE)s</title>
	</head>
	<body>
	<center>
	<h3>%(msg)s</h3>
	</center>
	</body>
	</html>
	"""
    pard["html"] = html % pard
    return pard


if __name__ == "__main__":
    import config

    pard = config.global_parameters(pard={})
# 	pard['module'] = 'admin'
# 	pard['program'] = 'utontiii'
# 	pard['menu_operation_tab'] = get_operation_recursive(pard)
# 	tools.dump(pard['menu_operation_tab'])
# 	t = get_menu_bin(pard)
# 	tools.dump(t)
# 	pard['id_utente'] = 'giames'
# 	load_user_data(pard)
