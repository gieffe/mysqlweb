# [MySQLweb]
MySQLweb is a web application that allows you to view, search, edit, import and export data from an SQL DataBase.
Created and maintained by [Gianfranco Messori].

### Install
```
python3 -m pip install git+https://bitbucket.org/gieffe/mysqlweb.git
```

#### Dependencies
MySQLweb runs on Python 3.x and should run on any Unix-like platform.

### Usage 
1. Run MySQLweb
```
mysqlweb
```
2. Point your browser to http://localhost:8088/
3. `ctr-c` to stop


### Configuration

#### Authentication
The current version of MySQLweb is multi-user and requires authentication (unless you use LOCAL MODE).
On the first run, an administrator user `admin@mysqlweb.dev` or user specified in DEFAULT_USER parameter (See [Site Configuration](#SiteConfiguration)) will be created with a temporary password (see message on console on the first run).

#### LOCAL MODE
By default, mysqlweb runs in local mode, which means that the application can only be accessed from the machine where mysqlweb is hosted.
In local mode authentication is not required, but a DEFAULT_USER is still created and used to store preferences.  
See [Site Configuration](#SiteConfiguration)


<a name="SiteConfiguration"></a>
#### Site Configuration ####
*mysqlweb* starts with default configuration.

Configuration files, data directory and log directory file are stored in the `~/.mysqlweb` directory.  
If you want to change the default directory, you must specify it in the `MYSQLWEB_DIR` environment variable.  
Example:
```
export MYSQLWEB_DIR="~/devel/mysqlweb"
```

To customize configuration you have to create a `site_config.yaml` file
in the default directory.

This is the default configuration:

```
HTTP_PORT: 8088,
MAX_ROW: 100,
COOKIE_EXPIRES_DAYS: 10,
DEFAULT_USER: 'admin@mysqlweb.dev',
LOCAL_MODE: True,
```

### Upgrade MySQLweb
1. kill or stop (`ctrl-c`) the running mysqlweb program
2. Upgrade MySQLweb
```
python3 -m pip install --upgrade git+https://bitbucket.org/gieffe/mysqlweb.git
```

### Install on MacOS (Homebrew)

##### 1. Install [Homebrew]

##### 2. Install python3
```
brew install python3
```

##### 3. Upgrade pip3
```
python3 -m pip install --upgrade pip
```

##### 4. Install MySQLweb (including requirements)
```
python3 -m pip install git+https://bitbucket.org/gieffe/mysqlweb.git
```

### Overview: What is MySQLweb?
MySQLweb is a web application that run queries, create and edit data, view and export results.  
Feautures:

#### DataBase Structure  
  Database Structure Browser, provides instant access to database schema and objects. 

#### Connection Manager  
  The Database Connections Panel enables developers to create, organize, and manage database connections 

#### History Panel  
  The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement. 

#### Exec Procedures  
  Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon) 

#### Save SQL  
  Developers can save and easily reuse  common Queries 

#### Dump SQL
  *Dump SQL* dumps results of a query to a file contains SQL insert statements.
  These stataments can be used for backup or transfer data to another SQL server.

#### Query Autocomplete  
  Press "esc" when you are typing queries and enjoy with Autocoplete-query feature! 

#### Query Wizard  
  Type wsel tablename [,tablename 2, ...] and run query: MySQLweb prepare for you select statement to query the specified tables.
  Type wins tablename and run query: MySQLweb prepares for you insert statement. 

#### Silk Icons Search  
  Type silk icon-pattern and run query: MySQLweb search Silk icon that matches the pattern specified. 

### Keyboard Shortcuts
- Run current query:  `cmd+enter` or `ctrl+enter`
- Comment selection: `cmd+k` or `ctrl+k`
- Completion: `esc`

### Connect to MySQL database using an SSH Tunnel
  From the database connections panel you can access a new feature that allows you to configure tunnels to connect to MySQL over the ssh protocol.
  This function has an inline help explaining how to set the different parameters.


You can also continue to setup an SSH tunnel using command like this from Terminal:

	$ ssh -l youruser yourhost.com -p 22 -N -f -C -L 1234:yourdbserver.com:3306



### LICENSE
[MIT]

[Gianfranco Messori]: https://bitbucket.org/gieffe
[MySQLweb]: https://bitbucket.org/gieffe/mysqlweb
[mysqlclient]: https://pypi.org/project/mysqlclient/
[MIT]: http://opensource.org/licenses/mit-license.php
[Homebrew]: https://brew.sh
