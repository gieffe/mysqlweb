from . import home
from . import users
from . import tools


def main(pard):
    pard["action"] = pard.get("action", "start")
    pard["page_header"] = pard["TITLE"] + " - Users"
    pard["javascript"] = javascript(pard)
    pard["sidebar"] = ""
    pard["main_body"] = ""
    pard["msg"] = ""
    pard["error_msg"] = ""

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])

    pard["userbar"] = home.render_userbar(pard)
    # --------------------------- start ----------------------------
    if pard["action"] == "start":
        result = users.get_list(pard)
        pard["main_body"] = render_list(pard, result)

    # --------------------------- insert ----------------------------
    elif pard["action"] == "insert":
        rec = get_form_new(pard)
        pard["main_body"] = render_new(pard, rec)

    # --------------------------- save_insert ----------------------------
    elif pard["action"] in ("save_insert",):
        rec = get_form_new(pard)
        rec["password_non_cifrata"] = rec["password"]
        try:
            users.insert(pard, rec)
            pard["msg"] = render_msg(
                pard,
                "%(email)s - new user created successfully with temporary password `%(password_non_cifrata)s`"  # noqa: E501
                % rec,
                "msg",
            )
            result = users.get_list(pard)
            pard["main_body"] = render_list(pard, result)
        except users.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_new(pard, rec)

    # --------------------------- edit ----------------------------
    elif pard["action"] == "edit":
        rec = users.get_by_id(pard, {"user_id": pard["user_id"]})
        pard["main_body"] = render_form(pard, rec)

    # --------------------------- save_update ----------------------------
    elif pard["action"] in ("save_update",):
        rec = get_form(pard)
        try:
            users.update(pard, rec)
            pard["msg"] = render_msg(
                pard, "%(email)s - preferences saved successfully" % rec, "msg"
            )
            result = users.get_list(pard)
            pard["main_body"] = render_list(pard, result)
        except users.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_form(pard, rec)

    # --------------------------- reset ----------------------------
    elif pard["action"] == "reset":
        rec = get_form(pard)
        try:
            new_password = users.reset_password(pard, rec)
            pard["msg"] = render_msg(
                pard,
                f"{rec['email']} - password reset with temporary value `{new_password}`",  # noqa: E501
                "msg",
            )
            pard["main_body"] = render_form(pard, rec)
        except users.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_form(pard, rec)

    # --------------------------- delete ----------------------------
    elif pard["action"] == "delete":
        rec = users.get_by_id(pard, {"user_id": pard["user_id"]})
        if not rec:
            pard["msg"] = render_msg(pard, "users not found", "err")
        else:
            users.delete(pard, {"user_id": rec["user_id"], "mw_user": pard["mw_user"]})
            pard["msg"] = render_msg(
                pard, "%(email)s - deleted successfully" % rec, "msg"
            )
        result = users.get_list(pard)
        pard["main_body"] = render_list(pard, result)

    else:
        pard["msg"] = 'Function not available. ("%(action)s")' % pard
        pard["main_body"] = (
            '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'  # noqa: E501
        )

    pard["html"] = render_page(pard)
    return pard


def render_form(pard, rec):
    h = []
    h.append(
        '<input type="hidden" name="user_id" id="user_id" value="%(user_id)s">' % rec
    )
    h.append(
        '<input type="hidden" name="user_sid" id="user_sid" value="%(sid)s">' % rec
    )

    h.append('<table width="100%">')
    h.append("<tr>")
    h.append('<th colspan="2">Edit User</th>' % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td width="150px"><b>Email</b></td>')
    h.append(
        '<td><input type="text" name="email" value="%(email)s" style="width: 98%%;">'
        % rec
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Admin User?</b></td>')
    h.append("<td>")
    if rec["admin"] == "Y":
        rec["yes"] = "checked"
        rec["no"] = ""
    else:
        rec["yes"] = ""
        rec["no"] = "checked"
    h.append('<input type="radio" id="admin" name="admin" value="Y" %(yes)s>' % rec)
    h.append('<label for="admin_user">Yes</label>&nbsp;&nbsp;')
    h.append('<input type="radio" id="admin" name="admin" value="N" %(no)s>' % rec)
    h.append('<label for="normal_user">No</label>&nbsp;&nbsp;')
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Sid</b></td>')
    h.append("<td>%(sid)s" % rec)
    h.append("</td>")
    h.append("</tr>")

    ## Bottom Action
    h.append('<tr><td colspan="2"></td></tr>')
    h.append("<tr>")

    ## Delete and Reset Button
    h.append('<td colspan="2">')
    h.append(
        """<input type="button" name="submit_form" value="Delete" class="btn" onclick="confirm_action('delete');">"""  # noqa: E501
    )
    h.append("&nbsp" * 3)
    h.append(
        """<input type="button" name="submit_form" value="Reset password" class="btn" onclick="confirm_action('reset');">"""  # noqa: E501
    )

    ## Save Button
    h.append('<span style="float: right;">')
    h.append(
        """<input type="button" name="submit_form" value="Cancel" class="btn" onclick="action.value='start'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
    )
    h.append(
        """<input type="submit" name="submit_form" value="Save" class="btn" onclick="action.value='save_update'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
        % rec
    )
    h.append("</span>")

    h.append("</td>")

    h.append("</tr>")
    ## -------------

    h.append("</table>")
    return "\n".join(h)


def render_new(pard, rec):
    h = []
    h.append(
        '<input type="hidden" name="password" id="password" value="%(password)s">' % rec
    )
    h.append(
        '<input type="hidden" name="retype_password" id="retype_password" value="%(retype_password)s">'  # noqa: E501
        % rec
    )

    h.append('<table width="100%">')
    h.append("<tr>")
    h.append('<th colspan="2">New User</th>' % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td width="150px"><b>Email</b></td>')
    h.append(
        '<td><input type="text" name="email" value="%(email)s" style="width: 98%%;">'
        % rec
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Admin User?</b></td>')
    h.append("<td>")
    if rec["admin"] == "Y":
        rec["yes"] = "checked"
        rec["no"] = ""
    else:
        rec["yes"] = ""
        rec["no"] = "checked"
    h.append('<input type="radio" id="admin" name="admin" value="Y" %(yes)s>' % rec)
    h.append('<label for="admin_user">Yes</label>&nbsp;&nbsp;')
    h.append('<input type="radio" id="admin" name="admin" value="N" %(no)s>' % rec)
    h.append('<label for="normal_user">No</label>&nbsp;&nbsp;')
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Password</b></td>')
    h.append("<td>%(password)s" % rec)
    h.append("</td>")
    h.append("</tr>")

    ## Bottom Action
    h.append('<tr><td colspan="2"></td></tr>')
    h.append("<tr>")

    h.append("""<td>&nbsp;</td>""")

    ## Save Button
    h.append('<td align="right">')
    h.append(
        """<input type="button" name="submit_form" value="Cancel" class="btn" onclick="action.value='start'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
    )
    h.append(
        """<input type="submit" name="submit_form" value="Save" class="btn" onclick="action.value='save_insert'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
        % rec
    )
    h.append("</td>")

    h.append("</tr>")
    ## -------------

    h.append("</table>")
    return "\n".join(h)


def render_list(pard, result):
    h = []
    h.append('<table width="100%">')
    h.append("<tr>")
    h.append('<td align="right" colspan="4">')
    h.append('<span style="float: left;">')
    h.append(
        """<input type="button" value="Exit" class="btn" onclick="location.href='/'">"""
    )
    h.append("&nbsp;" * 3)
    h.append("</span>")

    h.append(
        """<b><input type="button" name="submit_form" value="New" class="btn" onclick="action.value='insert'; form.submit();"></b>"""  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append("<th>&nbsp;</th>")
    h.append("<th>Email</th>")
    h.append("<th>Admin</th>")
    h.append("<th>Sid</th>")
    h.append("</tr>")

    for rec in result:
        h.append("<tr>")

        rec[
            "link_edit"
        ] = f"""
            <a href="{pard['APPSERVER']}users?action=edit&user_id={rec['user_id']}"
            title="Edit">
            <img src="/static/icons/application_form_edit.png" border="0"></a>
            """

        h.append('<td align="center">%(link_edit)s</td>' % rec)
        h.append("<td>%(email)s</td>" % rec)
        h.append("<td>%(admin)s</td>" % rec)
        h.append("<td>%(sid)s</td>" % rec)
        h.append("</tr>")

    h.append("</table>")
    return "\n".join(h)


def get_form(pard):
    rec = {}
    rec["user_id"] = pard.get("user_id", "")
    rec["email"] = pard.get("email", "")
    rec["admin"] = pard.get("admin", "N")
    rec["sid"] = pard.get("user_sid", "")
    return rec


def get_form_new(pard):
    rec = {}
    rec["email"] = pard.get("email", "")
    rec["admin"] = pard.get("admin", "N")
    rec["password"] = pard.get("password", tools.inventa_nome())
    rec["retype_password"] = pard.get("retype_password", rec["password"])
    rec["sid"] = "change_password"
    return rec


def render_msg(pard, msg, msg_class="err"):
    if isinstance(msg, str):
        msg = [msg]
    h = []
    for s in msg:
        h.append(f'<p class="{msg_class}">{s}</p>')
    return "\n".join(h)


def javascript(pard):
    javascript = """
    <script>

        let xhr;

        const confirm_action = function (action) {
            if (confirm('Confirm ' + action + '?')) {
                the_form = document.getElementById('theForm');
                the_action = document.getElementById('action');
                the_action.value = action;
                the_form.submit();
                }
        };

    </script>
    """
    return javascript


def render_page(pard):
    html = (
        """
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="author" content="gianfranco" />
    <title>%(TITLE)s</title>

%(CSS)s
<link rel="icon" href="/static/favicon.ico">
</head>
<body>

<!-- header -->
<div id="userbar">%(userbar)s</div>
<div id="header">%(page_header)s</div>

<!-- Sidebar -->
<div id="sidebar" style="display: none;">
%(sidebar)s
</div>

<!-- Main Body -->
<div id="main_body" align="center">
<form action="%(APPSERVER)susers" method="post" name="theForm" id="theForm"
      autocomplete="off" spellcheck="false" enctype="multipart/form-data">
<!-- enctype="multipart/form-data" -->
<input type="hidden" id="action" name="action" value="%(action)s">
<input type="hidden" id="module" name="module" value="%(module)s">
%(msg)s
%(main_body)s
</form>
</div>

</body>
%(javascript)s
</html>
    """
        % pard
    )
    return html
