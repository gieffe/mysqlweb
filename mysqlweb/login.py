import urllib

from . import users
from . import tools


def main(pard):
    pard["action"] = pard.get("action", "start")
    pard["page_header"] = pard["TITLE"]
    pard["javascript"] = javascript(pard)
    pard["sidebar"] = ""
    pard["main_body"] = ""
    pard["msg"] = ""
    pard["error_msg"] = ""

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])

    # --------------------------- local_mode ----------------------------
    pard.setdefault("REMOTE_ADDR", "")
    pard.setdefault("LOCAL_MODE", False)

    if pard["LOCAL_MODE"] and pard["action"] in ("start", "login"):
        if pard["REMOTE_ADDR"] != "127.0.0.1":
            pard["redirect"] = "/not_authorized"
            return pard
        else:
            pard.setdefault("continue", "/")
            try:
                pard["sid"] = users.local_login(pard, {"email": pard["DEFAULT_USER"]})
                user_data = users.get_user(pard, pard["sid"])
                pard["mw_user"] = str(user_data["user_id"])
                tools.check_dir("%(DATA_DIR)s/%(mw_user)s" % pard)
                pard["set_cookie"] = {"sid_user": pard["sid"]}
                pard["set_cookie_expires_days"] = int(
                    pard.get("COOKIE_EXPIRES_DAYS", 10)
                )
                pard["redirect"] = urllib.parse.unquote_plus(pard["continue"])
                return pard
            except users.Error as err:
                pard["msg"] = render_msg(pard, str(err), "err")
                pard["main_body"] = (
                    """<input type="button" name="btn_continue" value="Continue" class="btn" onclick="location.href='/';">"""  # noqa: E501
                )

    # --------------------------- start ----------------------------
    elif pard["action"] in ("start", "login"):
        pard["main_body"] = render_login(pard)

    elif pard["action"] == "authenticate":
        pard.setdefault("continue", "/")
        args = {}
        args["email"] = pard["email"]
        args["password"] = pard["password"]
        try:
            pard["sid"] = users.login(pard, args)
            user_data = users.get_user(pard, pard["sid"])
            pard["mw_user"] = str(user_data["user_id"])
            tools.check_dir("%(DATA_DIR)s/%(mw_user)s" % pard)
            pard["set_cookie"] = {"sid_user": pard["sid"]}
            pard["set_cookie_expires_days"] = int(pard.get("COOKIE_EXPIRES_DAYS", 10))
            pard["redirect"] = urllib.parse.unquote_plus(pard["continue"])
            return pard
        except users.ChangePassword as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_change_password(pard)
            # pard['redirect'] = '/login?action=change_password&email=%(email)s' % args
        except users.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_login(pard)

    elif pard["action"] == "not_authorized":
        pard["redirect"] = "/not_authorized"
        return pard

    elif pard["action"] == "logout":
        pard.setdefault("user_data", {})
        args = {}
        args["user_id"] = pard["user_data"].get("user_id", "")
        users.logout(pard, args)
        pard["delete_cookie"] = ["sid_user"]
        pard["redirect"] = "/"
        return pard

    elif pard["action"] == "change_password":
        args = {}
        args["email"] = pard["email"]
        args["password"] = pard["password"]
        args["new_password"] = pard["new_password"]
        args["retype_password"] = pard["retype_password"]
        try:
            sid = users.change_password(pard, args)
            pard["set_cookie"] = {"sid_user": sid}
            pard["set_cookie_expires_days"] = int(pard.get("COOKIE_EXPIRES_DAYS", 10))
            pard["msg"] = render_msg(pard, "Password Changed Succesfully", "msg")
            pard["main_body"] = (
                """<input type="button" name="btn_continue" value="Continue" class="btn" onclick="location.href='/';">"""  # noqa: E501
            )
        except users.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_change_password(pard)

    else:
        pard["msg"] = 'Function not available. ("%(action)s")' % pard
        pard["main_body"] = (
            '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'  # noqa: E501
        )

    pard["html"] = render_page(pard)
    return pard


def render_login(pard):
    h = []
    pard.setdefault("continue", "/")
    pard.setdefault("email", "")
    pard.setdefault("password", "")

    h.append(
        '<input type="hidden" name="continue" id="continue" value="%(continue)s">'
        % pard
    )

    h.append('<table id="login_table" width="50%">')
    h.append("<tr>")
    h.append('<th class="titolo">MySQLweb Login</th>')
    h.append("</tr>")

    h.append(
        """
        <tr>
          <td>
            <b>Email</b><br>
            <input type="text" name="email" value="%(email)s">
          </td>
        </tr>
        """
        % pard
    )

    h.append(
        """
        <tr>
          <td>
            <b>Password</b><br>
            <input type="password" name="password" value="%(password)s">
          </td>
        </tr>
        """
        % pard
    )

    ## Bottom Action
    h.append("<tr><td></td></tr>")

    h.append("<tr>")
    h.append('<td align="center">')
    h.append(
        """<input type="submit" name="submit_form" value="Login" class="btn" style="width: 98%;" onclick="action.value='authenticate'; form.submit();">"""  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")
    ## -------------

    h.append("</table>")
    return "\n".join(h)


def render_change_password(pard):
    h = []
    pard.setdefault("email", "")
    pard.setdefault("password", "")
    pard.setdefault("new_password", "")
    pard.setdefault("retype_password", "")

    h.append('<table id="login_table" width="50%">')
    h.append("<tr>")
    h.append('<th class="titolo">Change Password</th>')
    h.append("</tr>")

    h.append(
        """
        <tr>
          <td>
            <b>Email</b><br>
            <input type="hidden" name="email" value="%(email)s">
            %(email)s
          </td>
        </tr>
        """
        % pard
    )

    h.append(
        """
        <tr>
          <td>
            <b>Old Password</b><br>
            <input type="password" name="password" value="%(password)s">
          </td>
        </tr>
        """
        % pard
    )

    h.append(
        """
        <tr>
          <td>
            <b>New Password</b><br>
            <input type="password" name="new_password" value="%(new_password)s">
          </td>
        </tr>
        """
        % pard
    )

    h.append(
        """
        <tr>
          <td>
            <b>Verify Password</b><br>
            <input type="password" name="retype_password" value="%(retype_password)s">
          </td>
        </tr>
        """
        % pard
    )

    ## Bottom Action
    h.append("<tr><td></td></tr>")

    h.append("<tr>")
    h.append('<td align="center">')
    h.append(
        """<input type="submit" name="submit_form" value="Change Password" class="btn" style="width: 98%;" onclick="action.value='change_password'; form.submit();">"""  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")
    ## -------------

    h.append("</table>")
    return "\n".join(h)


def javascript(pard):
    javascript = """
    <script>

        let xhr;

        const confirm_action = function (action) {
            if (confirm('Confirm ' + action + '?')) {
                the_form = document.getElementById('theForm');
                the_action = document.getElementById('action');
                the_action.value = action;
                the_form.submit();
                }
        };

    </script>
    """
    return javascript


def render_msg(pard, msg, msg_class="err"):
    if isinstance(msg, str):
        msg = [msg]
    h = []
    for s in msg:
        h.append(f'<p class="{msg_class}">{s}</p>')
    return "\n".join(h)


def render_page(pard):
    html = (
        """
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="author" content="gianfranco" />
    <title>%(TITLE)s</title>

%(CSS)s
<link rel="icon" href="/static/favicon.ico">
</head>
<body>

<!-- header -->
<div id="header">%(page_header)s</div>

<!-- Sidebar -->
<div id="sidebar" style="display: none;">
%(sidebar)s
</div>

<!-- Main Body -->
<div id="main_body" align="center">
<form action="%(APPSERVER)slogin" method="post" name="theForm" id="theForm" autocomplete="off" spellcheck="false" enctype="multipart/form-data">  # noqa: E501
<!-- enctype="multipart/form-data" -->
<input type="hidden" id="action" name="action" value="%(action)s">
<input type="hidden" id="module" name="module" value="%(module)s">
%(msg)s
%(main_body)s
</form>
</div>

</body>
%(javascript)s
</html>
    """
        % pard
    )
    return html
