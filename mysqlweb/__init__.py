"""MySQLweb is a SQL client web application"""

__version__ = "25.1"
__author__ = "Gianfranco Messori <gmessori@gmail.com>"


def grun():
    import sys

    if len(sys.argv) > 1 and sys.argv[1] == "-v":
        print("version:", __version__)
        sys.exit(0)

    import subprocess
    import os.path
    from .config import copy_previus_data_dir

    copy_previus_data_dir()
    cdir = os.path.dirname(__file__)
    cmd = f"gunicorn -c {cdir}/gunicorn.conf.py mysqlweb.main:app"
    print(cmd)
    try:
        subprocess.run([cmd], shell=True)
    except KeyboardInterrupt:
        pass
