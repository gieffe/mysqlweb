from .dbapi import DBApi, DatabaseError
import re

import cx_Oracle

OracleError = cx_Oracle.DatabaseError


class Driver(DBApi):
    @classmethod
    def connect(cls, pard):
        connstring = pard["DB"]
        if not connstring:
            connstring = "%(USER)s/%(PASSWORD)s@%(HOST)s" % pard
        if not cx_Oracle:
            raise DatabaseError("Oracle client is not available")
        c = cx_Oracle.connect(connstring)
        return c

    @classmethod
    def get_db_tables_list(cls, pard):
        """Return the tables lists in "pard['DB']" database"""
        pard["sql"] = """SELECT table_name FROM all_tables"""
        result = cls.send(pard)
        tables_list = []
        for rec in result:
            tables_list.append(rec[0])
        return tables_list

    @classmethod
    def get_db_tables_list_like(cls, pard):
        """Return the tables lists in "pard['DB']" database
        where table name is like pard['table_search']"""
        # pard['sql'] = "show tables like '%(table_search)s%%'" % pard
        if pard["table_search"]:
            pard["sql"] = (
                """
                SELECT table_name FROM all_tables
                WHERE UPPER(table_name) LIKE '%s%%'
                """
                % pard["table_search"].upper()
            )
        else:
            pard["sql"] = """SELECT table_name FROM all_tables"""
        print(pard["sql"])
        result = cls.send(pard)
        tables_list = []
        for rec in result:
            tables_list.append(rec[0])
        return tables_list

    @classmethod
    def send(cls, pard):
        try:
            return super().send(pard)
        except OracleError as err:
            raise DatabaseError(str(err))

    @classmethod
    def send_dict(cls, pard):
        commands = get_query_command(pard["sql"])
        if commands == "select":
            pard["_exec_fetch"] = True
        else:
            pard["_exec_fetch"] = False
        try:
            return super().send_dict(pard)
        except OracleError as err:
            raise DatabaseError(str(err))

    @classmethod
    def get_primary_key(cls, pard):
        """Return the primary key columns lists for pard['db_table']"""
        pard["sql"] = (
            """
            SELECT cols.table_name, cols.column_name, cols.position, cons.status,
                   cons.owner
            FROM all_constraints cons, all_cons_columns cols
            WHERE cols.table_name = '%(db_table)s'
            AND cons.constraint_type = 'P'
            AND cons.constraint_name = cols.constraint_name
            AND cons.owner = cols.owner
            ORDER BY cols.table_name, cols.position
            """
            % pard
        )
        try:
            result = cls.send_dict(pard)
            print("get_primary_key", result)
        except DatabaseError:
            result = {"rows": []}
        primary_key = []
        for rec in result["rows"]:
            primary_key.append(rec["COLUMN_NAME"])
        if not primary_key:
            primary_key.append("ROWID")
        return primary_key

    # @classmethod
    # def show_index(cls, pard):
    #   """
    #   Return (primary_key_column_list, other_index_description)
    #   of pard['table_name']
    #
    #   sqlite> pragma index_list(book_episodes);
    #   seq | name                             | unique
    #   0   | sqlite_autoindex_book_episodes_1 | 1
    #
    #   sqlite> pragma index_info(sqlite_autoindex_book_episodes_1);
    #   seqno | cid | name
    #   0     | 1   | book_id
    #   1     | 2   | episode_title
    #
    #
    # Type Code   Type Description      Acts On Level
    # C   Check on a table  Column
    # O   Read Only on a view   Object
    # P   Primary Key   Object
    # R   Referential AKA Foreign Key   Column
    # U   Unique Key    Column
    # V   Check Option on a view    Object
    #
    #
    #
    #   """
    #   pard['db_table'] = pard['table_name']
    #   primary_key = cls.get_primary_key(pard)
    #   pard['sql'] = "pragma index_list(%(table_name)s)" % pard
    #   pard['sql'] = '''
    #       SELECT cols.table_name, cols.column_name, cols.position,
    #           cons.status, cons.owner, cons.constraint_type
    #       FROM all_constraints cons, all_cons_columns cols
    #       WHERE cols.table_name = '%(table_name)s'
    #       AND cons.constraint_name = cols.constraint_name
    #       AND cons.constraint_type != 'C'
    #       AND cons.owner = cols.owner
    #       ORDER BY cols.table_name, cols.position
    #       '''
    #   result = cls.send(pard)
    #   index = {}
    #   for rec in result:
    #       index_name = rec[1]
    #       non_unique = 0 if rec[2] else 1
    #       index[index_name] = {
    #           'index_name': index_name,
    #           'columns': [],
    #           'Non_unique': non_unique
    #           }
    #       pard['sql'] = "pragma index_info(%s)" % index_name
    #       index_info = cls.send(pard)
    #       for item in index_info:
    #           index[index_name]['columns'].append(item[2])
    #   index_list = []
    #   for key in index.keys():
    #       index_list.append(index[key])
    #   return primary_key, index_list
    #
    # select dbms_metadata.get_ddl('TABLE', 'SITPF', 'TMPROD') from dual;

    @classmethod
    def show_columns(cls, pard, out="dict"):
        """Return columns description from pard['table_name']
        The rows are formatted as a list of dictionaries by default.
        """
        if "db_table" in pard:
            pard["table_name"] = pard.get("table_name", pard["db_table"])
        pard["fetch_all_row"] = True
        pard["sql"] = (
            """SELECT COLUMN_NAME, DATA_TYPE, NULLABLE, DATA_DEFAULT
            FROM user_tab_cols
            WHERE UPPER(table_name) = '%s'
            """
            % pard["table_name"].upper()
        )
        print(pard["sql"])
        result = cls.send_dict(pard)
        if out == "dict":
            ll = []
            for rec in result["rows"]:
                d = {}
                d["Field"] = rec["COLUMN_NAME"]
                d["Type"] = rec["DATA_TYPE"]
                d["Null"] = rec["NULLABLE"]
                d["Key"] = ""
                d["Default"] = rec["DATA_DEFAULT"]
                d["Extra"] = ""
                ll.append(d)
            return ll
        elif out == "list":
            return [rec["COLUMN_NAME"] for rec in result["rows"]]

    @classmethod
    def limit_query(cls, sql_info: dict, query: str, limit: int) -> str:
        return query


# ----------------------------------------------------------------------------
# get query_command
# ----------------------------------------------------------------------------
nn_line_pat = re.compile(r"\s*(#|$)")
first_word_pat = re.compile(r"\s*(\w+)")


def get_query_command(sql):
    sql_lines = sql.split("\n")
    command = ""
    for line in sql_lines:
        # discard blank and comment lines
        if nn_line_pat.match(line):
            pass
        else:
            m = first_word_pat.match(line)
            if m:
                command = m.group(1).lower()
            break
    return command
