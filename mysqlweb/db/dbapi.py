"""
Base Class with all the method to enable data base access for MySQLweb
Inerith from it to define specific database method.

For Example:

class MySQL(DBApi):
    @classmethod
    def connect(cls, pard):
        return 'Connect Mysql'
    @classmethod
    def show_columns(cls, pard):
        ...
    ...
"""

import time
import logging


class DatabaseError(Exception):
    """Database error type."""


def close_pool_connection(pard):
    pard.setdefault("POOL", {})
    for k in pard["POOL"]:
        try:
            pard["POOL"][k].close()
            logging.debug("%s-%s: close connection" % k)
        except Exception:
            pass


class DBApi:

    @classmethod
    def connect(cls, pard):
        """To be override"""

    @classmethod
    def get_column_name(cls, c):
        columns = []
        for elem in c.description:
            column_name = elem[0]
            if column_name not in columns:
                columns.append(column_name)
            else:
                ind = 0
                while 1:
                    ind += 1
                    column_name = elem[0] + str(ind)
                    if column_name not in columns:
                        columns.append(column_name)
                        break
        return columns

    @classmethod
    def c_conn(cls, pard):
        pard.setdefault("POOL", {})
        k = (pard["mw_user"], pard["alias"])
        if k not in pard["POOL"]:
            pard["POOL"][k] = cls.connect(pard)
            pard["ATEXIT"] = close_pool_connection
            logging.debug("%(mw_user)s-%(alias)s: new connection" % pard)
        else:
            logging.debug("%(mw_user)s-%(alias)s: cached connection" % pard)
        # if 'sql' in pard:
        #   logging.debug(pard['sql'])
        return pard["POOL"][k]

    @classmethod
    def send(cls, pard):
        # pard = filter_sql(pard)
        conn = cls.c_conn(pard)
        c = conn.cursor()
        c.execute(pard["sql"])
        buf = c.fetchall()
        # conn.close()
        if not buf:
            return []
        else:
            return buf

    @classmethod
    def limit_query(cls, x, query: str, limit: int) -> str:
        raise NotImplementedError()

    @classmethod
    def requires_commit(cls, sql_info=None):
        if not sql_info:
            sql_info = {}
        if "query_command" in sql_info and sql_info["query_command"] in (
            "update",
            "insert",
            "replace",
            "truncate",
            "alter",
            "delete",
        ):
            return True
        else:
            return False

    @classmethod
    def send_dict(cls, pard):
        result = {"c_name": [], "rows": [], "rowcount": 0, "exec_time": "0"}
        start = time.time()
        # conn = cls.connect(pard)
        conn = cls.c_conn(pard)
        c = conn.cursor()
        _exec_fetch = pard.get("_exec_fetch", True)
        pard["fetch_all_row"] = pard.get("fetch_all_row", False)

        is_select = "sql_info" in pard and pard["sql_info"]["query_command"] == "select"

        if pard["MAX_ROW"] == "fetchall" or pard["fetch_all_row"]:
            c.execute(pard["sql"])
            result["rowcount"] = c.rowcount
            buf = c.fetchall() if _exec_fetch else []

        else:

            if is_select:
                pard["sql"] = cls.limit_query(
                    pard["sql_info"],
                    query=pard["sql"],
                    limit=int(pard["MAX_ROW"]),
                )

                if hasattr(c, "arraysize"):
                    c.arraysize = int(pard["MAX_ROW"])

                if "limit" not in pard["sql_info"]["words"]:
                    pard["limit_clause"] = True

            c.execute(pard["sql"])
            result["rowcount"] = c.rowcount
            buf = c.fetchmany(pard["MAX_ROW"]) if _exec_fetch else []

        sql_info = pard["sql_info"] if "sql_info" in pard else {}
        if cls.requires_commit(sql_info):
            conn.commit()

        stop = time.time()
        result["exec_time"] = repr(stop - start)[0:5]
        pard["fetch_all_row"] = False
        if buf:
            cols = cls.get_column_name(c)
            ll = []
            for elem in buf:
                rec = {}
                ind = 0
                for key in cols:
                    rec[key] = elem[ind]
                    ind += 1
                ll.append(rec)
            result["c_name"] = cols
            result["rows"] = ll
        # conn.close()
        return result

    @classmethod
    def run_procedure(cls, pard):
        # conn = cls.connect(pard)
        conn = cls.c_conn(pard)
        c = conn.cursor()
        result = []
        try:
            requires_commit = False

            for i, qd in enumerate(pard["sqls"]):
                resd = {
                    "query": qd["query"],
                    "c_name": [],
                    "rows": [],
                    "rowcount": 0,
                    "exec_time": "",
                    "error": "",
                }
                sql = qd["query"]
                start = time.time()

                requires_commit |= cls.requires_commit(qd["sql_info"])

                if i == len(pard["sqls"]) - 1:
                    # ultima query
                    is_select = qd["sql_info"]["query_command"] == "select"

                    if is_select:
                        sql = cls.limit_query(
                            qd["sql_info"],
                            query=sql,
                            limit=int(pard["MAX_ROW"]),
                        )
                        pard["limit_clause"] = True

                        if hasattr(c, "arraysize"):
                            c.arraysize = pard["MAX_ROW"]

                    c.execute(sql)

                    if qd["sql_info"]["query_command"] == "select":
                        buf = c.fetchmany(pard["MAX_ROW"])
                    else:
                        buf = []
                    if buf:
                        cols = cls.get_column_name(c)
                        ll = []
                        for elem in buf:
                            rec = {}
                            ind = 0
                            for key in cols:
                                rec[key] = elem[ind]
                                ind += 1
                            ll.append(rec)
                        resd["c_name"] = cols
                        resd["rows"] = ll
                else:
                    c.execute(sql)

                resd["rowcount"] = c.rowcount
                stop = time.time()
                resd["exec_time"] = repr(stop - start)[0:5]
                result.append(resd)

            if requires_commit:
                conn.commit()

        except Exception as err:
            resd["error"] = '<span class="err">DatabaseError: %s</span>' % str(err)
            conn.rollback()
            result.append(resd)

        finally:
            # conn.close()
            pass

        return result

    @classmethod
    def get_db_tables_list(cls, pard):
        """Return the tables lists in "pard['DB']" database (To be override)"""
        return []

    @classmethod
    def get_db_tables_list_like(cls, pard):
        """Return the tables lists in "pard['DB']" database
        where table name is like pard['table_search']
        (To be override)
        """
        return []

    @classmethod
    def get_primary_key(cls, pard):
        """Return the primary key columns lists for pard['db_table']
        (To be override)
        """
        return []

    @classmethod
    def show_columns(cls, pard, out="dict"):
        """Return columns description from pard['table_name']
        The rows are formatted as a list of dictionaries by default.
        (To be override)
        """
        return []

    @classmethod
    def show_all_columns(cls, pard, tables_list=[]):
        """This method could be DBMS indipendent"""
        columns = set()
        for pard["table_name"] in tables_list:
            table_fields = cls.show_columns(pard, out="list")
            for field in table_fields:
                columns.add(field)
        columns = list(columns)
        columns.sort()
        return columns

    @classmethod
    def show_index(cls, pard):
        """Return (primary_key_column_list, other_index_description)
        from pard['table_name']
        (To be override)
        """
        return [], []

    @classmethod
    def show_create_table(cls, pard):
        """(To be override)"""
        return ""

    @classmethod
    def show_table_status(cls, pard):
        """Return information about the state of a table
        (e.g. table rows)
        (To be override)
        """
        return {}
