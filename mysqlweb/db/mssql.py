import pymssql
import re

from .dbapi import DBApi, DatabaseError

MsSQLError = pymssql.OperationalError, pymssql.ProgrammingError


class Driver(DBApi):
    @classmethod
    def connect(cls, pard):
        conn_info = pard["HOST"].split(":")
        if len(conn_info) == 2:
            host, port = conn_info
        else:
            host, port = conn_info[0], "1443"
        user = pard["USER"]
        passwd = pard["PASSWORD"]
        db = pard["DB"]

        return pymssql.connect(
            server=host,
            user=user,
            password=passwd,
            port=port,
            database=db,
            timeout=10,
            tds_version="7.0",
        )

    @classmethod
    def send(cls, pard):
        try:
            return super().send(pard)
        except MsSQLError as err:
            raise DatabaseError(str(err))

    @classmethod
    def send_dict(cls, pard):
        try:
            return super().send_dict(pard)
        except MsSQLError as err:
            raise DatabaseError(str(err))

    @classmethod
    def get_db_tables_list(cls, pard):
        pard["sql"] = "select * from sysobjects where type = 'U'"
        result = cls.send(pard)
        return [rec[0] for rec in result]

    @classmethod
    def get_db_tables_list_like(cls, pard):
        pard["sql"] = (
            f"""\
            select
                *
            from
                sysobjects
            where
                type = 'U'
                and name like '{pard['table_search']}%'"
        """
        )
        result = cls.send(pard)
        return [rec[0] for rec in result]

    @classmethod
    def show_columns(cls, pard, out="dict"):
        table_name = pard.get("table_name", "")
        pard["sql"] = (
            f"""\
            select
                table_name,
                column_name,
                is_nullable,
                column_default,
                data_type,
                character_maximum_length as max
            from
                information_schema.columns
            where
                table_name = '{table_name}'
        """
        )
        results = cls.send_dict(pard)

        if out == "dict":
            return [
                {
                    "Field": rec["column_name"],
                    "Type": (
                        f"{rec['data_type']}({rec['max']})"
                        if rec["max"] is not None
                        else rec["data_type"]
                    ),
                    "Null": rec["is_nullable"],
                    "Key": "",
                    "Default": rec["column_default"],
                    "Extra": "",
                }
                for rec in results["rows"]
            ]

        elif out == "list":
            return [rec["COLUMN_NAME"] for rec in results["rows"]]

    @classmethod
    def limit_query(cls, sql_info, query: str, limit: int) -> str:
        return re.sub(r"\bselect\b", f"select top {limit}", query, count=1, flags=re.I)
