import pprint
import sys

from urllib.parse import parse_qs
from html import escape

from . import qlite
from . import tools


class Error(Exception):
    """Base class for other exceptions"""

    pass


class NotFound(Error):
    """Raised when the record is not found"""

    pass


class DuplicateKey(Error):
    """Key name already exist on preferences DB"""

    pass


class InvalidData(Error):
    """Raised when input data is not valid"""

    pass


class AuthenticationError(Error):
    """Raised when authentication failed"""

    pass


class ChangePassword(Error):
    """Raised when user must change the password"""

    pass


def init(pard):
    args = {}
    args.setdefault("email", "")
    args.setdefault("sid", "")
    args.setdefault("admin", "N")
    return args


def check_args(pard, args):
    args.setdefault("email", "")
    args.setdefault("admin", "N")
    args.setdefault("sid", "")

    if args["sid"]:
        try:
            args["sid"] = tools.ascii_string(args["sid"], prop="sid")
        except Exception as err:
            raise InvalidData(str(err))

    try:
        args["email"] = tools.email(args["email"])
    except Exception as err:
        raise InvalidData(str(err))

    if args["admin"] not in ("N", "Y"):
        raise InvalidData("Invalid value for admin flag")

    return args


def insert(pard, args):
    args = check_args(pard, args)
    args.setdefault("password", "")
    args.setdefault("retype_password", "")

    rec = get_by_email(pard, args)
    if rec:
        raise DuplicateKey("Email already in use" % args)

    if args["password"] != args["retype_password"]:
        raise InvalidData("Typing error: passwords entered does not match")

    try:
        args["password"] = tools.phash(args["password"], name="password", min_len=8)
    except Exception as err:
        raise InvalidData(str(err))

    sql = """
        insert into users
            (email, password, sid, admin)
        values
            (:email, :password, :sid, :admin)
        """
    user_id = pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return user_id


def login(pard, args):
    args.setdefault("email", "")
    args.setdefault("password", "")

    try:
        args["email"] = tools.email(args["email"])
    except Exception as err:
        raise InvalidData(str(err))

    rec = get_by_email(pard, args)
    if not rec:
        raise AuthenticationError("Invalid Email Address")

    try:
        args["password"] = tools.phash(
            args["password"], salt=rec["password"][:32], name="password"
        )
    except Exception as err:
        raise InvalidData(str(err))

    if rec["password"] != args["password"]:
        raise AuthenticationError("Invalid Password")

    if rec["sid"] == "change_password":
        raise ChangePassword("User must change his password")

    args["sid"] = tools.get_alpha_token()

    sql = """
        update
            users
        set
            sid = :sid
        where
            email = :email
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()

    return args["sid"]


def local_login(pard, args):
    args.setdefault("email", "")
    try:
        args["email"] = tools.email(args["email"])
    except Exception as err:
        raise InvalidData(str(err))
    rec = get_by_email(pard, args)
    if not rec:
        raise AuthenticationError("Invalid Email Address")
    args["sid"] = tools.get_alpha_token()
    sql = """
        update
            users
        set
            sid = :sid
        where
            email = :email
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return args["sid"]


def logout(pard, args):
    args.setdefault("user_id", "")
    sql = "update users set sid = '' where user_id = :user_id"
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()


def change_password(pard, args):
    args.setdefault("email", "")
    args.setdefault("password", "")
    args.setdefault("new_password", "")
    args.setdefault("retype_password", "")

    try:
        login(pard, args)
    except ChangePassword:
        pass

    if args["new_password"] != args["retype_password"]:
        raise InvalidData("Typing error: passwords entered does not match")

    try:
        args["password"] = tools.phash(args["new_password"], name="password", min_len=8)
    except Exception as err:
        raise InvalidData(str(err))

    args["sid"] = tools.get_alpha_token()
    sql = """
        update
            users
        set
            password = :password,
            sid = :sid
        where
            email = :email
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return args["sid"]


def get_by_id(pard, args):
    args.setdefault("user_id", 0)
    sql = "select * from users where user_id = :user_id"
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_by_email(pard, args):
    args.setdefault("email", "")
    sql = "select * from users where email = :email"
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_by_sid(pard, args):
    args.setdefault("sid", "")
    sql = "select * from users where sid = :sid"
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_user(pard, sid_user):
    result = get_by_sid(pard, {"sid": sid_user})
    return result


def get_default_user(pard, args=None):
    sql = "select * from users where status = 'default'"
    result = pard["sqlite.conn"].query(sql)
    return result[0] if result else {}


def set_default_user(pard, args):
    args.setdefault("email", "")
    user_data = get_by_email(pard, args)
    if not user_data:
        raise NotFound("User `%s` not found" % escape(args["email"]))
    default_user = get_default_user(pard)
    if default_user:
        sql = "update users set status = '' where user_id = :user_id"
        pard["sqlite.conn"].execute(sql, default_user)
    sql = "update users set status = 'default' where user_id = :user_id"
    pard["sqlite.conn"].execute(sql, user_data)
    pard["sqlite.conn"].commit()


def get_list(pard, args=None):
    if not args:
        args = {}
    sql = """
        select
            *
        from
            users
        order by
            email
        """
    result = pard["sqlite.conn"].query(sql, args)
    return result


def update(pard, args):
    args = check_args(pard, args)
    rec = get_by_id(pard, args)
    if not rec:
        raise NotFound("Record not found")

    rec = get_by_email(pard, args)
    if rec and str(rec["user_id"]) != str(args["user_id"]):
        raise DuplicateKey("email already in use")

    sql = """
    update
        users
    set
        email = :email,
        admin = :admin
    where
        user_id = :user_id
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return args


def delete(pard, args):
    args.setdefault("user_id", 0)
    sql = "delete from users where user_id = :user_id"
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return None


def reset_password(pard, args):
    rec = get_by_id(pard, args)
    if not rec:
        raise NotFound("Record not found")

    new_password = tools.inventa_nome()
    args["password"] = tools.phash(new_password)
    sql = """
        update
            users
        set
            password = :password,
            sid = 'change_password'
        where
            user_id = :user_id
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return new_password


def bulk_user_insert(pard, args):
    """
    Create users and copy connection preferences from user specified in
    user_to_copy parameter

    exemple:
            python users.py bulk_user_insert
                            "users=pippo@pluto.com,paperino@pluto.com&user_to_copy=10"

    output:
            ['pippo@pluto.com --> Email already in use',
             '9 - paperino@pluto.com --> nutria_marrone']

    """
    args.setdefault("users", "")
    args["users"] = args["users"].split(",")
    args.setdefault("user_to_copy", 1)
    result = []

    import prefs

    rec_prefs = prefs.get_list(
        pard, {"mw_user": int(args["user_to_copy"]), "status": "active"}
    )

    for email in args["users"]:
        email = email.strip()
        rec = {}
        rec["email"] = email
        rec["admin"] = "N"
        rec["password"] = tools.inventa_nome()
        rec["retype_password"] = rec["password"]
        rec["sid"] = "change_password"
        rec["password_non_cifrata"] = rec["password"]
        try:
            rec["user_id"] = insert(pard, rec)
        except Error as err:
            result.append(f"{rec['email']} --> {err}")
            continue
        for recp in rec_prefs:
            recp["mw_user"] = rec["user_id"]
            prefs.insert(pard, recp)
        result.append("%(user_id)s - %(email)s --> %(password_non_cifrata)s" % rec)
    return result


# -------------------------------------------------------------------------- #
def _get_action(action):
    try:
        action = globals()[action]
    except KeyError:
        raise Exception("Funzione `%s` non presente nel modulo" % action)
    if not hasattr(action, "__call__"):
        raise Exception("Funzione `%s` non presente nel modulo" % action)
    return action


def _get_param(params):
    qs = params
    params = parse_qs(qs)
    params = {k: v[0] if len(v) == 1 else v for k, v in params.items()}
    return params


def run():
    import config

    pard = config.global_parameters({})
    action = _get_action(sys.argv[1])
    params = sys.argv[2] if len(sys.argv) > 2 else ""
    args = _get_param(params)
    print(action.__name__, params, sep=" -> ")
    pard["sqlite.conn"] = qlite.DB(pard["SQLITE_DB"])
    result = action(pard, args)
    pard["sqlite.conn"].close()
    pprint.pprint(result)


if __name__ == "__main__":
    run()
