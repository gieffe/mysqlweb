import json
import pprint
import sys

from urllib.parse import parse_qs
from wsgian import utils

from . import home
from . import tools
from . import db
from . import tunnel
from . import users
from . import qlite
from . import session_data


# ---------------------------------------------------------------------------
# Module Config
# ---------------------------------------------------------------------------
db_structure = {
    "prefs": """
        CREATE TABLE prefs (
            id                integer not null primary key autoincrement,
            mw_user           text not null,
            alias             text not null,
            engine            text not null,
            conn_type         text not null,
            tunnel_id         integer not null,
            user              text not null,
            host              text not null,
            db                text not null,
            passw             text not null,
            color             text not null
            )
        """,
    "idx_prefs_alias": """
        CREATE UNIQUE INDEX idx_prefs_alias
            ON prefs (mw_user, alias)
        """,
    "alter_log": """
        CREATE TABLE alter_log (
            id       text not null primary key,
            stm      text not null
            )
        """,
    "users": """
        CREATE TABLE users (
            user_id           integer not null primary key autoincrement,
            email             text not null,
            password          blob not null,
            sid               text not null default '',
            admin             text not null default 'N',
            status            text not null default ''
            )
        """,
    "idx_users_email": """
        CREATE UNIQUE INDEX idx_users_email
            ON users (email)
        """,
}
db_structure.update([session_data.Sdata.create_statement()])

db_alter = [
    {
        "id": "1",
        "stm": "alter table prefs add column seq_num integer not null default 0",
    },
    {"id": "2", "stm": "alter table prefs add column status text not null default ''"},
    #   {'id' : '4', # provvisorio
    #    'stm': "alter table users add column status text not null default ''"},
]

conn_type_list = ["Standard", "SSH"]
# engine_list = ['mysql', 'oracle', 'sqlite', 'click']
engine_list = list(db.DRIVERS.keys())

# color_list = [
#       ('#2a68c8', 'default'),
#       ('#000000', 'Black'),
#       ('#FF0000', 'Red'),
#       ('#FFA500', 'Orange'),
#       ('#FFFF00', 'Yellow'),
#       ('#008000', 'Green'),
#       ('#0000FF', 'Blue'),
#       ('#800080', 'Purple'),
#       ('#808080', 'Gray'),
#       ]

color_list = [
    ("#2a68c8", "default"),
    ("#222222", "Black"),
    ("#eb291f", "Red"),
    ("#e67d00", "Orange"),
    ("#e6b400", "Yellow"),
    ("#0fb52b", "Green"),
    ("#006ff6", "Blue"),
    ("#9b48c8", "Purple"),
    ("#6c6d74", "Gray"),
]

# ---------------------------------------------------------------------------


def main(pard):
    pard["action"] = pard.get("action", "start")
    pard["page_header"] = pard["TITLE"] + " - Preferences"
    # pard['css'] = css(pard)
    pard["javascript"] = javascript(pard)
    pard["sidebar"] = ""
    pard["main_body"] = ""
    pard["msg"] = ""
    pard["error_msg"] = ""
    pard["max_rows"] = pard.get("max_rows", "25")
    if not pard["max_rows"].isdigit():
        pard["max_rows"] = "25"

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    pard.setdefault("user_data", {})
    if pard["user_data"]:
        pard["mw_user"] = str(pard["user_data"]["user_id"])

    pard["userbar"] = home.render_userbar(pard)

    # --------------------------- start ----------------------------
    if pard["action"] == "start":
        result = get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    # --------------------------- insert ----------------------------
    elif pard["action"] == "insert":
        rec = init(pard)
        pard["main_body"] = render_form(pard, rec)

    # --------------------------- edit ----------------------------
    elif pard["action"] == "edit":
        rec = get_by_id(pard, {"id": pard["id"], "mw_user": pard["mw_user"]})
        pard["main_body"] = render_form(pard, rec)

    # --------------------------- save_insert ----------------------------
    elif pard["action"] in ("save_insert", "save_update"):
        rec = get_form(pard)
        try:
            if pard["action"] == "save_insert":
                rec["id"] = insert(pard, rec)
            else:
                update(pard, rec)
            pard["msg"] = render_msg(
                pard, "`%(id)s - %(alias)s` preferences saved successfully" % rec, "msg"
            )
            result = get_list(pard, {"mw_user": pard["mw_user"]})
            pard["main_body"] = render_list(pard, result)
        except Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_form(pard, rec)

    # --------------------------- copy ----------------------------
    elif pard["action"] == "copy":
        args = {"id": pard["id"]}
        try:
            result = make_a_copy(pard, args)
            pard["msg"] = render_msg(
                pard,
                "`%(id)s - %(alias)s` preferences saved successfully" % result,
                "msg",
            )
            result = get_list(pard, {"mw_user": pard["mw_user"]})
            pard["main_body"] = render_list(pard, result)
        except Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_form(pard, rec)

    # --------------------------- delete ----------------------------
    elif pard["action"] == "delete":
        rec = get_form(pard)
        delete(pard, {"id": rec["id"], "mw_user": pard["mw_user"]})
        pard["msg"] = render_msg(
            pard, "`%(id)s - %(alias)s` deleted successfully" % rec, "msg"
        )
        result = get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    # --------------------------- get_tunnel_host ----------------------------
    elif pard["action"] == "get_tunnel_host":
        rec = tunnel.get_by_id(pard, {"id": pard["tunnel_id"]})
        if rec:
            pard["html"] = "127.0.0.1:%(local_bind_port)s" % rec
        else:
            pard["html"] = ""
        return pard

    # --------------------------- load_prefs ----------------------------
    elif pard["action"] == "load_prefs":
        rec = get_by_alias(pard, {"alias": pard["alias"], "mw_user": pard["mw_user"]})
        pard["html"] = json.dumps(rec)
        pard["header"] = [("Content-type", "application/json")]
        return pard

    # --------------------------- view_session_data ----------------------------
    elif pard["action"] == "view_session_data":
        rec = get_by_id(pard, {"id": pard["id"], "mw_user": pard["mw_user"]})
        sdata = session_data.Sdata(pard["sqlite.conn"], "%(mw_user)s::%(alias)s" % rec)
        result = sdata.list()
        pard["main_body"] = render_view_session_data(pard, rec, result)

    # --------------------------- delete_session_data ----------------------------
    elif pard["action"] == "delete_session_data":
        rec = get_by_id(pard, {"id": pard["id"], "mw_user": pard["mw_user"]})
        sdata = session_data.Sdata(pard["sqlite.conn"], "%(mw_user)s::%(alias)s" % rec)
        result = sdata.list()
        for el in result:
            sdata.delete(el["key"])
        pard["msg"] = render_msg(
            pard, "`%(id)s - %(alias)s` Session data deleted successfully" % rec, "msg"
        )
        result = get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    else:
        pard["msg"] = 'Function not available. ("%(action)s")' % pard
        pard["main_body"] = (
            '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'  # noqa: E501
        )

    pard["html"] = html(pard)
    return pard


def render_form(pard, rec):
    h = []
    if "id" in rec and rec["id"]:
        h.append('<input type="hidden" name="id" id="id" value="%(id)s">' % rec)
    h.append('<table width="100%">')
    h.append("<tr>")
    if pard["action"] in ("insert", "save_insert"):
        rec["v_action"] = "New"
    else:
        rec["v_action"] = "Edit"
    h.append('<th colspan="2">%(v_action)s Connection</th>' % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td width="150px"><b>Alias</b></td>')
    h.append(
        '<td><input type="text" name="alias" value="%(alias)s" style="width: 98%%;"></td>'  # noqa: E501
        % rec
    )
    h.append("</tr>")

    rec["pop_engine"] = tools.make_pop(
        "engine", [(x, x) for x in engine_list], rec["engine"], 'style="width: 150px;"'
    )

    h.append("<tr>")
    h.append("<td><b>Engine</b></td>")
    h.append("<td>%(pop_engine)s</td>" % rec)
    h.append("</tr>")

    rec["pop_conn_type"] = tools.make_pop(
        "conn_type",
        [(x, x) for x in conn_type_list],
        rec["conn_type"],
        'style="width: 150px;" onChange="change_conn_type();"',
    )

    h.append("<tr>")
    h.append("<td><b>Connection Type</b></td>")
    h.append("<td>%(pop_conn_type)s</td>" % rec)
    h.append("</tr>")

    tunnel_list = tunnel.get_list(pard)
    ll = [
        (el["id"], "[%(name)s] Server: %(ssh_server)s - DB Host: %(db_host)s" % el)
        for el in tunnel_list
    ]
    ll.insert(0, (0, "&nbsp;"))
    if rec["conn_type"] == "Standard":
        disabled = "disabled"
    else:
        disabled = ""
    rec["pop_tunnel"] = tools.make_pop(
        "tunnel_id",
        ll,
        rec["tunnel_id"],
        f'{disabled} onChange="change_tunnel_id(this.value);"',
    )

    h.append("<tr>")
    h.append("<td><b>Tunnel</b></td>")
    h.append("<td>%(pop_tunnel)s</td>" % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append("<td><b>User</b></td>")
    h.append(
        '<td><input type="text" name="user" value="%(user)s" style="width: 98%%;"></td>'
        % rec
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append("<td><b>Host</b></td>")
    h.append(
        '<td><input type="text" name="host" id="host" value="%(host)s" style="width: 98%%;"></td>'  # noqa: E501
        % rec
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append("<td><b>Data Base</b></td>")
    h.append(
        '<td><input type="text" name="db" value="%(db)s" style="width: 98%%;"></td>'
        % rec
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append("<td><b>Password</b></td>")
    h.append(
        '<td><input type="password" name="passw" value="%(passw)s" style="width: 98%%;"></td>'  # noqa: E501
        % rec
    )
    h.append("</tr>")

    rec["pop_color"] = tools.make_pop(
        "color", color_list, rec["color"], 'onChange="change_pop_color();"'
    )

    h.append("<tr>")
    h.append("<td><b>Color</b></td>")
    h.append(
        '<td>%(pop_color)s <span id="talloncino" style="background-color: %(color)s;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> </td>'  # noqa: E501
        % rec
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td width="150px"><b>Sequence Number</b></td>')
    h.append(
        '<td><input type="text" name="seq_num" value="%(seq_num)s" style="width: 98%%;"></td>'  # noqa: E501
        % rec
    )
    h.append("</tr>")

    rec["pop_status"] = tools.make_pop(
        "status",
        [("", "Active"), ("disabled", "Disabled")],
        rec["status"],
        'style="width: 150px;"',
    )

    h.append("<tr>")
    h.append("<td><b>Status</b></td>")
    h.append("<td>%(pop_status)s</td>" % rec)
    h.append("</tr>")

    h.append('<tr><td colspan="2"></td></tr>')
    h.append("<tr>")

    if pard["action"] in ("insert", "save_insert"):
        h.append("<td>&nbsp;</td>")
    else:
        h.append(
            """<td><input type="button" name="submit_form" value="Delete" class="btn" onclick="confirm_action('delete');"></td>"""  # noqa: E501
        )

    h.append('<td align="right">')
    h.append(
        """<input type="button" name="submit_form" value="Cancel" class="btn" onclick="action.value='start'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
    )
    if pard["action"] in ("insert", "save_insert"):
        rec["action"] = "save_insert"
    else:
        rec["action"] = "save_update"
    h.append(
        """<input type="submit" name="submit_form" value="Save" class="btn" onclick="action.value='%(action)s'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
        % rec
    )
    h.append("</td>")

    h.append("</tr>")

    h.append("</table>")
    return "\n".join(h)


def render_view_session_data(pard, rec, result):
    h = []
    h.append('<input type="hidden" name="id" id="id" value="%(id)s">' % rec)
    h.append('<table width="100%">')
    h.append("<tr>")
    h.append('<th colspan="2">Session data for "%(alias)s"</th>' % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td align="right" colspan="2">')
    h.append('<span style="float: left;">')
    h.append(
        """<input type="button" value="Exit" class="btn" onclick="action.value='start'; form.submit();">"""  # noqa: E501
    )
    h.append("&nbsp;" * 3)
    h.append("</span>")
    if result:
        h.append(
            """<input type="button" name="submit_form" value="Delete Session Data" class="btn" onclick="confirm_action('delete_session_data');">"""  # noqa: E501
        )
    h.append("</td>")
    h.append("</tr>")

    if result:
        h.append("<tr>")
        h.append("<th>Key</th>")
        h.append("<th>Value</th>")
        h.append("</tr>")
        for el in result:
            value = utils.format_as_pre(el["value"])
            h.append("<tr>")
            h.append("<td>%(key)s</td>" % el)
            h.append(f'<td><div class="vtop dont-break-out">{value}</div></td>')
            h.append("</tr>")
    h.append("</table>")
    return "\n".join(h)


def render_list(pard, result):
    h = []
    h.append('<table width="100%">')
    h.append("<tr>")
    h.append(
        '<th colspan="13">Connection Preferences (for %s)</th>'
        % pard["user_data"]["email"]
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td align="right" colspan="13">')
    h.append('<span style="float: left;">')
    alias = pard.get("alias", "")
    if alias:
        link = f"qq/{alias}"
        h.append(
            f"""<input type="button" value="Exit" class="btn" onclick="location.href='{link}';">"""  # noqa: E501
        )
    if "user_data" in pard and pard["user_data"] and pard["user_data"]["admin"] == "Y":
        h.append(
            f"""<input type="button" value="SSH Tunnel Preferences" class="btn" onclick="location.href='/tunnel?alias={alias}';">"""  # noqa: E501
        )
        h.append(
            f"""<input type="button" value="Users Management" class="btn" onclick="location.href='/users?alias={alias}';">"""  # noqa: E501
        )
    h.append("</span>")
    h.append(
        """<b><input type="button" name="submit_form" value="New" class="btn" onclick="action.value='insert'; form.submit();"></b>"""  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append("<th>&nbsp;</th>")
    h.append("<th>&nbsp;</th>")
    h.append("<th>Alias</th>")
    h.append("<th>Engine</th>")
    h.append("<th>Type</th>")
    h.append("<th>Tunnel ID</th>")
    h.append("<th>User</th>")
    h.append("<th>Host</th>")
    h.append("<th>DB</th>")
    h.append("<th>Password</th>")
    h.append("<th>Color</th>")
    h.append("<th>Seq.Num</th>")
    h.append("<th>Status</th>")
    h.append("</tr>")

    for rec in result:
        h.append("<tr>")
        rec[
            "link_edit"
        ] = f"""
            <a href="{pard['APPSERVER']}prefs?action=edit&id={rec['id']}"
            title="Edit">
            <img src="/static/icons/application_form_edit.png" border="0"></a>
            """
        rec[
            "link_connect"
        ] = f"""
            <a href="{pard['APPSERVER']}cv/{rec['alias']}"
            title="Connect">
            <img src="/static/icons/database_connect.png" border="0"></a>
            """
        rec[
            "link_copy"
        ] = f"""
            <a href="{pard['APPSERVER']}prefs?action=copy&id={rec['id']}"
            title="Make a copy">
            <img src="/static/icons/page_copy.png" border="0"></a>
            """
        rec[
            "link_view_session_data"
        ] = f"""
            <a href="{pard['APPSERVER']}prefs?action=view_session_data&id={rec['id']}"
            title="View session data">
            <img src="/static/icons/report_user.png" border="0"></a>
            """
        h.append('<td align="center">%(link_connect)s</td>' % rec)
        h.append(
            '<td align="center">%(link_edit)s&nbsp;&nbsp;&nbsp;%(link_copy)s&nbsp;&nbsp;&nbsp;%(link_view_session_data)s</td>'  # noqa: E501
            % rec
        )
        h.append("<td>%(alias)s</td>" % rec)
        h.append("<td>%(engine)s</td>" % rec)
        h.append("<td>%(conn_type)s</td>" % rec)
        if not rec["tunnel_name"]:
            rec["tunnel_name"] = ""
        if not rec["tunnel_id"] or rec["tunnel_id"] == "0":
            rec["tunnel_id"] = ""
        h.append("<td>%(tunnel_id)s - %(tunnel_name)s</td>" % rec)
        h.append("<td>%(user)s</td>" % rec)
        h.append("<td>%(host)s</td>" % rec)
        h.append("<td>%(db)s</td>" % rec)
        if rec["passw"]:
            rec["v_passw"] = "*" * len(rec["passw"])
        else:
            rec["v_passw"] = "&nbsp;"
        h.append("<td>%(v_passw)s</td>" % rec)
        h.append('<td style="background-color: %(color)s;">&nbsp;</td>' % rec)
        h.append("<td>%(seq_num)s</td>" % rec)
        h.append("<td>%(status)s</td>" % rec)
        h.append("</tr>")

    h.append("</table>")
    return "\n".join(h)


def get_form(pard):
    rec = {}
    rec["id"] = pard.get("id", "")
    rec["alias"] = pard.get("alias", "")
    rec["engine"] = pard.get("engine", "mysql")
    rec["conn_type"] = pard.get("conn_type", "Standard")
    rec["tunnel_id"] = pard.get("tunnel_id", "")
    rec["user"] = pard.get("user", "")
    rec["host"] = pard.get("host", "")
    rec["db"] = pard.get("db", "")
    rec["passw"] = pard.get("passw", "")
    rec["color"] = pard.get("color", "")
    rec["seq_num"] = pard.get("seq_num", "0")
    rec["status"] = pard.get("status", "")
    return rec


def render_msg(pard, msg, msg_class="err"):
    if isinstance(msg, str):
        msg = [msg]
    h = []
    for s in msg:
        h.append(f'<p class="{msg_class}">{s}</p>')
    return "\n".join(h)


def javascript(pard):
    javascript = """
    <script>

        let xhr;

        const confirm_action = function (action) {
            if (confirm('Confirm ' + action + '?')) {
                the_form = document.getElementById('theForm');
                the_action = document.getElementById('action');
                the_action.value = action;
                the_form.submit();
                }
        };

        const change_conn_type = function () {
            let pop_tunnel_id = document.getElementById('tunnel_id');
            let pop_conn_type = document.getElementById('conn_type');
            if (pop_conn_type.value == 'SSH') {
                pop_tunnel_id.disabled = false;
                }
            else {
                pop_tunnel_id.disabled = true;
                pop_tunnel_id.value = 0;
                change_tunnel_id(0);
                }
        };

        const change_pop_color = function () {
            let pop_color = document.getElementById('color');
            let talloncino = document.getElementById('talloncino');
            talloncino.style.backgroundColor = pop_color.value;
        };

        const change_tunnel_id = function (tunnel_id) {
            let host = document.getElementById('host');
            if (tunnel_id == 0) {
                host.disabled = false;
                host.value = '';
                return false
            }
            else {
                host.disabled = true;
            }
            xhr = new XMLHttpRequest();
            if (!xhr) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
            }
            xhr.onload = function () {
                let host = document.getElementById('host');
                host.value = this.responseText;
            };

            let query = []
            query.push(encodeURIComponent('action') + '=' + encodeURIComponent('get_tunnel_host'));  # noqa: E501
            query.push(encodeURIComponent('tunnel_id') + '=' + encodeURIComponent(tunnel_id));  # noqa: E501
            let qq = query.join('&');

            let url = '/prefs?' + qq;
            xhr.open('GET', url, true);
            xhr.send();

        };

    </script>
    """
    return javascript


def html(pard):
    html = (
        """
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="author" content="gianfranco" />
    <title>%(TITLE)s</title>

%(CSS)s
<link rel="icon" href="/static/favicon.ico">
</head>
<body>

<!-- header -->
<div id="userbar">%(userbar)s</div>
<div id="header">%(page_header)s</div>

<!-- Sidebar -->
<div id="sidebar" style="display: none;">
%(sidebar)s
</div>

<!-- Main Body -->
<div id="main_body" align="center">
<form action="/prefs" method="post" name="theForm" id="theForm"
      autocomplete="off" spellcheck="false">
<!-- enctype="multipart/form-data" -->
<input type="hidden" id="action" name="action" value="%(action)s">
<input type="hidden" id="module" name="module" value="%(module)s">
%(msg)s
%(main_body)s
</form>
</div>

</body>
%(javascript)s
</html>
    """
        % pard
    )
    return html


# -------------------------------------------------------------------------- #
# backend
# -------------------------------------------------------------------------- #


class Error(Exception):
    """Base class for other exceptions"""

    pass


class NotFound(Error):
    """Raised when the record is not found"""

    pass


class DuplicateKey(Error):
    """Key name already exist on preferences DB"""

    pass


class InvalidData(Error):
    """Raised when input data is not valid"""

    pass


def init(pard):
    args = {}
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    args.setdefault("alias", "")
    args.setdefault("engine", "mysql")
    args.setdefault("conn_type", "Standard")
    args.setdefault("tunnel_id", 0)
    args.setdefault("user", "")
    args.setdefault("host", "")
    args.setdefault("db", "")
    args.setdefault("passw", "")
    args.setdefault("color", pard["DEFAULT_COLOR"])
    args.setdefault("seq_num", "0")
    args.setdefault("status", "")
    return args


def get_sid_user(pard):
    pard.setdefault("user_data", {})
    if pard["user_data"]:
        return str(pard["user_data"]["user_id"])
    else:
        return pard.get("REMOTE_IP_ADDR", "localhost")


def check_args(pard, args):
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    args.setdefault("alias", "")
    args.setdefault("engine", "mysql")
    args.setdefault("conn_type", "Standard")
    args.setdefault("tunnel_id", 0)
    args.setdefault("user", "")
    args.setdefault("host", "")
    args.setdefault("db", "")
    args.setdefault("passw", "")
    args.setdefault("color", pard["DEFAULT_COLOR"])
    args.setdefault("seq_num", "0")
    args.setdefault("status", "")
    if "id" in args and args["id"]:
        args["id"] = int(args["id"])

    if not args["alias"]:
        args["alias"] = tools.inventa_nome()

    args["alias"] = args["alias"].replace(" ", "_")
    try:
        args["alias"] = tools.ascii_string(args["alias"], "Alias", lang="it")
    except Exception as err:
        raise InvalidData(str(err))

    for k in ("alias", "engine", "conn_type", "db", "color"):
        if not args[k]:
            raise InvalidData("A value is required for the key `%s`" % k)

    if args["conn_type"] not in conn_type_list:
        raise InvalidData("Invalid Connection Type")

    if args["engine"] not in engine_list:
        raise InvalidData("Data Base engine not implemented")

    if args["engine"] in ("mysql", "click") and not args["user"]:
        raise InvalidData("A value is required for the key `user`")

    try:
        args["seq_num"] = tools.to_int(args["seq_num"], "Sequence Number", lang="en")
    except Exception as err:
        raise InvalidData(str(err))

    if args["status"] not in ("", "disabled"):
        raise InvalidData("Invalid `Status`")

    if not args["tunnel_id"]:
        args["tunnel_id"] = 0
    args["tunnel_id"] = int(args["tunnel_id"])
    if args["conn_type"] == "SSH":
        if args["tunnel_id"]:
            rec_tunnel = tunnel.get_by_id(pard, {"id": args["tunnel_id"]})
            if not rec_tunnel:
                raise NotFound("Tunnel id `%s` not found" % args["tunnel_id"])
            else:
                args["host"] = "127.0.0.1:%s" % rec_tunnel["local_bind_port"]

    return args


def insert(pard, args):
    args = check_args(pard, args)

    rec = get_by_alias(pard, args)
    if rec:
        raise DuplicateKey("Duplicate alias name for %(mw_user)s" % args)

    sql = """
        insert into prefs
            (mw_user, alias, engine, conn_type, tunnel_id,
             user, host, db, passw, color, seq_num, status)
        values
            (:mw_user, :alias, :engine, :conn_type, :tunnel_id,
             :user, :host, :db, :passw, :color, :seq_num, :status)
        """
    prefs_id = pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return prefs_id


def make_a_copy(pard, args):
    rec = get_by_id(pard, args)
    if not rec:
        raise NotFound("Record not found")
    rec["alias"] = tools.inventa_nome()
    rec["prefs_id"] = insert(pard, rec)
    return rec


def get_by_id(pard, args):
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    args.setdefault("id", 0)
    sql = "select * from prefs where id = :id and mw_user = :mw_user"
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_by_alias(pard, args):
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    args.setdefault("alias", "")
    sql = "select * from prefs where alias = :alias and mw_user = :mw_user"
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_by_value(pard, args):
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)

    args.setdefault("conn_type", "")
    args.setdefault("engine", "")
    args.setdefault("tunnel_id", "")
    if not args["tunnel_id"]:
        args["tunnel_id"] = 0
    args.setdefault("user", "")
    args.setdefault("passw", "")
    args.setdefault("host", "")
    args.setdefault("db", "")

    sql = """
        select
            *
        from
            prefs
        where
            mw_user = :mw_user
            and conn_type = :conn_type
            and engine = :engine
            and tunnel_id = :tunnel_id
            and user = :user
            and passw = :passw
            and host = :host
            and db = :db
        """
    result = pard["sqlite.conn"].query(sql, args)
    return result[0] if result else {}


def get_list(pard, args=None):
    if not args:
        args = {}
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    args.setdefault("status", "all")
    sql = """
        select
            a.*, b.name as tunnel_name
        from
            prefs a
        left join
            tunnel b
            on a.tunnel_id = b.id
        where
            a.mw_user = :mw_user
        order by
            a.status, a.seq_num, a.engine, a.alias
        """
    result = pard["sqlite.conn"].query(sql, args)
    if args["status"] == "active":
        result = [rec for rec in result if rec["status"] == ""]
    return result


def update(pard, args):
    args = check_args(pard, args)
    rec = get_by_id(pard, args)
    if not rec:
        raise NotFound("Record not found")

    rec = get_by_alias(pard, args)
    if rec and rec["id"] != args["id"]:
        raise DuplicateKey("Duplicate preferences for %(alias)s" % args)

    sql = """
    update
        prefs
    set
        mw_user = :mw_user,
        alias = :alias,
        engine = :engine,
        conn_type = :conn_type,
        tunnel_id = :tunnel_id,
        user = :user,
        host = :host,
        db = :db,
        passw = :passw,
        color = :color,
        seq_num = :seq_num,
        status = :status
    where
        id = :id
        """
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return args


def delete(pard, args):
    args.setdefault("id", 0)
    mw_user = get_sid_user(pard)
    args.setdefault("mw_user", mw_user)
    sql = "delete from prefs where id = :id and mw_user = :mw_user"
    pard["sqlite.conn"].execute(sql, args)
    pard["sqlite.conn"].commit()
    return None


def load_user_auth(pard):
    pard.setdefault("user_data", {})
    pard.setdefault("alias", "")
    if not pard["user_data"] or not pard["alias"]:
        return False
    pard["mw_user"] = str(pard["user_data"]["user_id"])
    args = {"mw_user": pard["mw_user"], "alias": pard["alias"]}
    rec = get_by_alias(pard, args)
    if not rec:
        return False
    pard["USER"] = rec["user"]
    pard["PASSWORD"] = rec["passw"]
    pard["HOST"] = rec["host"]
    pard["DB"] = rec["db"]
    pard["ALIAS"] = rec["alias"]
    pard["COLOR"] = rec["color"]
    pard["CONN_TYPE"] = rec["conn_type"]
    pard["ENGINE"] = rec["engine"]
    pard["TUNNEL_ID"] = rec["tunnel_id"]

    ## Don't allow access to sqlite local DB if user is not administrator
    if pard["ENGINE"] == "sqlite" and pard["user_data"]["admin"] != "Y":
        return False

    return True


def _insert_default_user(pard):
    pard.setdefault("DEFAULT_USER", None)
    if not pard["DEFAULT_USER"]:
        print("Please, specify valid email adress in DEFAULT_USER parameter")
        raise SystemExit
    pard["sqlite.conn"] = qlite.DB(pard["SQLITE_DB"])
    result = users.get_list(pard)
    if not result:
        rec = {}
        rec["email"] = pard["DEFAULT_USER"]  # 'admin@mysqlweb.dev'
        rec["admin"] = "Y"
        rec["password"] = tools.inventa_nome()
        rec["retype_password"] = rec["password"]
        password = rec["password"]
        rec["sid"] = "change_password"
        user_id = users.insert(pard, rec)
        msg = f"""
            *********************************************************
            Administrator user "{pard['DEFAULT_USER']}" has been entered
            with the temporary password "{password}".
            You will be asked to change it the first time you log in.
            *********************************************************
            """
        msg = msg.replace("\t", "")
        print(msg)

        ## Move preference stored as 127.0.0.1 to admin
        sql = f"""
            update
                prefs
            set
                mw_user = '{user_id}'
            where
                mw_user = '127.0.0.1'
            """
        pard["sqlite.conn"].execute(sql)
        pard["sqlite.conn"].commit()

    ## set_default_user
    default_user = users.get_default_user(pard)
    if not default_user or default_user["email"] != pard["DEFAULT_USER"]:
        print("set default user to %s" % pard["DEFAULT_USER"])
        users.set_default_user(pard, {"email": pard["DEFAULT_USER"]})

    pard["sqlite.conn"].close()


# -------------------------------------------------------------------------- #
def test_database(pard, drop=False):
    db = qlite.DB(pard["SQLITE_DB"], db_structure)
    db.test_database(drop)
    db.exec_alter_table(db_alter)
    _insert_default_user(pard)


def _get_action(action):
    try:
        action = globals()[action]
    except KeyError:
        raise Exception("Funzione `%s` non presente nel modulo" % action)
    if not hasattr(action, "__call__"):
        raise Exception("Funzione `%s` non presente nel modulo" % action)
    return action


def _get_param(params):
    qs = params
    params = parse_qs(qs)
    params = {k: v[0] if len(v) == 1 else v for k, v in params.items()}
    return params


def run():
    import config

    pard = config.global_parameters({})
    action = _get_action(sys.argv[1])
    params = sys.argv[2] if len(sys.argv) > 2 else ""
    args = _get_param(params)
    print(action.__name__, params, sep=" -> ")
    pard["sqlite.conn"] = qlite.DB(pard["sqlite.conn"])
    result = action(pard, args)
    pard["sqlite.conn"].close()
    pprint.pprint(result)


if __name__ == "__main__":
    run()
