import pprint
import os
import shutil
import yaml
import wsgian

from . import tools
from .db.dbapi import DatabaseError


def default_config():
    d = {
        "HTTP_PORT": 8088,
        "MAX_ROW": 100,
        "COOKIE_EXPIRES_DAYS": 10,
        "DEFAULT_USER": "admin@mysqlweb.dev",
        "LOCAL_MODE": True,
    }
    return d


def copy_previus_data_dir():
    if "MYSQLWEB_DIR" in os.environ:
        return os.environ["MYSQLWEB_DIR"]
    base_dir = os.path.expanduser("~/.mysqlweb")
    if os.path.isdir(base_dir):
        return base_dir
    else:
        import readline  # noqa: F401

        resp = input(
            "\n"
            "Enter the base directory of previus installation of MySQLweb\n"
            "EG: ~/devel/mysqlweb\n"
            "Leave empty if you run mysqlweb for the first time or you'd like to start"
            "with default configuration\n"
        )
        resp = os.path.expanduser(resp)
        if not resp:
            return base_dir
        if os.path.isdir(f"{resp}/data"):
            tools.check_dir(base_dir)
            shutil.copytree(f"{resp}/data", f"{base_dir}/data")
            print(f"Successfully copied {resp}/data to {base_dir}/data")
        else:
            print(f"Directory Not Found: {resp}/data")
            raise SystemExit
        if os.path.isfile(f"{resp}/site_config.yaml"):
            shutil.copy2(f"{resp}/site_config.yaml", base_dir)
            print(f"Successfully copied {resp}/site_config.yaml to {base_dir}")


def global_parameters(pard):
    # Per cambiare la directory contenente i data file i log e l'eventuale
    # site_config.yaml personalizzato:
    #
    # export MYSQLWEB_DIR='~/pybin/mysqlweb'

    if "MYSQLWEB_DIR" in os.environ:
        pard["BASE_DIR"] = os.environ["MYSQLWEB_DIR"]
    else:
        pard["BASE_DIR"] = "~/.mysqlweb"
    pard["BASE_DIR"] = os.path.expanduser(pard["BASE_DIR"])

    pard["MODULE_DIR"] = os.path.dirname(__file__)
    pard["LOG_DIR"] = pard["BASE_DIR"] + "/log"
    pard["DATA_DIR"] = pard["BASE_DIR"] + "/data"
    pard["HTML_DIR"] = pard["MODULE_DIR"] + "/html"
    pard["STATIC_DIR"] = pard["HTML_DIR"]
    pard["IMG_DIR"] = pard["HTML_DIR"] + "/img"
    pard["ICON_DIR"] = pard["HTML_DIR"] + "/icons"
    # pard['APPLICATION_LOG_FILE'] = pard['LOG_DIR'] + '/application_log.txt'
    pard["APPLICATION_LOG_FILE"] = "stdout"
    pard["SQL_LOG_FILE"] = pard["LOG_DIR"] + "/sql_log.txt"

    check_dir_tree(pard)

    site_config_yaml = pard["BASE_DIR"] + "/site_config.yaml"
    if site_config_yaml and os.path.isfile(site_config_yaml):
        buf = open(site_config_yaml).read()
        site_config = yaml.load(buf, Loader=yaml.CLoader)
    else:
        print("default_config")
        site_config = default_config()

    ## Per sicurezza carico da site_config tutte e sole le chiavi che servono
    for k in [
        "HTTP_PORT",
        "MAX_ROW",
        "ENABLE_EVAL",
        "COOKIE_EXPIRES_DAYS",
        "SQLITE_DB",
        "DEFAULT_USER",
        "LOCAL_MODE",
        "LOGLEVEL",
    ]:
        pard[k] = site_config.get(k)

    pard["APPSERVER"] = "/"

    # --------------------------------------------------------------- #
    pard["DEBUG"] = 1
    pard["SID_TIMEOUT"] = 28800
    pard["DEBUG_SQL"] = 0
    # pard["TUNNEL_MONITOR"] = True
    pard["PROJECT"] = "mw"
    pard["TITLE"] = "MySQLWeb"
    sqlweb_css = wsgian.mtime_url(pard, "sqlweb.css")
    pard["CSS"] = f'<link rel="stylesheet" href="{sqlweb_css}" type="text/css">'
    pard["DEFAULT_COLOR"] = "#2a68c8"
    # ------------------------------------------------------------- #

    ### wsgian ###
    if not pard["SQLITE_DB"]:
        pard["SQLITE_DB"] = pard["DATA_DIR"] + "/mysqlweb.sqlite"
    if not pard["COOKIE_EXPIRES_DAYS"]:
        pard["COOKIE_EXPIRES_DAYS"] = 10
    if not pard["LOCAL_MODE"]:
        pard["LOCAL_MODE"] = False
    pard["APP_ERROR_LIST"] = (DatabaseError,)

    return pard


def check_dir_tree(pard):
    for key in ["BASE_DIR", "LOG_DIR", "DATA_DIR"]:
        tools.check_dir(pard[key])


def check_db(pard):
    from . import prefs
    from . import tunnel

    prefs.test_database(pard)
    tunnel.test_database(pard)


if __name__ == "__main__":
    # result = global_parameters({})
    result = copy_previus_data_dir()
    pprint.pprint(result)
