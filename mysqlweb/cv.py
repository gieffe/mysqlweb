import datetime
import decimal
import json

# import pprint
import textwrap
import wsgian

from . import home
from . import prefs
from . import db
from . import tools
from . import qq
from . import sql_info
from .session_data import Sdata
from .db.dbapi import DatabaseError
from wsgian import utils


def main(pard):
    if not prefs.load_user_auth(pard):
        pard["redirect"] = "/not_authorized"
        return pard
    sdata = Sdata(pard["sqlite.conn"], "%(mw_user)s::%(ALIAS)s" % pard)
    pard["userbar"] = home.render_userbar(pard)
    pard["action"] = pard.get("action", "start")
    pard["menu"] = qq.render_menu(pard)
    pard["result_msg"] = ""
    pard["main_body"] = ""
    pard["sidebar"] = ""
    view_type = sdata.get("view_type", default="content")
    pard["toolbar"] = render_toolbar(pard, view_type)
    pard["content_toolbar"] = ""
    pard.setdefault("max_row", pard["MAX_ROW"])
    try:
        pard["MAX_ROW"] = int(pard["max_row"])
    except ValueError:
        pard["max_row"] = pard["MAX_ROW"]
    pard[
        "javascript"
    ] = """
        <script src="/static/javascripts/htmx.min.js"></script>
        <script src="/static/javascripts/class-tools.js"></script>
    """

    # --------------------------- start ----------------------------
    if pard["action"] in ("start", "content_view", "reset"):
        pard.setdefault("selected_table", "")
        pard["table_search"] = sdata.get("table_search", default="")
        if pard["selected_table"]:
            sdata.set("selected_table", pard["selected_table"])
        else:
            pard["selected_table"] = sdata.get("selected_table", default="")
        if pard["selected_table"]:
            tl = db.db_access.get_db_tables_list(pard)
            if pard["selected_table"] not in tl:
                pard["selected_table"] = ""
                sdata.set("selected_table", "")
        pard["sidebar"] = render_sidebar(pard)
        if pard["selected_table"]:
            if view_type == "content":
                if pard["action"] == "reset":
                    flist = []
                    sdata.set("filter::%(selected_table)s" % pard, json.dumps(flist))
                else:
                    flist = sdata.get("filter::%(selected_table)s" % pard, [])
                    if flist and isinstance(flist, str):
                        flist = json.loads(flist)

                try:
                    result = get_table_data(
                        pard,
                        {
                            "table": pard["selected_table"],
                            "max_row": pard["max_row"],
                            "flist": flist,
                        },
                    )
                    pard["main_body"] = render_result(pard, result)
                except DatabaseError as e:
                    error_message = str(e)
                    pard[
                        "main_body"
                    ] = f"""
                        <div id="result_area">
                            <h3 style="color: red;">{error_message}</h3>
                        </div>
                    """

                pard["content_toolbar"] = render_content_toolbar(pard, flist)

            elif view_type == "structure":
                pard["main_body"] = render_table_description(pard)

    # --------------------------- filter_result -----------------------------
    elif pard["action"] == "filter_result":
        flist = get_filters_from_params(pard)
        try:
            result = get_table_data(
                pard,
                {
                    "table": pard["table_name"],
                    "max_row": pard["max_row"],
                    "flist": flist,
                    "order_by": pard.get("order_by", ""),
                    "sort_descending": pard.get("sort_descending", "N"),
                },
            )
            pard["html"] = render_result(pard, result)
            save_filter(pard, flist)

        except DatabaseError as e:
            error_message = str(e)
            pard["status"] = 400
            pard["html"] = error_message

        return pard

    # --------------------------- display_sql -----------------------------
    elif pard["action"] == "display_sql":
        flist = get_filters_from_params(pard)
        result = get_table_data(
            pard,
            {
                "table": pard["table_name"],
                "max_row": pard["max_row"],
                "flist": flist,
                "order_by": pard.get("order_by", ""),
                "sort_descending": pard.get("sort_descending", "N"),
            },
            sql_only=True,
        )
        pard["html"] = render_display_sql(pard, result)
        return pard

    # --------------------------- get_table_list ----------------------------
    elif pard["action"] == "get_table_list":
        sdata.set("table_search", pard["table_search"])
        pard["selected_table"] = sdata.get("selected_table", default="")
        pard["html"] = render_tables_list(pard)
        return pard

    # --------------------------- set_view_type ----------------------------
    elif pard["action"] == "set_view_type":
        sdata.set("view_type", pard["view_type"])
        pard["redirect"] = "/cv/%(ALIAS)s" % pard
        return pard

    # ------------ show_create_table ------------------------------
    elif pard["action"] == "show_create_table":
        pard["html"] = render_show_create_table(pard)
        return pard

    # ------------ display_table_description ----------------------
    elif pard["action"] == "display_table_description":
        pard["html"] = render_table_description(pard)
        return pard

    # ------------ add_filter -------------------------------------
    elif pard["action"] == "add_filter":
        pard["table_name"] = sdata.get("selected_table", default="")
        flist = get_filters_from_params(pard)
        flist.append({"table_column": "", "table_operator": "", "search_value": ""})
        pard["html"] = (
            render_table_filters(pard, flist)
            + render_reset_filter(pard, oob=True)
            + render_btn_display_sql(pard, oob=True)
        )
        return pard

    # ------------ delete_filter -------------------------------------
    elif pard["action"] == "delete_filter":
        pard["table_name"] = sdata.get("selected_table", default="")
        flist = get_filters_from_params(pard)
        flist.pop(int(pard["ind"]))
        sdata.set("filter::%(table_name)s" % pard, json.dumps(flist))
        pard["html"] = render_table_filters(pard, flist)
        return pard

    # ----------------- get_row -------------------------------------
    elif pard["action"] == "get_row":
        pard["table_name"] = sdata.get("selected_table", default="")
        pk = utils.cgi_params(pard, "pk")
        result = get_row(pard, pk)
        pard["html"] = render_edit_row(pard, result, pk)
        return pard

    # ----------------- allow_edit ----------------------------------
    elif pard["action"] == "allow_edit":
        pard["html"] = (
            """\
            <a  href="#"
                id="allow_changes_btn"
                hx-get="/cv/%(ALIAS)s?action=prevent_edit"
                title="Click the enter in view mode"
                hx-swap="outerHTML"
                hx-target="#allow_changes_btn"
            >
            <img src="/static/icons/application_form_delete.png" border="0">
            <input type="hidden" name="allow_edit" id="allow_edit" value="Y" />
            </a>
        """
            % pard
        )
        selected_row = pard.get("selected_row", "")
        if selected_row:
            pard["header"] = [
                ("Content-type", "text/html"),
                ("HX-Trigger", '{"selRow":{"target" : "#%s"}}' % selected_row),
            ]
        return pard

    # ----------------- prevent_edit ----------------------------------
    elif pard["action"] == "prevent_edit":
        pard["html"] = (
            """\
            <a  href="#"
                id="allow_changes_btn"
                hx-get="/cv/%(ALIAS)s?action=allow_edit"
                title="Click to enter in edit mode"
                hx-include="#selected_row"
                hx-swap="outerHTML"
                hx-target="#allow_changes_btn"
            >
            <img src="/static/icons/application_form_edit.png" border="0">
            <input type="hidden" name="allow_edit" id="allow_edit" value="N" />
            </a>
        """
            % pard
        )
        return pard

    # ----------------- update_field ----------------------------------
    elif pard["action"] == "update_field":
        pk = utils.cgi_params(pard, "pk")
        value = pard[pard["field_name"]]
        args = {
            "table_name": pard["table_name"],
            "field_name": pard["field_name"],
            "pk": pk,
            "field_value": value,
        }
        result = set_field_value(pard, args)
        if result["error"]:
            msg = result["error"]
            cls = "err"
        else:
            msg = "%(table_name)s.%(field_name)s updated successfully" % args
            cls = "msg"
        pard[
            "html"
        ] = f"""\
            <span
                id="save_row_result_msg"
                class="{cls}"
                classes="add is-hidden:3s"
            >{msg}</span>
        """
        return pard

    # ----------------- delete_record ----------------------------------
    elif pard["action"] == "delete_record":
        pk = utils.cgi_params(pard, "pk")
        args = {
            "table_name": pard["table_name"],
            "pk": pk,
        }
        result = delete_record(pard, args)
        if result["error"]:
            msg = result["error"]
            cls = "err"
        else:
            msg = f"%(table_name)s - {pk} - deleted successfully" % args
            cls = "msg"
        pard["html"] = render_delete_record(pard, msg, cls)
        return pard

    else:
        pard["result_msg"] = tools.format_messaggio(
            'Function not available. ("%(action)s")' % pard
        )
        pard[
            "main_body"
        ] = """\
            <input
                type="button"
                name="submit_form"
                value="Back"
                onclick="javascript:history.go(-1);"
            />
        """

    pard["html"] = render_page(pard)
    return pard


def javascript(pard):
    js = """
        <script>

/*
        const live = function(event_name, selector, callback, allow_default) {
            document.addEventListener(event_name, function(event){
                let element = event.target.closest(selector);
                if (element) {
                    if(!allow_default) event.preventDefault();
                    callback(element, event);
                }
            });
        };
 */

        const select_row = function (ele) {
            var row_sel = htmx.find('#result_area tr.selected');
            if (row_sel) {
                htmx.removeClass(row_sel, 'selected');
            };
            htmx.addClass(ele, 'selected');
            var pk = ele.getAttribute('hx-vals');
            document.getElementById('selected_row_pk').value = pk;
            document.getElementById('selected_row').value = ele.id;
            var allow_edit = document.getElementById('allow_edit');
            if (allow_edit.value == 'Y') {
               htmx.trigger(ele, 'selRow');
            };
        };
        const close_edit_box = function () {
            var el = document.getElementById('edit-box');
            el.style.display = "none";
            el.innerHTML = "";
            var ct_form = document.getElementById('content_toolbar_form');
            htmx.trigger(ct_form, 'submit');
        };
        const set_order_by = function (field_name) {
            var el = document.getElementById('order_by');
            var sd = document.getElementById('sort_descending');
            if (el.value == field_name) {
                if (sd.value == 'N') {
                    sd.value = 'S';
                }
                else {
                    sd.value = 'N';
                };
            }
            else {
                el.value = field_name;
                sd.value = 'N';
            }
            var ct_form = document.getElementById('content_toolbar_form');
            htmx.trigger(ct_form, 'submit');
        };
        const set_placeholder = function (ele, ind) {
            var el_in = document.getElementById("search_value[" + ind + "]");
            var operator = ele.value;
            if (operator == "IN" || operator == "NOT IN") {
                el_in.setAttribute("placeholder", "1, 2, 3");
            }
            else if (operator == "IS NULL" || operator == "IS NOT NULL") {
                el_in.setAttribute("placeholder", "");
            }
            else if (operator == "BETWEEN" || operator == "NOT BETWEEN") {
                el_in.setAttribute("placeholder", "1 AND 100");
            }
            else if (operator == "REGEXP" || operator == "NOT REGEXP") {
                el_in.setAttribute("placeholder", "Pattern");
            }
            else {
                el_in.setAttribute("placeholder", "EMPTY");
            };
        };

/*         live('click', '.data-row', select_row); */

        </script>
    """
    return js


def render_page(pard):
    sqlweb_css = wsgian.mtime_url(pard, "sqlweb.css")
    sqlweb_cv_css = wsgian.mtime_url(pard, "sqlweb_cv.css")
    pard[
        "CSS"
    ] = f"""\
        <link rel="stylesheet" href="{sqlweb_css}" type="text/css">
        <link rel="stylesheet" href="{sqlweb_cv_css}" type="text/css">
    """
    pard["melacjs"] = wsgian.mtime_url(pard, "javascripts/melac.js")
    pard["javascript"] += javascript(pard)
    html = """
        <html>
            <head>
                <meta charset="utf-8"></meta>
                %(CSS)s
                <title>%(TITLE)s</title>
                <link rel="icon" href="/static/favicon.ico">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/stackoverflow-light.min.css">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js"></script>
                <script src="%(melacjs)s"></script>
                %(javascript)s
                <script>
                    htmx.on("htmx:responseError", function(evt) {
                        if (evt.detail.xhr.status == 400) {
                            alert(evt.detail.xhr.responseText);
                        }
                        else {
                            alert("Unexpected error: " + evt.detail.error);
                        };
                    });
                </script>
            </head>
            <body>
                <div id="userbar">%(userbar)s</div>
                %(menu)s
                %(toolbar)s
                %(result_msg)s
                <table width="100%%" class="col2">
                <tr>
                    <td width="18%%" style="vertical-align: top;">
                        %(sidebar)s
                    </td>
                    <td width="82%%" style="vertical-align: top;">
                        %(content_toolbar)s
                        <div id="edit-box" style="display: none;"></div>
                        %(main_body)s
                    </td>
                </tr>
                </table>
            </body>
        </html>
    """  # noqa: E501
    html = html % pard
    return html


def render_sidebar(pard):
    pard["table_search"] = pard.get("table_search", "")
    pard["tables_list"] = render_tables_list(pard)
    html = (
        """
    <table width="100%%" class="sidebar">
    <tr>
        <td>
            <input type="text" name="table_search" value="%(table_search)s"
                id="table_filter"
                hx-get="/cv/%(ALIAS)s?action=get_table_list"
                hx-trigger="keyup changed delay:500ms"
                hx-target="#tables_list_area"
                placeholder="Filter..."
                spellcheck="false"
                autocomplete="off"
            >
        </td>
    </tr>
    <tr>
        <td id="tables_list_area" style="vertical-align:top;">
            <div>%(tables_list)s</div>
        </td>
    </tr>
    </table>
        <script type="text/javascript" language="javascript" charset="utf-8">
        document.getElementById('table_filter').focus();
    </script>
    """
        % pard
    )
    return html


def render_tables_list(pard):
    tables_list = db.db_access.get_db_tables_list_like(pard)
    if pard["selected_table"] and pard["selected_table"] not in tables_list:
        tables_list.insert(0, pard["selected_table"])
    h = ['<ul id="table_list" hx-boost="true">']
    for table in tables_list:
        if table == pard["selected_table"]:
            selected = "selected"
        else:
            selected = ""
        h.append(
            f"""
            <li class="easy_read {selected}">
              <a
                 href="/cv/{pard['ALIAS']}?action=content_view&selected_table={table}"
                 class="navlink"
              />
                <img src="/static/icons/table.png" style="float: left;"/>
                <span style="margin-left: 6px;">{table}</span>
              </a>
            </li>
            """
        )
    h.append("</ul>")
    return "\n".join(h)


def get_table_data(pard, args, sql_only=False):
    args.setdefault("table", "")
    args.setdefault("max_row", 100)
    args.setdefault("flist", [])
    args.setdefault("order_by", "")
    args.setdefault("sort_descending", "N")
    if not args["table"]:
        return []
    conds = []
    for rec in args["flist"]:
        if rec["table_column"]:
            if rec["table_operator"] in ("IN", "NOT IN"):
                conds.append(
                    "`%(table_column)s` %(table_operator)s (%(search_value)s)" % rec
                )
            elif rec["table_operator"] in ("BETWEEN", "NOT BETWEEN"):
                conds.append(
                    "`%(table_column)s` %(table_operator)s %(search_value)s" % rec
                )
            elif rec["table_operator"] in ("IS NULL", "IS NOT NULL"):
                conds.append("`%(table_column)s` %(table_operator)s" % rec)
            else:
                conds.append(
                    "`%(table_column)s` %(table_operator)s '%(search_value)s'" % rec
                )

    if conds:
        where = "where\n            " + "\n            and ".join(conds)
    else:
        where = ""

    order_by = ""
    if args["order_by"]:
        if args["sort_descending"] == "S":
            order_by = f"order by `{args['order_by']}` desc"
        else:
            order_by = f"order by `{args['order_by']}`"

    pard[
        "query"
    ] = f"""\
        select
            *
        from
            `{args['table']}`
        {where}
        {order_by}
        limit
            {args['max_row']}
        """
    # logging.debug(pard['query'])
    if sql_only:
        return pard["query"]

    pard["sql_query"] = pard["query"]
    pard["sql_info"] = sql_info.get_sql_info(pard)
    pard["sql"] = pard["query"]
    result = db.db_access.send_dict(pard)
    return result


def render_result(pard, result):
    pard.setdefault("order_by", "")
    pard.setdefault("sort_descending", "N")
    desc_icon = '<img src="/static/icons/arrow_down.png" border="0">'
    h = []
    h.append('<div id="result_area">')
    if result["rows"]:
        h.append('<table width="100%">')
        h.append("<tr>")
        h.append('<th scope="col">&nbsp;</th>')
        for col in result["c_name"]:
            selected = "selected" if col == pard["order_by"] else ""
            arrow = desc_icon if selected and pard["sort_descending"] == "S" else ""
            h.append(
                f"""
                <th
                    scope="col"
                    class="data-row-header {selected}"
                    onClick="set_order_by('{col}');"
                    title="Sort"
                >{col} {arrow}</th>
            """
            )
        h.append("</tr>")

        sel_pk = pard.get("selected_row_pk", {})
        if sel_pk:
            try:
                sel_pk = json.loads(sel_pk)
            except Exception:
                sel_pk = {}
        if not isinstance(sel_pk, dict):
            sel_pk = {}

        row_num = 1
        for rec in result["rows"]:
            # encode primary key in a json object
            pk_dict = {}
            for key in pard["sql_info"]["primary_key"]:
                if isinstance(rec[key], datetime.datetime) or isinstance(
                    rec[key], decimal.Decimal
                ):
                    rec[key] = str(rec[key])
                pk_dict[f"pk[{key}]"] = rec[key]
            pk_json = json.dumps(pk_dict)

            # is this row selected row?
            if sel_pk:
                selected = "selected"
                for key in pard["sql_info"]["primary_key"]:
                    if sel_pk[f"pk[{key}]"] != pk_dict[f"pk[{key}]"]:
                        selected = ""
                        break

            rec = utils.cgi_escape(rec)
            if pk_dict:
                h.append(
                    f"""
                    <tr valign="top" class="data-row {selected}" id="r{row_num}"
                        hx-get="/cv/%(ALIAS)s?action=get_row"
                        hx-target="#edit-box"
                        hx-swap="outerHTML"
                        hx-trigger="selRow queue:all"
                        hx-indicator="#spinner"
                        hx-vals='{pk_json}'
                        onClick="select_row(this);"
                    >
                """
                    % pard
                )
            else:
                h.append(
                    """
                    <tr valign="top" class="data-row">
                """
                )

            h.append(
                '<th style="text-align: right; width: 20px;">%s</th>' % str(row_num)
            )
            for key in result["c_name"]:
                val = str(rec[key])
                # if val.find('\n') > -1:
                #     val = '<pre>%s</pre>' % val
                h.append('<td class="truncate">%s</td>' % val)
            h.append("</tr>")
            row_num += 1
        h.append("</table>")
    h.append("</div> <!-- result_area -->")
    h = "\n".join(h)
    return h


def render_display_sql(pard, sql):
    sql = textwrap.dedent(sql)
    # sql = utils.format_as_pre(sql)
    h = f"""\
        <table style="width: 100%;">
            <tr>
                <th>
                    <img
                        src="/static/img/uncheck.png"
                        border="0"
                        onClick="document.getElementById('sql_area').innerHTML = '';"
                    />
                    &nbsp;&nbsp;&nbsp;SQL&nbsp;&nbsp;&nbsp;
                    <span id="clipboard_msg"></span>
                    <button
                        type="button"
                        style="float: right;"
                        title="Copy to clipboard"
                        onclick="copy_to_clipboard('code_text', 'clipboard_msg');"
                    >
                        <img src="/static/img/clippy.svg" width="13" alt="Copy to clipboard">
                    </button>
                    <textarea name="hide" id="code_text" style="display:none;">{sql}</textarea>
                </th>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <pre><code id="code_view" class="language-SQL">{sql}</code></pre>
                </td>
            </tr>
        </table>
        <script>
            var code_view_el = document.getElementById('code_view');
            hljs.highlightElement(code_view_el);
        </script>
    """  # noqa: E501
    # <div class="vtop dont-break-out language-sql" id="sql-code">{sql}</div>
    return h


def render_toolbar(pard, view_type):
    pard["btn_content"] = (
        """
        <a href="/cv/%(ALIAS)s?action=set_view_type&view_type=content">
        <img src="/static/icons/application_view_columns.png" border="0"><br>
        Content
        </a>
    """
        % pard
    )
    pard["btn_structure"] = (
        """
        <a href="/cv/%(ALIAS)s?action=set_view_type&view_type=structure">
        <img src="/static/icons/wrench.png" border="0"><br>
        Structure
        </a>
    """
        % pard
    )
    pard["btn_query"] = (
        """
        <a href="/qq/%(ALIAS)s" title="Query">
        <img src="/static/icons/page_white_code_red.png" border="0"><br>
        Query
        </a>
    """
        % pard
    )
    pard["btn_connection"] = (
        """
        <a href="#"
           title="Open Conneclion's List In a New Window"
           onClick="window.open('/prefs');return false;"
        >
        <img src="/static/icons/text_list_bullets.png" border="0"><br>
        Connection List
        </a>
        """
        % pard
    )

    pard["cls_content"] = ""
    pard["cls_structure"] = ""
    if view_type == "content":
        pard["cls_content"] = "selected"
    elif view_type == "structure":
        pard["cls_structure"] = "selected"

    h = (
        """

    <div id="toolbar">
    <table class="action" width="100%%">
    <tr>
        <td width="196">%(btn_connection)s</td>
        <td width="80" class="%(cls_content)s">%(btn_content)s</td>
        <td width="80" class="%(cls_structure)s">%(btn_structure)s</td>
        <td width="80">%(btn_query)s</td>
        <td style="text-align: left;">
            <span id="progress_span">&nbsp;</span>
        </td>
    </tr>
    </table>
    </div><!-- id="toolbar" -->

    """
        % pard
    )
    return h


def render_content_toolbar(pard, flist=None):
    if not flist:
        flist = get_filters_from_params(pard)
    table_status = db.db_access.show_table_status(pard)
    if table_status:
        trows = int(table_status["Rows"])
        table_rows = f"""\
            <td class="small" style="font-weight: normal;">table rows<br> {trows:,}</td>
        """
    else:
        table_rows = ""
    table_filters = render_table_filters(pard, flist)

    if not flist:
        btn_filter = (
            """
            <button
                class="btn"
                id="btn_filter"
                hx-get="/cv/%(ALIAS)s?action=add_filter"
                hx-target="#table_filters"
                hx-swap="outerHTML"
                hx-include="#table_filters"
            >
                Filter
            </button>
        """
            % pard
        )
        btn_display_sql = '<span id="btn_display_sql"></span>'
    else:
        btn_filter = render_reset_filter(pard)
        btn_display_sql = render_btn_display_sql(pard)

    allow_changes_btn = (
        """
        <a  href="#"
            id="allow_changes_btn"
            hx-get="/cv/%(ALIAS)s?action=allow_edit"
            hx-include="#selected_row"
            title="Click to enter in edit mode"
            hx-swap="outerHTML"
            hx-target="#allow_changes_btn"
        >
        <img src="/static/icons/application_form_edit.png" border="0">
        <input type="hidden" name="allow_edit" id="allow_edit" value="N" />
        </a>
    """
        % pard
    )
    ALIAS = pard["ALIAS"]
    h = f"""
    <div id="content_toolbar">
    <form hx-post="/cv/{ALIAS}/filter_result"
          hx-target="#result_area"
          hx-swap="outerHTML"
          hx-indicator="#spinner"
          style="margin-bottom: 0px;"
          id="content_toolbar_form"
    >
    <input type="hidden" name="table_name" value="{pard['selected_table']}" />
    <input type="hidden" name="order_by" id="order_by" value="" />
    <input type="hidden" name="sort_descending" id="sort_descending" value="N" />
    <input type="hidden" name="selected_row_pk" id="selected_row_pk" value="" />
    <input type="hidden" name="selected_row" id="selected_row" value="" />
    <table class="action" width="100%%">
    <tr>
        <td style="text-align: left; min-width: 400px;">
            <div id="sql_area" class="html_title" style="width: 400px;"></div>
            {table_filters}
        </td>
        <td>
            {btn_display_sql}
            {btn_filter}
        </td>
        <td style="text-align: right; width: 180px;">
            <span>
            Limit:&nbsp;
            <input type="text" name="max_row" value="{pard['max_row']}" size="5" />
            &nbsp;
            </span>
            <img id="spinner" class="htmx-indicator" src="/static/img/progress.gif" />
            <button class="btn">Search</button>
            &nbsp;
        </td>
        <td>
            {allow_changes_btn}
        </td>
        {table_rows}
    </tr>
    </table>
    </form>
    </div><!-- id="content_toolbar" -->

    """
    return h


def render_reset_filter(pard, oob=False):
    oob = 'hx-swap-oob="true"' if oob else ""
    ALIAS = pard["ALIAS"]
    btn_filter = f"""
        <button
            id="btn_filter"
            type="button"
            class="btn"
            {oob}
            onclick="location.href = '/cv/{ALIAS}?action=reset';"
        >
            Reset Filter
        </button>
    """
    return btn_filter


def render_btn_display_sql(pard, oob=False):
    oob = 'hx-swap-oob="true"' if oob else ""
    ALIAS = pard["ALIAS"]
    html = f"""\
        <span id="btn_display_sql" {oob}>
            <a  href="#" hx-post="/cv/{ALIAS}/display_sql"
                hx-target="#sql_area"
                hx-swap="innerHTML"
                title="Display sql"
            >
                <img src="/static/icons/page_white_code.png"
                     border="0"
                     style="vertical-align: text-bottom;"
                />
            </a>
            &nbsp;
        </span>
    """
    return html


def get_filters_from_params(pard):
    table_column = utils.cgi_params(pard, "table_column", defdict=True)
    table_operator = utils.cgi_params(pard, "table_operator", defdict=True)
    search_value = utils.cgi_params(pard, "search_value", defdict=True)

    flist = []
    # if not table_column:
    #     table_column[0] = ""
    for k in table_column:
        flist.append(
            {
                "table_column": table_column[k],
                "table_operator": table_operator[k],
                "search_value": search_value[k],
            }
        )
    return flist


def save_filter(pard, flist):
    Sdata(pard["sqlite.conn"], "%(mw_user)s::%(ALIAS)s" % pard).set(
        "filter::%(table_name)s" % pard, json.dumps(flist)
    )


def render_table_filters(pard, flist):
    h = []
    h.append('<div id="table_filters">')

    col_list = db.db_access.show_columns(pard, out="list")
    ll = [(i, i) for i in col_list]
    ol = [
        ("=", "="),
        ("<>", "&lt;&gt;"),
        ("<", "&lt;"),
        (">", "&gt;"),
        ("<=", "&le;"),
        (">=", "&ge;"),
        ("IN", "IN"),
        ("NOT IN", "NOT IN"),
        ("IS NULL", "IS NULL"),
        ("IS NOT NULL", "IS NOT NULL"),
        ("BETWEEN", "BETWEEN"),
        ("NOT BETWEEN", "NOT BETWEEN"),
        ("LIKE", "LIKE"),
    ]
    if pard.get("ENGINE") == "mysql":
        ol.append(("REGEXP", "Contains"))
        ol.append(("NOT REGEXP", "Not contains"))
    filters = []
    for i, rec in enumerate(flist):
        pop_table_column = tools.make_pop(f"table_column[{i}]", ll, rec["table_column"])
        pop_table_operator = tools.make_pop(
            f"table_operator[{i}]",
            ol,
            rec["table_operator"],
            f""" onChange="set_placeholder(this, '{i}')" """,
        )
        val = rec["search_value"]
        add_icon = (
            """
            <a  href="#" hx-get="/cv/%(ALIAS)s?action=add_filter"
                hx-target="#table_filters"
                hx-swap="outerHTML"
                hx-include="#table_filters"
            >
                <img src="/static/icons/add.png" border="0" />
            </a>
        """
            % pard
        )
        delete_icon = (
            f"""
            <a  href="#" hx-get="/cv/%(ALIAS)s?action=delete_filter&ind={i}"
                hx-target="#table_filters"
                hx-swap="outerHTML"
                hx-include="#table_filters"
            >
                <img src="/static/icons/delete.png" border="0" />
            </a>
        """
            % pard
        )
        if i == 0 and len(flist) > 1:
            add_and_delete = delete_icon + add_icon
        elif i == 0:
            add_and_delete = add_icon
        else:
            add_and_delete = delete_icon
        e_val = utils.cgi_escape(val)
        placeholder = get_placeholder(rec["table_operator"])
        filters.append(
            f"""
            {pop_table_column}
            {pop_table_operator}
            <input
                type="text"
                id="search_value[{i}]"
                name="search_value[{i}]"
                value="{e_val}" size="30"
                spellcheck="false"
                autocomplete="off"
                placeholder="{placeholder}"
            />
            {add_and_delete}
        """
        )
    h.append("<br>".join(filters))
    h.append("</div>")
    return "\n".join(h)


def get_placeholder(operator):
    d = {
        "IN": "1, 2, 3",
        "NOT IN": "1, 2, 3",
        "IS NULL": "",
        "IS NOT NULL": "",
        "BETWEEN": "1 AND 100",
        "NOT BETWEEN": "1 AND 100",
        "REGEXP": "Pattern",
        "NOT REGEXP": "Pattern",
    }
    return d.get(operator, "EMPTY")


def render_table_description(pard):
    pard["table_name"] = pard["selected_table"]
    describe = db.db_access.show_columns(pard)
    primary_key, index_list = db.db_access.show_index(pard)

    h = [
        """
        <ul id="column_list">
            <li class="cl-header">
                <img
                    src="/static/img/table_gear.png"
                    border="0"
                    style="vertical-align: middle;"
                    title="show create table"
                    hx-get="/cv/%(ALIAS)s?action=show_create_table&table_name=%(table_name)s"
                    hx-target="#column_list"
                    hx-swap="outerHTML"
                > &nbsp;
                <b>%(table_name)s</b>
            </li>
        """
        % pard
    ]
    for rec in describe:
        if rec["Field"] in primary_key:
            rec["Field"] = "<b>%(Field)s</b>" % rec
            rec["Type"] = "<b>%(Type)s</b>" % rec
            rec["key_img"] = (
                """style="list-style-image: url('/static/img/key.png');" """
            )
        else:
            rec["key_img"] = ""
        rec["Type"] = rec["Type"].upper()
        h.append(
            """
            <li %(key_img)s>%(Field)s: %(Type)s <small><i>%(Extra)s</i></small></li>
            """
            % rec
        )

    for rec in index_list:
        if rec["Non_unique"]:
            rec["unique"] = ""
        else:
            rec["unique"] = "Unique"
        h.append(
            """
            <li style="list-style-image: url('/static/img/folder_key.png');">
                %(unique)s Index "%(index_name)s"
            </li>
            <ul>
            """
            % rec
        )
        for key in rec["columns"]:
            h.append(
                """<li style="list-style-image: url('/static/img/key.png');">%s</li>"""
                % key
            )
        h.append("</li></ul>")
    h.append("</ul>")
    return "\n".join(h)


def render_show_create_table(pard):
    pard["show_create_table"] = db.db_access.show_create_table(pard)
    h = (
        """
        <ul id="column_list">
            <li class="cl-header">
                <img
                    src="/static/img/table_gear.png"
                    border="0"
                    style="vertical-align: middle;"
                    title="Describe Table"
                    hx-get="/cv/%(ALIAS)s?action=display_table_description&selected_table=%(table_name)s"
                    hx-target="#column_list"
                    hx-swap="outerHTML"
                > &nbsp;
                <b>%(table_name)s</b>
            </li>
            <li style="list-style: none;">
            <pre>%(show_create_table)s</pre>
            </li>
        """
        % pard
    )
    return h


def get_row(pard, pk):
    sql = ["select * from %(table_name)s where 1 = 1" % pard]
    for key in pk:
        if isinstance(pk[key], str):
            sql.append("and `{}` = '{}'".format(key, pk[key].replace("'", "''")))
        else:
            sql.append("and `{}` = {}".format(key, str(pk[key])))
    pard["sql"] = "\n".join(sql)
    result = db.db_access.send_dict(pard)
    return result


def render_edit_row(pard, result, pk):
    h = []
    h.append('<div id="edit-box" hx-ext="class-tools" style="display: block;">')
    if result["rows"]:
        h.append('<table class="edit-box-table">')
        #
        #                 hx-get="/cv/%(ALIAS)s?action=close_edit_box"
        #                 hx-target="#edit-box"
        #                 hx-swap="outerHTML"
        #
        h.append(
            """
            <td colspan="2" style="text-align: center;">
            <span id="save_row_result_msg" class="msg"></span>
            <img src="/static/icons/cross.png"
                style="float: right;"
                onClick="close_edit_box();"
            />
            </td>
        """
            % pard
        )
        rec = result["rows"][0]
        rec = utils.cgi_escape(rec)
        pk_dict = {}
        for key in pk:
            if isinstance(rec[key], datetime.datetime) or isinstance(
                rec[key], decimal.Decimal
            ):
                rec[key] = str(rec[key])
            pk_dict[f"pk[{key}]"] = rec[key]
        pk_json = json.dumps(pk_dict)
        for key in result["c_name"]:
            h.append("<tr>")
            h.append(f'<td class="label">{key}</td>')
            if key in pk:
                h.append(f'<td class="pk">{rec[key]}</td>')
            else:
                val = str(rec[key])
                n_rows = len(val.split("\n"))
                h.append("<td>")
                url = f"/cv/%(ALIAS)s/%(table_name)s/{key}/update" % pard
                if n_rows > 1 or '"' in val:
                    h.append(
                        f"""
                        <textarea
                            name="{key}"
                            rows="{n_rows}"
                            hx-post="{url}"
                            hx-target="#save_row_result_msg"
                            hx-swap="outerHTML"
                            hx-trigger="keyup changed delay:1s"
                            hx-vals='{pk_json}'
                            spellcheck="false"
                            autocomplete="off"
                        >{val}</textarea>
                    """
                    )
                else:
                    h.append(
                        f"""
                        <input
                            type="text"
                            name="{key}"
                            value="{val}"
                            hx-post="{url}"
                            hx-target="#save_row_result_msg"
                            hx-swap="outerHTML"
                            hx-trigger="keyup changed delay:1s"
                            hx-vals='{pk_json}'
                            spellcheck="false"
                            autocomplete="off"
                        />
                    """
                    )
                h.append("</td>")
            h.append("</tr>")
        url_delete = "/cv/%(ALIAS)s/%(table_name)s/delete_record" % pard
        h.append(
            f"""\
            <tr>
              <td
                colspan="2"
                style="text-align: right; padding-top: 15px; padding-right: 15px;"
              >
                <button
                    class="btn"
                    hx-post="{url_delete}"
                    hx-vals='{pk_json}'
                    hx-confirm="Are you sure you want to delete this record?"
                    hx-target="#edit-box"
                    hx-swap="outerHTML"
                >
                    Delete Record
                </button>
              </td>
            </tr>
        """
        )
        h.append("</table>")
    h.append("</div>")
    return "\n".join(h)


def set_field_value(pard, args):
    result = {"error": ""}

    pard["table_name"] = args["table_name"]
    col_list = db.db_access.show_columns(pard, out="list")
    if args["field_name"] not in col_list:
        result["error"] = "%(field_name)s is not a column of %(table_name)s" % args
        return result

    args["field_value"] = str(args["field_value"]).replace("'", "''")
    sql = [
        """
        update
            `%(table_name)s`
        set
            `%(field_name)s` = '%(field_value)s'
    """
        % args
    ]

    if "upd_user" in col_list:
        sql.append(", upd_user = 'mysqlweb:%(USER)s'" % pard)

    sql.append("where 1 = 1 ")

    pk = args["pk"]
    for key in pk:
        if isinstance(pk[key], str):
            sql.append("and `{}` = '{}'".format(key, pk[key].replace("'", "''")))
        else:
            sql.append("and `{}` = {}".format(key, str(pk[key])))

    pard["sql"] = "\n".join(sql)
    pard["sql_info"] = {"query_command": "update"}
    try:
        db.db_access.send_dict(pard)
    except DatabaseError as e:
        error_message = f"{type(e).__name__}: {str(e)}"
        result["error"] = error_message
    return result


def delete_record(pard, args):
    result = {"error": ""}
    sql = [
        """
        delete from
            `%(table_name)s`
    """
        % args
    ]
    sql.append("where 1 = 1 ")
    pk = args["pk"]
    for key in pk:
        if isinstance(pk[key], str):
            sql.append("and `{}` = '{}'".format(key, pk[key].replace("'", "''")))
        else:
            sql.append("and `{}` = {}".format(key, str(pk[key])))

    pard["sql"] = "\n".join(sql)
    pard["sql_info"] = {"query_command": "delete"}
    try:
        db.db_access.send_dict(pard)
        # result["error"] = "<pre>" + pard["sql"] + "</pre>"
    except DatabaseError as e:
        error_message = f"{type(e).__name__}: {str(e)}"
        result["error"] = error_message
    return result


def render_delete_record(pard, msg="Done!", cls="msg"):
    h = f"""\
        <div id="edit-box" hx-ext="class-tools" style="display: block;">
        <div
            id="save_row_result_msg"
            class="{cls}"
        >
            {msg}
        </div>
        <div style="text-align: center; padding-top: 15px;">
        <button class="btn" onClick="close_edit_box();">
            Close
        </button>
        </div>
    """
    return h
