#
#  Web Query Browser
#
#  Authors: gianfranco
#           andrea bettelli (query autocomplete)
#
#  || ToDo ||
#  - Gestire errori in update (per adesso va bene cosi')
#  - Pagina HTML con Query History (Aggiungere filtro?)
#  -
#  - cgi escape di tutti i campi visualizzati
#  - in editing: se dentro c'e' un \n o \r textarea con cgi_escape
#                altrimenti cgi_escape con quote = true ... (Pier!)

import time
import os
import re
import datetime
import decimal
import six
import json

# import db_access
from . import db
from .db import dbapi
from . import prefs
from . import home
from . import sql_info
from . import tools
from .session_data import Sdata

from collections import UserDict
from wsgian import utils


class UTF8(UserDict):
    """Trasforma automaticamente chiavi da unicode ad utf8"""

    def __getitem__(self, key):
        value = super().__getitem__(key)
        if isinstance(value, str):
            return six.ensure_str(value, encoding="utf8")
        return value

    def __setitem__(self, key, value):
        if isinstance(key, dict) and not isinstance(key, UTF8):
            value = UTF8(value)
        return super().__setitem__(key, value)


def main(pard):
    # pard = UTF8(pard)
    if not prefs.load_user_auth(pard):
        pard["redirect"] = "/not_authorized"
        return pard
    sdata = Sdata(pard["sqlite.conn"], "%(mw_user)s::%(ALIAS)s" % pard)

    pard["userbar"] = home.render_userbar(pard)

    pard["module"] = "qq"
    pard["program"] = "main"
    pard["action"] = pard.get("action", "start")
    pard["menu"] = render_menu(pard)
    pard["search_form"] = ""
    pard["msg"] = []
    pard["result_msg"] = ""
    pard["main_body"] = ""
    pard[
        "javascript"
    ] = """
        <script src="/static/javascripts/prototype.js" type="text/javascript"></script>
        <script src="/static/javascripts/sql-formatter/sql-formatter.min.js" type="text/javascript"></script>
        <script src="/static/javascripts/melac.js" type="text/javascript"></script>
        """
    pard["javascript"] += ajax_js(pard)
    pard["javascript"] += test_keydown()
    pard["opensearch"] = ""  # opensearch(pard)
    if "max_row" in pard:
        try:
            pard["MAX_ROW"] = int(pard["max_row"])
        except ValueError:
            pard["max_row"] = pard["MAX_ROW"]
    enable_eval = pard.get("ENABLE_EVAL", False)

    # --------------------------- start ----------------------------
    if pard["action"] == "start":
        pard["query_area"] = sdata.get("query_area", default="")
        pard["query_area_height"] = sdata.get("query_area_height", default="135")
        pard["search_form"] = render_search_form(pard)
        pard["main_body"] = """<p>MySQL Query Navigator</p>"""

    # --------------------------- run_query ----------------------------
    elif pard["action"] == "run_query":
        sdata.save(
            {
                "query_area": pard["query_area"],
                "query_area_height": pard["query_area_height"],
            }
        )
        pard["query"] = pard["query"].lstrip()
        dec = re.compile(r"^\d+.?\d*$")
        pard["sql_query"] = pard["query"]
        pard["sql_info"] = sql_info.get_sql_info(pard)
        if pard["sql_info"]["query_command"] == "wsel":
            pard = wsel_query_wizard(pard)
        elif pard["sql_info"]["query_command"] == "wins":
            pard = wins_query_wizard(pard)
        elif pard["sql_info"]["query_command"] == "silk":
            pard = silk_icons(pard)
        elif pard["sql_info"]["query_command"] == "eval" and enable_eval:
            pard = eval_query(pard)
        elif dec.findall(pard["sql_info"]["query_command"]):
            pard["sql"] = "select " + pard["query"]
            pard = exec_query(pard)
        else:
            pard["sql"] = clean_query(pard["query"])
            pard = exec_query(pard)

    # --------------------------- download ----------------------------
    elif pard["action"] == "download":
        pard["query"] = pard["query"].lstrip()
        pard["sql_query"] = pard["query"]
        pard["sql_info"] = sql_info.get_sql_info(pard)
        pard["sql"] = clean_query(pard["query"])
        if pard["prev_fetchall"]:
            pard["MAX_ROW"] = "fetchall"
        pard["result"] = db.db_access.send_dict(pard)
        pard["html"] = tools.dict_to_table(
            pard, pard["result"]["rows"], pard["result"]["c_name"]
        )
        pard["filename"] = "mysqlweb_result.xls"
        pard["header"] = [
            ("Content-type", "application/xls"),
            ("Content-Disposition", 'attachment; filename="%(filename)s"' % pard),
        ]
        return pard

    # --------------------------- dump ---------------------------------
    elif pard["action"] == "dump":
        pard["query"] = pard["query"].lstrip()
        pard["sql_query"] = pard["query"]
        pard["sql_info"] = sql_info.get_sql_info(pard)
        pard["sql"] = clean_query(pard["query"])
        if pard["prev_fetchall"]:
            pard["MAX_ROW"] = "fetchall"
        pard["result"] = db.db_access.send_dict(pard)
        pard["html"] = tools.dict_to_insert(
            pard["result"]["rows"], pard["result"]["c_name"]
        )
        pard["filename"] = "mysqlweb_result.sql"
        pard["header"] = [
            ("Content-type", "application/octet-stream"),
            ("Content-Disposition", 'attachment; filename="%(filename)s"' % pard),
        ]
        return pard

    # --------------------------- run_procedure ----------------------------
    elif pard["action"] == "run_procedure":
        pard["sqls"] = split_query(pard["query_area"])
        pard = run_procedure(pard)

    # --------------------------- row_delete ----------------------------
    elif pard["action"] == "row_delete":
        pard["msg"].append(delete_row(pard))
        pard["query"] = pard["query"].lstrip()
        pard["sql_query"] = pard["query"]
        pard["sql_info"] = sql_info.get_sql_info(pard)
        pard["sql"] = clean_query(pard["query"])
        pard["action"] = "start"
        if pard["prev_fetchall"]:
            pard["fetchall"] = "Y"
        pard = exec_query(pard)

    # --------------------------- allow_edit_field ----------------------------
    elif pard["action"] == "allow_edit_field":
        pard["field_value"] = db_get_field_value(pard)
        pard["html"] = render_edit_field(pard)
        return pard

    # --------------------------- restore_field ----------------------------
    elif pard["action"] == "restore_field":
        pard["html"] = db_get_field_value(pard)
        return pard

    # --------------------------- update_field ----------------------------
    elif pard["action"] == "update_field":
        db_update_field(pard)
        pard["html"] = db_get_field_value(pard)
        return pard

    # --------------------------- query_history ----------------------------
    elif pard["action"] == "query_history":
        pard["search_form"] = render_search_form(pard)
        pard["history"] = get_query_history(pard)
        pard["main_body"] = render_query_history(pard)

    # --------------------------- prepare_insert ----------------------------
    elif pard["action"] == "prepare_insert":
        pard["html"] = prepare_insert(pard)
        return pard

    # --------------------------- save_query ----------------------------
    elif pard["action"] == "save_query":
        pard["filename"] = pard.get("save_as_query_name", "mysqlweb_query.sql")
        pard["header"] = [
            ("Content-type", "text/plain"),
            ("Content-Disposition", 'attachment; filename="%(filename)s"' % pard),
        ]
        # pard['header'] = 'Content-type: text/plain; name="%(filename)s"\nContent-Disposition: attachment; filename="%(filename)s"\n\n' % pard
        pard["html"] = pard["query_area"]
        return pard

    # --------------------------- load_query ----------------------------
    elif pard["action"] == "load_query":
        pard["query"] = pard["query_area"] = ""
        if "file_upload" in pard and pard["file_upload"]:
            content = pard["file_upload"]["content"]
            pard["query_area"] = six.ensure_str(content, encoding="utf8")
        pard["search_form"] = render_search_form(pard)
        pard["main_body"] = """<p>MySQL Query Navigator</p>"""

    # --------------------------- autocomplete_query ----------------------
    elif pard["action"] == "autocomplete_query":
        pard["html"] = set_autocomplete_query(pard)
        return pard

    else:
        pard["result_msg"] = tools.format_messaggio(
            'Function not available. ("%(action)s")' % pard
        )
        pard["main_body"] = (
            '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'
        )

    pard["html"] = render_page(pard)
    return pard


def run_procedure(pard):
    query = []
    for pard["sql_query"] in pard["sqls"]:
        qd = {}
        qd["query"] = pard["sql_query"]
        qd["sql_info"] = sql_info.get_sql_info(pard)
        query.append(qd)

    pard["sqls"] = query
    pard["query"] = query[-1]["query"]
    result = db.db_access.run_procedure(pard)
    html = []
    ind = 1
    pard["result"] = []
    for resd in result:
        resd["num_row"] = len(resd["rows"])
        resd["ind"] = str(ind)
        if resd["error"]:
            html.append('%(ind)s - <span class="err">%(error)s</span>' % resd)
            pard["query"] = resd["query"]
        elif resd["num_row"] > 0:
            if "limit_clause" in pard:
                resd["limit_msg"] = (
                    ' - "limit %(MAX_ROW)s" clause has been added!' % pard
                )
            else:
                resd["limit_msg"] = ""
            html.append(
                "%(ind)s - Result <b>1</b> - <b>%(num_row)s</b> of <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds) %(limit_msg)s"
                % resd
            )
            pard["result"] = resd
        elif resd["rowcount"] > 0:
            html.append(
                "%(ind)s - Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)"
                % resd
            )
        else:
            html.append(
                "%(ind)s - Query OK, <b>Empty set</b> (<b>%(exec_time)s</b> seconds)"
                % resd
            )
        ind += 1

    pard["msg"] = "<br>".join(html)

    pard["result_msg"] = render_result_msg(pard)
    if pard["result"]:
        pard["main_body"] = render_result(pard)
    pard["search_form"] = render_search_form(pard)

    return pard


def exec_query(pard):
    """Exec the query and sets:
    pard['search_form'],
    pard['result_msg'],
    pard['main_body'],
    pard['javascript']
    """
    try:
        if ("fetchall" in pard and pard["fetchall"] == "Y") or pard["sql_info"][
            "query_command"
        ] != "select":
            pard["MAX_ROW"] = "fetchall"
        #       else:
        #           if (pard['sql_info']['query_command'] == 'select'
        #           and 'limit' not in pard['sql_info']['words']):
        #               pard['sql'] = pard['sql'] + '\nlimit %(MAX_ROW)s' % pard
        #               pard['limit_clause'] = True
        pard["result"] = db.db_access.send_dict(pard)
        set_query_history(pard)
        pard["num_row"] = len(pard["result"]["rows"])
        pard["rowcount"] = pard["result"]["rowcount"]
        pard["exec_time"] = pard["result"]["exec_time"]
        pard["sql_info"]["selected_columns"] = pard["result"]["c_name"]

        pard["update_allowed"] = False
        if pard["sql_info"]["query_command"] == "select":
            if "select_tables_list" in pard["sql_info"]:
                if len(pard["sql_info"]["select_tables_list"]) == 1:
                    primary_key = set(pard["sql_info"]["primary_key"])
                    selected_columns = set(pard["result"]["c_name"])
                    if primary_key.issubset(selected_columns):
                        if primary_key:
                            pard["update_allowed"] = True

        if pard["num_row"] > 0:
            pard["query_result"] = (
                "Result <b>1</b> - <b>%(num_row)s</b> of <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)"
                % pard
            )
        elif pard["rowcount"] > 0:
            pard["query_result"] = (
                "Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)"
                % pard
            )
        else:
            pard["query_result"] = (
                "Query OK, <b>Empty set</b> (<b>%(exec_time)s</b> seconds)" % pard
            )

        if "limit_clause" in pard:
            pard["query_result"] = (
                pard["query_result"]
                + ' - "limit %(MAX_ROW)s" clause has been added!' % pard
            )

        pard["edit_data"] = pard.get("edit_data", "")
        if pard["update_allowed"]:
            if pard["edit_data"] == "True":
                pard["update_img"] = (
                    """
                    <img src="/static/img/lock_open.png" border="0"
                       title="Read only"
                       onClick="read_only();">&nbsp;
                       """
                    % pard
                )
            else:
                pard["update_img"] = (
                    """
                    <img src="/static/img/lock.png" border="0"
                       title="Edit Data"
                       onClick="f_edit_data();">&nbsp;
                       """
                    % pard
                )
        else:
            pard["update_img"] = ""

        if pard["sql_info"]["query_command"] == "select":
            pard[
                "download_img"
            ] = """
                    <img src="/static/icons/table_save.png" border="0"
                       title="Download Result"
                       onClick="runquery('download');">&nbsp;&nbsp;
                """
            pard[
                "dump_img"
            ] = """
                    <img src="/static/icons/page_white_put.png" border="0"
                       title="SQL Dump Result"
                       onClick="runquery('dump');">&nbsp;&nbsp;
                """
        else:
            pard["download_img"] = ""
            pard["dump_img"] = ""

        pard["javascript"] = (
            pard["javascript"]
            + """
            <script type="text/javascript" language="javascript" charset="utf-8">

            function read_only()
                {
                if ($('prev_fetchall').value == 'Y')
                    {
                        $('fetchall_checkbox').checked = 'checked';
                    }
                    document.forms['query'].elements['action'].value='run_query';
                    document.getElementById('edit_data').value='';
                    document.forms['query'].submit();
                }

            function f_edit_data()
                {
                if ($('prev_fetchall').value == 'Y')
                    {
                        $('fetchall_checkbox').checked = 'checked';
                    }
                    document.forms['query'].elements['action'].value='run_query';
                    document.getElementById('edit_data').value='True';
                    document.forms['query'].submit();
                }

            </script>
            """
        )

        pard["javascript"] = pard["javascript"] + render_query_info(pard)

        pard["msg"].append(
            """
            <img src="/static/img/view.png" title="Query Information"
             onClick="display_query_info();">
             <div id="query_info" class="html_title" style="width: 400px;"></div>
             &nbsp;&nbsp;%(update_img)s&nbsp;&nbsp;%(download_img)s
             &nbsp;&nbsp;%(dump_img)s
             %(query_result)s
             """
            % pard
        )

        pard["result_msg"] = render_result_msg(pard)
        pard["main_body"] = render_result(pard)
    except dbapi.DatabaseError as e:
        pard["msg"] = '<span class="err">DatabaseError: %s</span>' % (str(e))
        pard["result_msg"] = render_result_msg(pard)
    pard["search_form"] = render_search_form(pard)

    return pard


def split_query(sql):
    ll = sql.split(";\r\n")
    sqls = []
    for x in ll:
        x = x.strip()
        if x:
            sqls.append(clean_query(x))
    return sqls


def clean_query(sql):
    """
    Eseguiamo tutti i controlli sull'sql inserito.
    """
    # Rimozione dell'eventuale ";" inserito a fine riga
    sql = sql.strip()
    if sql[-1:] == ";":
        sql = sql[:-1]

    return sql


def render_search_form(pard):
    pard["query"] = pard.get("query", "")
    pard["query_area"] = pard.get("query_area", "")
    pard["edit_data"] = pard.get("edit_data", "")
    #   pard['browse_table'] = pard.get('browse_table', '')
    #   pard['browse_table_column'] = pard.get('browse_table_column', '')
    #   pard['browse_table_operator'] = pard.get('browse_table_operator', '')
    #   pard['browse_table_value'] = pard.get('browse_table_value', '')
    #
    #   tables_list = db.db_access.get_db_tables_list(pard)
    #   ll = [(i,i) for i in tables_list]
    #   ll.insert(0, ('', '&nbsp;'))
    #   pard['pop_browse_table'] = tools.make_pop(
    #       'browse_table', ll, pard['browse_table'],
    #       jscript='onChange="run_browse_table(this.value);"'
    #       )
    #
    #   pard['toolbar_search'] = ''
    #   if pard['browse_table']:
    #       pard['table_name'] = pard['browse_table']
    #       col_list = db.db_access.show_columns(pard, out='list')
    #       ll = [(i,i) for i in col_list]
    #       pard['pop_browse_table_column'] = tools.make_pop(
    #           'browse_table_column', ll, pard['browse_table_column'])
    #
    #       pard['pop_browse_table_operator'] = tools.make_pop(
    #           'browse_table_operator',
    #           [('=', '='), ('like', 'like'), ('<>', '&lt;&gt;'), ('regexp', 'regexp')],
    #           pard['browse_table_operator']
    #           )
    #
    #       pard['toolbar_search'] = """
    #           <td>Search:&nbsp;
    #               %(pop_browse_table_column)s
    #               %(pop_browse_table_operator)s
    #               <input
    #                   type="text"
    #                   name="browse_table_value"
    #                   id="browse_table_value"
    #                   value="%(browse_table_value)s" size="12"
    #                   onKeyDown="run_table_search(event);" />
    #           </td>
    #           """ % pard
    #

    pard["max_row"] = pard.get("max_row", pard["MAX_ROW"])
    if "fetchall" in pard and pard["fetchall"] == "Y":
        pard["prev_fetchall"] = "Y"
    else:
        pard["prev_fetchall"] = ""
    #   pard['btn_run'] = """
    #       <input type="submit" name="submit_form" value="Run" class="btn"
    #       onclick='action.value="run_query"; form.submit();'
    #       accesskey="r" />
    #       """

    pard["pard_js"] = '{module: "%(module)s",'
    pard["pard_js"] += 'appserver: "%(APPSERVER)sqq/%(ALIAS)s",'
    # pard['pard_js'] += 'sid: "%(sid)s",'
    pard["pard_js"] += 'action: "autocomplete_query",'
    pard["pard_js"] += 'id_textarea: "query_area"}'
    pard["pard_js"] = pard["pard_js"] % pard

    pard["javascript"] = (
        pard["javascript"]
        + """
        <script type="text/javascript" language="javascript" charset="utf-8">

        // Aggiungo il metodo trim alle stringhe
        if(typeof(String.prototype.trim) === "undefined")
        {
            String.prototype.trim = function()
            {
                return String(this).replace(/^\\s+|\\s+$/g, '');
            };
        }

        var isIE = (navigator.appName == "Microsoft Internet Explorer");

        function split_query(qq) {

            var isIE = (navigator.appName == "Microsoft Internet Explorer");
            if (isIE) {
                var sep = ';\\r\\n';
            }
            else {
                var sep = ';\\n';
            }

            var result = [];
            var ll = qq.split(sep);
            var start = 0;

            for (var i = 0; i < ll.length; i++) {
                d = {};
                el = ll[i];
                d['query'] = el.trim();
                d['length'] = el.length;
                d['start'] = start;
                d['end'] = start + el.length + 2;
                start = d['end'];
                result.push(d);
                };

            return result;
            }

        function get_query_by_position(qq, pos) {
            result = split_query(qq);
            for (var i = 0; i < result.length; i++) {
                el = result[i]
                if ((pos >= el['start']) && (pos < el['end']))
                    return el['query'];
                }
            return ''
            }

        function runquery(action)
            {
            document.getElementById('edit_data').value='';
            document.forms['query'].elements['action'].value=action;
            document.forms['query'].submit();
            }

        function run_last_query()
            {
            var query_area = $('query_area');
            var sql = query_area.value
            var result = split_query(sql);
            if (result.length > 0)
                {
                $('query').value = result[result.length - 1]['query'];
                runquery('run_query');
                }
            }

        function run_current() {
            var el = document.getElementById('query_area');
            var result = get_current_query(el);
            if (result) {
                $('query').value = result.trim();
                runquery('run_query');
                }
        }

        function run_selection()
            {
            var selected_query = getSelection($('query_area'));
            $('query').value = selected_query;
            runquery('run_query');
            }

        function save_query_area()
            {
            var tmp = window.prompt("Save As:", "mysqlweb_query.sql");
            if (tmp)
                {
                $('save_as_query_name').value = tmp;
                document.forms['query'].elements['action'].value="save_query";
                document.forms['query'].submit();
                }
            }

        function textarea_add_row(inc, tarea)
            {
            var tag_h = tarea + '_height';
            var tag_td = 'td_' + tarea;
            document.getElementById(tag_h).value = parseInt(document.getElementById(tag_td).style.height) + inc;
            document.getElementById(tag_td).style.height = document.getElementById(tag_h).value + 'px';
            }

        function set_query_area_height(height) {
            height = parseInt(height);
            let tag_h = document.getElementById("query_area_height");
            tag_h.value = height;
            document.getElementById("div_query_area").style.height = tag_h.value + 'px';
            document.getElementById("query_area").style.height = '100%%';

            //console.log('H: ' + height);
            //console.log(document.getElementById("query_area_height").value);

        };

        function minimize_query_area() {
            //document.getElementById("td_query_area").style.height = '25px';
            document.getElementById("query_area_wide_height").value = document.getElementById("query_area_height").value;

            set_query_area_height(20);

            let el = document.getElementById("btn_minimize_query_area");
            el.setAttribute("src", "/static/icons/resultset_previous.png");
            el.setAttribute("title", "Wide Query Area");
            el.setAttribute("onClick", "wide_query_area();");
            };

        function wide_query_area() {
            let hh = document.getElementById("query_area_wide_height").value;

            set_query_area_height(parseInt(hh));

            let el = document.getElementById("btn_minimize_query_area");
            el.setAttribute("src", "/static/icons/resultset_down.png");
            el.setAttribute("title", "Minimize Query Area");
            el.setAttribute("onClick", "minimize_query_area();");
            };

/*
        function db_structure()
            {
                newWindow = window.open("%(APPSERVER)sdb/%(ALIAS)s",
                            "db_structure_%(DB)s",
                            config="height=600, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no"
                            );
                newWindow.focus();
             }
 */

        function new_query_window()
            {
                newWindow = window.open('%(APPSERVER)sqq/%(ALIAS)s');
                return false;
             }


        function toggle_multi_area()
            {
            if ($('query2_display').value == 'N')
                {
                $('query2_display').value = 'S';
                }
            else
                {
                $('query2_display').value = 'N';
                }
            $('multi_area').toggle();
            }

        function run_browse_table(table_name)
            {
            var query_area = $('query_area');
            query_area.value = 'select * from ' + table_name;
            run_last_query();
            }

/*
        function run_table_search(event)
            {
                var test = test_keydown(event);
                if (test == Event.KEY_RETURN) {
                    var query_area = $('query_area');
                    var query_table = $('browse_table').value;
                    var query_column = $('browse_table_column').value;
                    var query_operator = $('browse_table_operator').value;
                    var query_value = $('browse_table_value').value;
                    query_area.value = 'select * from ' + query_table + ' where ' + query_column + ' ' + query_operator + " '" + query_value + "'";
                    run_last_query();
                    }
            }

 */

        function keysPressed(e) {
            var e = e || event;

            const meta = e.ctrlKey || e.metaKey

            // ctrl/cmd + K
            if (meta && e.keyCode === 75) {
                e.preventDefault();
                commenta(this)
            }

            // ctrl/cmd + Enter
            if (meta && e.keyCode === 13) {
                var result = get_current_query(this);
                if (result) {
                    document.getElementById('query').value = result.trim()
                    runquery('run_query');
                }
                e.preventDefault();
            }

            // Esc
            if (e.keyCode === 27) {
                autocomplete_query(%(pard_js)s);
                return false;
            }

            // Tab
            if (e.keyCode === 9) {
                tabba(this);
                e.preventDefault();
            }
        }


        // set textarea value to: text before caret + tab + text after caret
        function tabba(el) {
            var start = el.selectionStart;
            var end = el.selectionEnd;

            var tmp = el.value;
            el.value = tmp.substring(0, start)
                     + "\\t"
                     + tmp.substring(end);

            el.selectionStart = el.selectionEnd = start + 1;
        }


        // Comment (sql) selected text
        function commenta(el) {
            var start = el.selectionStart;
            var end = el.selectionEnd;
            var tmp = el.value;

            el.value = tmp.substring(0, start)
                     + '/* '
                     + tmp.substring(start, end)
                     + ' */'
                     + tmp.substring(end)

            // Set cursor position at the end of commented text
            el.selectionStart = el.selectionEnd = end + 6;

        }


        // Get query by cursor position
        function get_current_query(el) {
            var sql = el.value;
            var start = el.selectionStart;
            var query = get_query_by_position(sql, start);
            return query
        }


        // Select "txt" string between content of "el" textarea
        function select_txt(el, txt) {
            sql = el.value;
            if (txt == '') {
                txt = sql;
            }
            ind = sql.indexOf(txt);
            if (ind != -1) {
                el.selectionStart = ind;
                el.selectionEnd = ind + txt.length + 1;
            }
        }

        // Format content of query area
        function format_sql()
            {
            var query_area = $('query_area');
            var val = query_area.value;
            var val = sqlFormatter.format(val,
                {
                    language: 'mysql',
                    tabWidth: 4,
                    linesBetweenQueries: 1,
                });
            query_area.value = val;
            }


        </script>
        """
        % pard
    )
    pard[
        "btn_next"
    ] = """
        <img src="/static/img/forward.png" border="0"
        title="Next query"
        onClick="navhistory('next')">
        """
    pard[
        "btn_prev"
    ] = """
        <img src="/static/img/back.png" border="0"
        title="Previus query"
        onClick="navhistory('prev')">
        """
    pard["btn_history"] = (
        """
        <a href="%(APPSERVER)sqq/%(ALIAS)s?action=query_history"
        title="History"
        accesskey="h">
        <img src="/static/img/History.png" border="0"></a>
        """
        % pard
    )
    pard["btn_new_query"] = (
        """
        <a href="#"
        title="Open New Query Window"
        onClick="new_query_window();"
        accesskey="n">
        <img src="/static/icons/application_add.png" border="0"></a>
        """
        % pard
    )
    pard["btn_new_conn"] = (
        """
        <a href="%(APPSERVER)sprefs?alias=%(ALIAS)s" title="Connection Manager">
        <img src="/static/icons/text_list_bullets.png" border="0"></a>
        """
        % pard
    )
    pard[
        "btn_textarea_add_row"
    ] = """
        <img src="/static/img/textarea_add_row.png" border="0"
        title="Increase query area"
        onClick="textarea_add_row(20, 'query_area')">
        """
    pard[
        "btn_textarea_remove_row"
    ] = """
        <img src="/static/img/textarea_remove_row.png" border="0"
        title="Decrease query area"
        onClick="textarea_add_row(-20, 'query_area')">
        """
    #   pard['btn_db_structure'] = """
    #       <img src="/static/img/database_gear.png" border="0"
    #       title="DB Structure"
    #       onClick="db_structure();">
    #       """
    pard["btn_content_view"] = (
        """
        <a href="%(APPSERVER)scv/%(ALIAS)s" title="Content View">
        <img src="/static/icons/application_view_columns.png" border="0"></a>
        """
        % pard
    )
    pard["btn_run"] = (
        """
        <a href="javascript:run_current()"
        title="Run current query (cmd+Enter or ctrl+Enter)"
        accesskey="r">
        <img src="/static/icons/lightning.png" border="0"></a>
        """
        % pard
    )
    pard[
        "btn_run_selection"
    ] = """
        <img src="/static/icons/lightning_go.png" border="0"
        title="Run Selection"
        onClick="run_selection();">
        """
    pard[
        "btn_exec_all"
    ] = """
        <img src="/static/icons/script_lightning.png" border="0"
        title="Exec Procedure (Exec all queries in transaction)"
        onClick="runquery('run_procedure');">
        """
    pard[
        "btn_multi_area"
    ] = """
        <img src="/static/icons/page_white_lightning.png" border="0"
        title="Show Last Executed Query"
        onClick="toggle_multi_area();">
        """
    pard[
        "btn_comment_selection"
    ] = """
        <img src="/static/icons/page_white_code.png" border="0"
        title="Comment selection (cmd+k or ctrl+k)"
        onClick="commenta(document.getElementById('query_area'));">
        """
    pard[
        "btn_format_sql"
    ] = """
        <img src="/static/icons/text_indent.png" border="0"
        title="SQL Formatter"
        onClick="format_sql();">
        """
    pard[
        "btn_save_query"
    ] = """
        <input type="hidden" name="save_as_query_name" id="save_as_query_name" value="mysqlweb_query">
        <img src="/static/img/save.png" border="0"
        title="Save Query Area"
        onClick="save_query_area('save_query');">
        """
    pard[
        "btn_load_query"
    ] = """
        <img src="/static/icons/folder_page.png" border="0"
        title="Load Query"
        onClick="$('load_query_form').show();">
        <div id="load_query_form" class="html_title" style="display: none; padding: 6px; border: 2px solid #F0F0F0;">
        <table style="width: 100%;">
        <tr>
            <th>
                <img src="/static/img/uncheck.png" border="0" onClick="$('load_query_form').hide();">
                &nbsp;&nbsp;&nbsp;Load query from local file
            </th>
        </tr>
        <tr>
            <td>
                <input type="file" name="file_upload" size="30" />
                <input type="button" name="submit_form" value="Load" onclick="runquery('load_query');" />
            </td>
        </tr>
        </table>
        </div>
        """

    pard["javascript"] = pard["javascript"] + js_navhistory(pard)
    pard["query_history_len"] = str(len(pard["query_history"]) - 1)
    pard["query_area_height"] = pard.get("query_area_height", "135")
    pard["query_area_wide_height"] = pard.get("query_area_wide_height", "135")
    if int(pard["query_area_wide_height"]) < 135:
        pard["query_area_wide_height"] = "135"
    pard["query2_display"] = pard.get("query2_display", "N")
    try:
        command = pard["sql_info"]["query_command"]
    except KeyError:
        command = ""
    if command in ("wsel", "wins"):
        pard["m_style"] = ""
        pard["query2_display"] = "S"
    elif pard["query2_display"] == "N":
        pard["m_style"] = 'style="display: none;"'
    else:
        pard["m_style"] = ""

    if int(pard["query_area_height"]) < 40:
        pard[
            "btn_minimize_query_area"
        ] = """
            <img src="/static/icons/resultset_previous.png" border="0"
                 title="Wide Query Area"
                 onClick="wide_query_area()"
                 id="btn_minimize_query_area"
                 height="20"
            >
            """
        pard["query_area_height"] = "36"
    else:
        pard[
            "btn_minimize_query_area"
        ] = """
            <img src="/static/icons/resultset_down.png" border="0"
                 title="Minimize Query Area"
                 onClick="minimize_query_area()"
                 id="btn_minimize_query_area"
                 height="20"
            >
            """

    h = (
        """
    <input type="hidden" name="edit_data" id="edit_data" value="%(edit_data)s" />
    <input type="hidden" name="query_area_height" id="query_area_height" value="%(query_area_height)s" />
    <input type="hidden" name="query_area_wide_height" id="query_area_wide_height" value="%(query_area_wide_height)s" />
    <input type="hidden" name="pk_del" id="pk_del" value="" />
    <input type="hidden" name="tbl_del" id="tbl_del" value="" />
    <input type="hidden" name="prev_fetchall" id="prev_fetchall" value="%(prev_fetchall)s" />
    <table width="100%%">
    <tr>

        <td style="margin: 0; padding: 0;" id="td_query_area">
            <div style="height: %(query_area_height)spx;" id="div_query_area">
            <textarea style="border: none; width: 100%%; height: 100%%"
                      id="query_area"
                      name="query_area"
                      spellcheck="false"
                >%(query_area)s</textarea>
            </div>
        </td>

        <td width="50px" style="margin: 0; padding: 0; vertical-align: top; background-color: #DDDDDD;">

            <table class="action" width="100%%">
<!--
            <tr>
                <td style="vertical-align: bottom;">
                    %(btn_textarea_remove_row)s
                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom;">
                    %(btn_textarea_add_row)s
                </td>
            </tr>
 -->
            <tr>
                <td style="height: 34px;">%(btn_minimize_query_area)s</td>
            </tr>
            </table>

        </td>
    </tr>
    </table>

    <div id="multi_area" %(m_style)s>

    <input type="hidden" name="query2_display" id="query2_display" value="%(query2_display)s" />

    <table width="100%%">
    <tr>

        <td style="margin: 0; padding: 0; height: 70">
            <textarea
                style="border: none; width: 100%%; height: 100%%"
                id="query"
                name="query"
                readonly="readonly"
            >%(query)s</textarea>
        </td>

    </tr>
    </table>

    <script type="text/javascript" language="javascript" charset="utf-8">

        document.getElementById('query_area').focus();
        var el_query_area = document.getElementById('query_area');
        var el_query = document.getElementById('query');
        select_txt(el_query_area, el_query.value);

        el_query_area.addEventListener("keydown", keysPressed, false);

        // ------------------------------------------------------------------
        // Intercetto i cambiamenti di dimensione della textarea che contiene
        // la query
        const resize_observer = new ResizeObserver(function(entries) {
            // since we are observing only a single element,
            // so we access the first element in entries array
            let rect = entries[0].contentRect;

            // current width & height
            //let width = rect.width;
            let height = rect.height;

            //document.getElementById("query_area_height").value = height + 5;
            //document.getElementById("td_query_area").style.height = height + 5;
            set_query_area_height(height);

        });

        // start observing for resize
        resize_observer.observe(document.querySelector("#div_query_area"));
        // -----------------------------------------------------------------

        // Focus
        el_query_area.focus();

    </script>

    </div><!-- id="multi_area" -->

    <div id="toolbar">

    <table class="action" width="100%%">
    <tr>
        <td>%(btn_run)s</td>
        <td>%(btn_run_selection)s</td>
        <td>%(btn_exec_all)s</td>
        <td>%(btn_comment_selection)s</td>
        <td>%(btn_multi_area)s</td>
        <td>%(btn_format_sql)s</td>
        <td>
            <b>Limit:&nbsp;</b><input type="text" name="max_row" value="%(max_row)s" size="3">
        </td>
        <td>
            <b>Fetch All&nbsp;</b><input type="checkbox" id="fetchall_checkbox" name="fetchall" value="Y">
        </td>
        <td>
            %(btn_load_query)s&nbsp;%(btn_save_query)s
        </td>
        <td>
            <input type="hidden" name="query_index" id="query_index" value="%(query_history_len)s" />
            %(btn_prev)s%(btn_next)s%(btn_history)s
        </td>
        <td>%(btn_new_query)s%(btn_new_conn)s</td>
        <td>%(btn_content_view)s</td>
        <td>
            <button
                type="button"
                title="Copy to clipboard"
                onclick="copy_to_clipboard('query_area', 'clipboard_msg');"
            >
                <img src="/static/img/clippy.svg" width="13" alt="Copy to clipboard" style="vertical-align: text-bottom;">
            </button>
            <span id="clipboard_msg"></span>
        </td>
        <td width="100%%" style="text-align: left;"><span id="progress_span">&nbsp;</span></td>
    </tr>
    </table>

    </div><!-- id="toolbar" -->

    """
        % pard
    )
    return h


def render_result_msg(pard):
    pard["msg"] = pard.get("msg", "")
    if isinstance(pard["msg"], list):
        pard["msg"] = "<br>".join(pard["msg"])
    h = (
        """
        <table width="100%%">
            <tr>
            <td bgcolor="#e5ecf9" align="left" nowrap style="border-top: 1px solid #3366CC;">
            %(msg)s
            </td>
            </tr>
        </table>
        """
        % pard
    )
    return h


def render_result(pard):
    pard["update_allowed"] = pard.get("update_allowed", False)
    h = []
    if pard["result"]["rows"]:
        h.append("<table width=100%%>")
        h.append("<tr>")
        h.append('<th scope="col">&nbsp;</th>')
        for col in pard["result"]["c_name"]:
            h.append('<th scope="col">%s</th>' % col)
        h.append("</tr>")

        if pard["update_allowed"] and pard["edit_data"] == "True":
            h.append(render_result_for_update(pard))
        else:
            row_num = 1
            for rec in pard["result"]["rows"]:
                #               rec = tools.sbianca(rec)
                rec = utils.cgi_escape(rec)
                h.append('<tr valign="top">')
                h.append(
                    '<th style="text-align: right; width: 20px;">%s</th>' % str(row_num)
                )
                for key in pard["result"]["c_name"]:
                    if str(rec[key]).find("\n") > -1:
                        rec[key] = "<pre>%s</pre>" % str(rec[key])
                    h.append("<td>%s</td>" % str(rec[key]))
                h.append("</tr>")
                row_num += 1
        h.append("</table>")
    h = "\n".join(h)
    return h


def render_result_for_update(pard):
    pard["table_name"] = pard["sql_info"]["select_tables_list"][0]
    pard["javascript"] += ajax_allow_edit_field(pard)
    h = []
    row_num = 1
    for rec in pard["result"]["rows"]:
        pk_dict = {}
        # encode primary key in a json object
        for key in pard["sql_info"]["primary_key"]:
            if isinstance(rec[key], datetime.datetime) or isinstance(
                rec[key], decimal.Decimal
            ):
                rec[key] = str(rec[key])
            pk_dict[key] = rec[key]
        pk_json = json.dumps(pk_dict)

        #       rec = tools.sbianca(rec)
        rec = utils.cgi_escape(rec)
        h.append('<tr valign="top">')
        d = {
            "pk": pk_json,
            "row_num": str(row_num),
            "table_name": json.dumps(pard["table_name"]),
        }
        h.append(
            """
            <th style="text-align: right; white-space: nowrap; width: 35px;">
                %(row_num)s&nbsp;
                <img src="/static/icons/delete.png"
                    title="Delete"
                    onClick='row_delete({pk: %(pk)s, table_name: %(table_name)s})'
                    style="border: solid 0px;">
                &nbsp;
                <img src="/static/icons/add.png"
                    title="Duplicate this record"
                    onClick='prepare_insert({pk: %(pk)s, table_name: %(table_name)s})'
                    style="border: solid 0px;">
            </th>
            """
            % d
        )
        col_num = 1
        for key in pard["result"]["c_name"]:
            if str(rec[key]).find("\n") > -1:
                rec[key] = "<pre>%s</pre>" % str(rec[key])
            if (
                key in pard["sql_info"]["primary_key"]
                or key not in pard["sql_info"]["table_columns"]
            ):
                h.append("<td>%s</td>" % str(rec[key]))
            else:
                # pard_js = pard_js_action
                pard_js = "{pk: %s," % pk_json
                pard_js += 'area: "r{}c{}",'.format(str(row_num), str(col_num))
                pard_js += 'field_name: "%s"}' % key
                h.append(
                    """<td onDblClick='allow_edit_field({})'><span id="r{}c{}">{}</span></td>""".format(
                        pard_js, str(row_num), str(col_num), str(rec[key])
                    )
                )
            col_num += 1
        h.append("</tr>")
        row_num += 1
    h = "\n".join(h)
    return h


def render_query_info(pard):
    html = [
        """
        <table style="width: 100%;">
        <tr>
        <th colspan="2">
            <img src="/static/img/uncheck.png" border="0" onClick="document.getElementById('query_info').innerHTML = '';">
            &nbsp;&nbsp;&nbsp;Query Information
        </th>
        </tr>
        """
    ]
    for key in list(pard["sql_info"].keys()):
        if key == "words":
            continue
        elif isinstance(pard["sql_info"][key], list):
            html.append(
                "<tr><td><b>{}:&nbsp;</b></td><td>{}</td></tr>".format(
                    key, ", ".join(pard["sql_info"][key])
                )
            )
        else:
            html.append(
                "<tr><td><b>{}:&nbsp;</b></td><td>{}</td></tr>".format(
                    key, str(pard["sql_info"][key])
                )
            )
    html.append("</table>")
    html = "\n".join(html)
    # html = tools.escape_javascript(html)
    hj = {"content": html}
    js = """
        <script type="text/javascript" language="javascript" charset="utf-8">

        function display_query_info()
        {
            content = %s;
            document.getElementById('query_info').innerHTML = content.content;
        }

        </script>
        """ % json.dumps(
        hj
    )
    return js


def opensearch(pard):
    html = (
        """
        <link title="MySQL Query Navigator: %(ALIAS)s" rel="search" type="application/opensearchdescription+xml"
        href="%(APPSERVER)sOpenSearch/%(ALIAS)s.xml">
        """
        % pard
    )
    return html


def render_page(pard):
    html = """
        <html>
            <head>
                <meta charset="utf-8"></meta>
                %(CSS)s
                <title>%(TITLE)s</title>
                %(javascript)s
                %(opensearch)s
                <link rel="icon" href="/static/favicon.ico">
            </head>
            <body>
                <div id="userbar">%(userbar)s</div>
                %(menu)s
                <form action="%(APPSERVER)sqq/%(ALIAS)s" method="post" name="query" enctype="multipart/form-data">
                <input type="hidden" name="module" value="%(module)s">
                <input type="hidden" name="action" value="%(action)s">
                %(search_form)s
                %(result_msg)s
                <div id="main_body">
                    %(main_body)s
                </div>
                </form>
            </body>
        </html>
        """
    html = html % pard
    return html


def render_menu(pard):
    pard["current_connection"] = "%(USER)s@%(HOST)s/%(DB)s" % pard
    if "query" in pard and pard["query"]:
        pard["TITLE"] = (
            pard.get("ALIAS", pard["DB"])
            + " - "
            + pard["query"][0:200].replace("\n", " ")
        )
    else:
        # pard['TITLE'] = '%(TITLE)s - %(current_connection)s' % pard
        pard["TITLE"] = pard.get("ALIAS", pard["DB"])

    light = tools.lightness(pard["COLOR"])
    pard["text_color"] = "#ffffff" if light < 60 else "#666666"
    h = (
        """
    <table width="100%%" class="header" style="background-color: %(COLOR)s;">
        <tr>
            <td class="left" style="color: %(text_color)s;">MySQL Web</td>
            <td class="right" style="color: %(text_color)s;" title="%(current_connection)s">
                <span style="font-size: 2em;">%(ALIAS)s</span>
            </td>
        </tr>
    </table>
    """
        % pard
    )
    return h


def render_edit_field(pard):
    pard["field_value"] = str(pard["field_value"])
    pard["textarea_rows"] = pard["field_value"].split("\n")
    pard["t_rows"] = len(pard["textarea_rows"])
    pard["text_size"] = 3
    for item in pard["textarea_rows"]:
        if len(item) > pard["text_size"]:
            pard["text_size"] = len(item)
    if pard["text_size"] > 80:
        rows = pard["text_size"] / 80 + 1
        if rows > pard["t_rows"]:
            pard["t_rows"] = rows
        pard["text_size"] = "80"
    else:
        pard["text_size"] = str(pard["text_size"])

    #   pk_dict = json.loads(pard['pk'])
    #   pard['pk_json'] = json.dumps(pk_dict)

    pard_js = '{module: "qq",'
    pard_js += 'appserver: "%(APPSERVER)sqq/%(ALIAS)s",'
    pard_js += 'table_name: "%(table_name)s",'
    pard_js += 'field_name: "%(field_name)s",'
    pard_js += "pk: %(pk)s,"
    pard_js += 'area: "%(area)s",'
    pard_js = pard_js % pard

    pard["pard_js_update"] = pard_js + 'action: "update_field"}'
    pard["pard_js_restore"] = pard_js + 'action: "restore_field"}'

    if pard["t_rows"] > 1 or pard["field_value"].find('"') > -1:
        h = (
            """
            <textarea
                id="field_%(area)s"
                name="%(field_name)s"
                cols="%(text_size)s"
                rows="%(t_rows)s"
            >%(field_value)s</textarea>
            <img src="/static/img/save.png" title="Save" onclick='update_field(%(pard_js_update)s);'>&nbsp;
            <img src="/static/img/uncheck.png" title="Cancel" onclick='restore_field(%(pard_js_restore)s);'>
            """
            % pard
        )
    else:
        h = (
            """
            <input id="field_%(area)s" type="text" name="%(field_name)s" size="%(text_size)s" value="%(field_value)s"
                   onKeyDown='var test = test_keydown(event);
                              if (test == Event.KEY_RETURN) {update_field(%(pard_js_update)s);   return false;};
                              if (test == Event.KEY_ESC)    {restore_field(%(pard_js_restore)s); return false;};'>
            <img src="/static/img/save.png" title="Save" onclick='update_field(%(pard_js_update)s);'>&nbsp;
            <img src="/static/img/uncheck.png" title="Cancel" onclick='restore_field(%(pard_js_restore)s);'>
            """
            % pard
        )
    return h


def db_get_field_value(pard):
    pk_dict = json.loads(pard["pk"])
    pard["sql"] = "select `%(field_name)s` from `%(table_name)s` where " % pard
    ll = []
    for key in list(pk_dict.keys()):
        if isinstance(pk_dict[key], str):
            ll.append("`{}` = '{}'".format(key, pk_dict[key].replace("'", "''")))
        else:
            ll.append("`{}` = {}".format(key, str(pk_dict[key])))
    pard["sql"] += " and ".join(ll)
    result = db.db_access.send(pard)
    result = str(result[0][0])
    result = utils.cgi_escape(result)
    return result


def db_update_field(pard):
    pk_dict = UTF8(json.loads(pard["pk"]))
    pard["field_value"] = pard.get("field_value", "")
    pard["field_value"] = pard["field_value"].replace("'", "''")
    print("{!r}: {!r}".format(pk_dict, pard["field_value"]))
    pard["sql"] = (
        "update `%(table_name)s` set `%(field_name)s` = '%(field_value)s' where " % pard
    )

    ll = []
    for k in list(pk_dict.keys()):
        key = six.ensure_str(k, encoding="utf8")
        if isinstance(pk_dict[key], str):
            ll.append("`{}` = '{}'".format(key, pk_dict[key].replace("'", "''")))
        else:
            ll.append("`{}` = '{}'".format(key, pk_dict[key]))

    pard["sql"] += " and ".join(ll)
    pard["sql_info"] = {"query_command": "update"}
    db.db_access.send_dict(pard)
    return pard["sql"]


def delete_row(pard):
    pk_dict = json.loads(pard["pk_del"])
    pard["sql_info"] = {"query_command": "delete"}
    pard["sql"] = "delete from `%(tbl_del)s` where " % pard
    ll = []
    for key in list(pk_dict.keys()):
        if isinstance(pk_dict[key], str):
            ll.append("`{}` = '{}'".format(key, pk_dict[key].replace("'", "''")))
        else:
            ll.append("`{}` = {}".format(key, str(pk_dict[key])))
    pard["sql"] += " and ".join(ll)
    result = db.db_access.send_dict(pard)
    result["sql"] = pard["sql"]
    query_result = (
        '"%(sql)s" Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)'
        % result
    )
    return query_result


def prepare_insert_old(pard):
    pk_dict = json.loads(pard["pk"])
    pard["sql"] = "select * from %(table_name)s where " % pard
    ll = []
    for key in list(pk_dict.keys()):
        if isinstance(pk_dict[key], str):
            ll.append("{} = '{}'".format(key, pk_dict[key].replace("'", "''")))
        else:
            ll.append("{} = {}".format(key, str(pk_dict[key])))
    pard["sql"] += " and ".join(ll)
    result = db.db_access.send_dict(pard)
    if result["rowcount"] == 0:
        return "Not Found"
    rec = result["rows"][0]
    columns_list = []
    value_list = []
    for c_name in result["c_name"]:
        columns_list.append(c_name)
        if isinstance(rec[c_name], str):
            value_list.append("'%s'" % rec[c_name].replace("''", "'"))
        elif isinstance(rec[c_name], datetime.datetime):
            value_list.append("'%s'" % rec[c_name])
        else:
            value_list.append("%s" % rec[c_name])
    pard["sql"] = "INSERT INTO {}\n({})\nVALUES\n({});".format(
        pard["table_name"],
        ", ".join(columns_list),
        ", ".join(value_list),
    )
    return pard["sql"]


def prepare_insert(pard):
    pk_dict = json.loads(pard["pk"])
    pard["sql"] = "select * from `%(table_name)s` where " % pard
    ll = []
    for key in list(pk_dict.keys()):
        if isinstance(pk_dict[key], str):
            ll.append("`{}` = '{}'".format(key, pk_dict[key].replace("'", "''")))
        else:
            ll.append("`{}` = {}".format(key, str(pk_dict[key])))
    pard["sql"] += " and ".join(ll)
    result = db.db_access.send_dict(pard)
    if result["rowcount"] == 0:
        return "Not Found"
    rec = result["rows"][0]

    if pard.get("ENGINE") == "mysql":
        ll = []
        for c_name in result["c_name"]:
            value = rec[c_name]
            if isinstance(rec[c_name], str):
                value = value.replace("''", "'")
                ll.append(f"\t{c_name} = '{value}'")
            elif isinstance(rec[c_name], datetime.datetime):
                ll.append(f"\t{c_name} = '{value}'")
            else:
                ll.append(f"\t{c_name} = {value}")
        pard["sql"] = "\n".join(
            ["INSERT INTO", "   %s" % pard["table_name"], "SET", ""]
        ) + ",\n".join(ll)
    else:
        columns_list = []
        value_list = []
        for c_name in result["c_name"]:
            columns_list.append(c_name)
            if isinstance(rec[c_name], str):
                value_list.append("'%s'" % rec[c_name].replace("''", "'"))
            elif isinstance(rec[c_name], datetime.datetime):
                value_list.append("'%s'" % rec[c_name])
            else:
                value_list.append("%s" % rec[c_name])
        pard["sql"] = "INSERT INTO {}\n({})\nVALUES\n({});".format(
            pard["table_name"],
            ", ".join(columns_list),
            ", ".join(value_list),
        )
    return pard["sql"]


def set_autocomplete_query(pard):
    flag_autocompletion_found = False
    # Decomposing the query using the current position of cursor
    pard["field_pos"] = int(pard["field_pos"])
    first_part = pard["field_value"][: pard["field_pos"]]
    last_part = pard["field_value"][pard["field_pos"] :]
    # We must not take care of the trailing whitespace or punctuation char in completion
    # but we have to preserve that once the word is completed
    whitespace_after_completion = ""
    if first_part:
        trailing_char_pat = re.compile(r".+?([^\w]+)\Z", re.DOTALL | re.IGNORECASE)
        trailing_char_list = trailing_char_pat.findall(first_part)
        if trailing_char_list:
            whitespace_after_completion = trailing_char_list[-1]
    # Decomposing the first part of the query using module sql_info: the last word of this block is
    # the word I have to (auto)complete
    pard["sql_query"] = first_part
    list_first_part = sql_info.get_sql_info(pard, flag_column_info=False)["words"]
    i = -1
    try:
        word_to_complete = list_first_part[i]
    except IndexError:
        # the cursor is in position 0 (zero): we have nothing to complete
        word_to_complete = ""
    # Detach of the word_to_complete from the first part of the query; we re-attach the word_completed
    # at the end of the procedure
    first_part = first_part[: first_part.rfind(word_to_complete)]

    # Searching the part of the word_to_complete right to the cursor (we could have digited something
    # or tryed to autocomplete before now): these characters are useful in the next step for deciding
    # what word is the right one to use in completion.
    word_completed_yet = ""
    if last_part:
        word_completed_yet_pat = re.compile(
            r"\A(\w+)(?=\s|\=|\,|\;|\>|\<|\(|\'|\"|\Z)", re.DOTALL | re.IGNORECASE
        )
        word_completed_yet_list = word_completed_yet_pat.findall(last_part)
        if word_completed_yet_list:
            word_completed_yet = word_completed_yet_list[0]
            last_part = last_part[len(word_completed_yet) :]

    if word_to_complete:
        while flag_autocompletion_found is False:
            i -= 1
            try:
                word_before = list_first_part[i]
                # Autocomplete of table-columns
                if word_before in ("select", "where", "by", "on", "set"):
                    pard["sql_query"] = pard["field_value"]
                    sql_info_dict = sql_info.get_sql_info(pard, flag_column_info=False)
                    db_column_list = []
                    if sql_info_dict["select_tables_list"]:
                        db_column_list = db.db_access.show_all_columns(
                            pard, tables_list=sql_info_dict["select_tables_list"]
                        )
                        db_column_list.sort()
                    # Completion
                    (word_to_complete, word_completed_yet) = get_completion(
                        pard, word_to_complete, word_completed_yet, db_column_list
                    )
                    flag_autocompletion_found = True
                # Autocomplete of table
                elif word_before in (
                    "from",
                    "into",
                    "update",
                    "wsel",
                    "wins",
                    "describe",
                    "join",
                ):
                    db_tables_list = db.db_access.get_db_tables_list(pard)
                    db_tables_list.sort()
                    # Completion
                    (word_to_complete, word_completed_yet) = get_completion(
                        pard, word_to_complete, word_completed_yet, db_tables_list
                    )
                    flag_autocompletion_found = True
            except IndexError:
                flag_autocompletion_found = True

    # Final substitution
    diz_out = {}
    diz_out["query"] = (
        first_part
        + word_to_complete
        + whitespace_after_completion
        + word_completed_yet
        + last_part
    )
    diz_out["pos"] = pard["field_pos"]

    diz_out["original_query"] = pard["field_value"]
    diz_out["first_part"] = first_part
    diz_out["word_to_complete"] = word_to_complete
    diz_out["word_completed_yet"] = word_completed_yet
    diz_out["last_part"] = last_part
    diz_out["whitespace_after_completion"] = whitespace_after_completion

    return json.dumps(diz_out)


def get_completion(pard, word_to_complete, word_completed_yet, db_field_list):
    flag_repeat_iteration = True
    if word_completed_yet:
        # If the word was previously completed (word_completed_yet != ''), we have
        # to admit only the first column-name after the old completion: so we search
        # for the old_word and we choose the next column_name
        old_word = word_to_complete + word_completed_yet
        flag_old_word_found = False
        for field in db_field_list:
            if field == old_word:
                flag_old_word_found = True
            if (
                flag_old_word_found
                and field.startswith(word_to_complete)
                and field != old_word
            ):
                word_to_complete = field
                word_completed_yet = ""
                flag_repeat_iteration = False
                break
    # If we haven't found any completion, we could be in one of the next 3 scenarios:
    # 1) word_completed_yet was empty
    #    --> so we use the first completion
    # 2) old_word (word_to_complete + word_completed_yet) was not a field-name
    #    --> so we use the first completion (overwriting the whole word)
    # 3) old_word was a field-name but was the last usable in db_field_list
    #    --> so we choose again the first usable field-name
    # In all possibilities the first usable field-name (that means "startswith(word_to_complete)")
    # is the right decision
    if flag_repeat_iteration:
        for field in db_field_list:
            if field.startswith(word_to_complete) and field != word_to_complete:
                word_to_complete = field
                word_completed_yet = ""
                break

    return (word_to_complete, word_completed_yet)


def get_query_history(pard):
    history_file = get_history_name(pard)
    try:
        fp = open(history_file)
        buf = fp.read()
        fp.close()
    except OSError:
        buf = ""
    query_list = buf.split("<end>")
    hist = []
    for q in query_list:
        if not q:
            continue
        rec = {}
        try:
            rec["datetime"], rec["query"] = q.split(":::")
            hist.append(rec)
        except Exception:
            pass
    return hist


def set_query_history(pard):
    history_file = get_history_name(pard)
    pard["now"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    fp = open(history_file, "a")
    fp.write("%(now)s:::%(query)s<end>" % pard)
    fp.close()


def get_history_name(pard):
    db = pard["DB"]
    pos = db.find(":")
    fp = db[pos + 1 :]
    pard["_DB_NAME"] = os.path.basename(fp)
    pard["_DB_NAME"] = "%(USER)s_%(HOST)s_%(_DB_NAME)s" % pard

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])
    history_file = "%(DATA_DIR)s/%(mw_user)s/%(_DB_NAME)s.txt" % pard
    tools.check_dir
    return history_file


def js_navhistory(pard):
    pard["query_history"] = get_query_history(pard)[-50:]
    qh = []
    for rec in pard["query_history"]:
        qh.append(
            "'"
            + rec["query"]
            .replace("\\", "\\\\")
            .replace("'", "\\'")
            .replace("\n", "\\n")
            .replace("\r", "\\r")
            .replace('"', '"')
            + "'"
        )
    qh.append("''")
    pard["qh"] = ", ".join(qh)
    js = (
        """
        <script type="text/javascript" language="javascript" charset="utf-8">
        function navhistory(direction) {
            qh = [%(qh)s];
            i = document.getElementById('query_index').value

            if (direction == 'next') {
                i++
                };
            if (direction == 'prev') {
                i--
                };
            if (i < 0) {
                i = 0
                };
            if (i > qh.length - 1) {
                i = qh.length - 1
                };

            document.getElementById('query_area').value = qh[i];
            document.getElementById('query_index').value = i;
            }
        </script>
        """
        % pard
    )
    return js


def render_query_history(pard):
    pard["history"].reverse()
    h = [
        """
        <script type="text/javascript" language="javascript" charset="utf-8">
        function set_query(el) {
            var query = el.innerHTML;
            document.getElementById('query_area').value = query;
            document.getElementById('query_area').focus();
        }
        </script>
        <table width=100%>
        <tr>
            <th scope="col">Query</th>
            <th scope="col">Date and Time</th>
        </tr>
        """
    ]
    for rec in pard["history"]:
        # rec['query_esc'] = tools.escape_javascript(rec['query'])
        h.append(
            """
            <tr>
                <td title="Set query">
                    <pre onClick="set_query(this);">%(query)s</pre>
                </td>
                <td>%(datetime)s</td>
            </tr>
            """
            % rec
        )
    h.append("</table>")
    return "\n".join(h)


def wsel_query_wizard(pard):
    args = pard["sql_query"][4:].replace(",", " ").split()
    tables_list = db.db_access.get_db_tables_list(pard)
    error = ""
    if len(args) == 0:
        error = "Syntax: wsel table_name [, table_name]"
    elif len(args) == 1:
        join = False
    else:
        join = True
        alias = "abcdefghijklmn"
    i = 0
    columns = []
    tables = []
    for pard["table_name"] in args:
        if pard["table_name"] not in tables_list:
            error = "Table %(table_name)s doesn't exist" % pard
            break
        columns_list = db.db_access.show_columns(pard, out="list")
        if join:
            columns_list = ["{}.{}".format(alias[i], x) for x in columns_list]
            pard["table_name"] = "{} {}".format(pard["table_name"], alias[i])
        columns.append(", ".join(columns_list))
        tables.append(pard["table_name"])
        i += 1
    if error:
        pard["msg"] = '<span class="err">%s</span>' % error
        pard["result_msg"] = render_result_msg(pard)
    else:
        pard["query"] = "SELECT " + ",\n".join(columns) + "\nFROM " + ", ".join(tables)
    pard["search_form"] = render_search_form(pard)
    return pard


def wins_query_wizard(pard):
    args = pard["sql_query"][4:].replace(",", " ").split()
    tables_list = db.db_access.get_db_tables_list(pard)
    error = ""
    if len(args) != 1:
        error = "Syntax: wins table_name"
    else:
        pard["table_name"] = args[0]
        if pard["table_name"] not in tables_list:
            error = "Table %(table_name)s doesn't exist" % pard
    if error:
        pard["msg"] = '<span class="err">%s</span>' % error
        pard["result_msg"] = render_result_msg(pard)
    else:
        columns_list = db.db_access.show_columns(pard, out="list")
        value_list = ["'%%(%s)s'" % x for x in columns_list]
        pard["query"] = "INSERT INTO {}\n({})\nVALUES\n({})".format(
            pard["table_name"],
            ", ".join(columns_list),
            ", ".join(value_list),
        )
    pard["search_form"] = render_search_form(pard)

    return pard


def silk_icons(pard):
    args = pard["sql_query"][4:].replace(",", " ").split()
    if args:
        pard["icon_name"] = args[0]
    else:
        pard["icon_name"] = ""
    pard["icon_name"] = pard["icon_name"].lower()

    lf = os.listdir("%(HTML_DIR)s/icons/" % pard)
    i = 0
    pard["cols"] = 10
    h = ['<table width="100%%">']
    for ic in lf:
        if ic[-4:] != ".png":
            continue
        if pard["icon_name"]:
            if ic.lower().find(pard["icon_name"]) == -1:
                continue
        if i % pard["cols"] == 0:
            h.append("<tr>")
        pard["icon_file_name"] = ic
        h.append(
            """
            <td title="%(icon_file_name)s">
                <img src="/static/icons/%(icon_file_name)s" alt="%(icon_file_name)s" border="0" onClick="$('%(icon_file_name)s').toggle();">
                <span class="small" style="display: none;" id="%(icon_file_name)s">%(icon_file_name)s</span>
            </td>
            """
            % pard
        )
        i += 1
        if i % pard["cols"] == 0:
            h.append("</tr>")

    if i % pard["cols"] != 0:
        h.append("</tr>")

    h.append("</table>")
    pard["main_body"] = "\n".join(h)
    pard["result_msg"] = render_result_msg(pard)
    pard["search_form"] = render_search_form(pard)

    return pard


def eval_query(pard):
    """Really dangerous use only if you completely trust your users

    http://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html
    """
    code = pard["query"][5:]  # .replace('\r\n', '\n')
    builtins = {"__builtins__": None, "True": True, "False": False}
    import time, math, tools, re, decimal, itertools  # noqa: E401

    dd = {
        "int": int,
        "enumerate": enumerate,
        "abs": abs,
        "time": time,
        "math": math,
        "dict": dict,
        "tools": tools,
        "divmod": divmod,
        "float": float,
        "isinstance": isinstance,
        "len": len,
        "range": range,
        "re": re,
        "decimal": decimal,
        "itertools": itertools,
    }
    try:
        result = eval(code, builtins, dd)
    except Exception:
        result = tools.build_exception()
        result += "\n\n%s" % repr(code)
    pard["main_body"] = "<pre>%s<pre>" % result
    pard["result_msg"] = render_result_msg(pard)
    pard["search_form"] = render_search_form(pard)

    return pard


def ajax_allow_edit_field(pard):
    html = (
        """
        <script type="text/javascript" language="javascript" charset="utf-8">

        function allow_edit_field(pard_js)
        {
            //area: name of the target box
            //field_name
            //pk: json object of row primary key
            params = "module=" + escape("%(module)s");
            params += "&table_name=" + escape("%(table_name)s");
            params += "&action=allow_edit_field";
            params += "&area=" + escape(pard_js.area);
            params += "&field_name=" + escape(pard_js.field_name);
            params += "&pk=" + escape(Object.toJSON(pard_js.pk));

            // To disable DblClick in field edit mode.
            var edit_mode = $('field_' + pard_js.area);
            if (edit_mode != null) {
                return true
                }

            _complete = 0;

            req = new Ajax.Request
            (
                "%(APPSERVER)sqq/%(ALIAS)s",
                {
                    method: 'post',
                    parameters: params,
                    onLoading: function()
                    {
                        if (_complete == 0) {
                            $(pard_js.area).innerHTML = '<img src="/static/img/progress.gif">';
                        }
                    },
                    onComplete: function(resp)
                    {
                        //document.getElementById(pard_js.area).innerHTML = resp.responseText;
                        _complete = 1;
                        $(pard_js.area).innerHTML = resp.responseText;
                        $("field_" + pard_js.area).focus();
                        $("field_" + pard_js.area).select();
                    }
                }
            )
        }


        function row_delete(pard_js)
        {
            if (confirm("Confirm delete?"))
            {
                document.forms['query'].elements['action'].value = 'row_delete';
                $('tbl_del').value = pard_js.table_name;
                $('pk_del').value = Object.toJSON(pard_js.pk);
                document.forms['query'].submit();
            }
        }


        function prepare_insert(pard_js)
        {
            //table_name
            //pk: json object of row primary key
            params = "module=" + escape("%(module)s");
            params += "&table_name=" + escape(pard_js.table_name);
            params += "&action=prepare_insert";
            params += "&pk=" + escape(Object.toJSON(pard_js.pk));
            if ($('query2_display').value == 'N')
            {
                toggle_multi_area();
            }
            req = new Ajax.Request
            (
                "%(APPSERVER)sqq/%(ALIAS)s",
                {
                    method: 'post',
                    parameters: params,
                    onComplete: function(resp)
                    {
                        //$('query_area').value = $('query_area').value + '\\n' + resp.responseText;
                        $('query_area').value = resp.responseText;
                    }
                }
            )
        }

        </script>
        """
        % pard
    )
    return html


def ajax_js(pard):
    html = (
        """
        <script type="text/javascript" language="javascript" charset="utf-8">

        function close_window(window)
        {
            document.getElementById(window).innerHTML = '';
        }

        function restore_field(pard_js)
        {
            //area: name of the target box
            //table_name
            //field_name
            //pk: json object of row primary key
            params = "module=" + encodeURIComponent(pard_js.module);
            params += "&action=" + encodeURIComponent(pard_js.action);
            params += "&area=" + encodeURIComponent(pard_js.area);
            params += "&field_name=" + encodeURIComponent(pard_js.field_name);
            params += "&table_name=" + encodeURIComponent(pard_js.table_name);
            params += "&pk=" + encodeURIComponent(Object.toJSON(pard_js.pk));
            req = new Ajax.Request
            (
                pard_js.appserver,
                {
                    method: 'post',
                    parameters: params,
                    onLoading: function()
                    {
                        document.getElementById(pard_js.area).innerHTML = '<img src="/static/img/progress.gif">';
                    },
                    onComplete: function(resp)
                    {
                        document.getElementById(pard_js.area).innerHTML = resp.responseText;
                    }
                }
            )
        }

        function update_field(pard_js)
        {
            //area: name of the target box
            //table_name
            //field_name
            //pk: json object of row primary key
            params = "module=" + encodeURIComponent(pard_js.module);
            params += "&action=" + encodeURIComponent(pard_js.action);
            params += "&area=" + encodeURIComponent(pard_js.area);
            params += "&field_name=" + encodeURIComponent(pard_js.field_name);
            params += "&table_name=" + encodeURIComponent(pard_js.table_name);
            params += "&pk=" + encodeURIComponent(Object.toJSON(pard_js.pk));
            params += "&field_value=" + encodeURIComponent(document.getElementById("field_" + pard_js.area).value);
            //params += "&field_value=" + $("field_" + pard_js.area).value;
            req = new Ajax.Request
            (
                pard_js.appserver,
                {
                    method: 'post',
                    parameters: params,
                    onLoading: function()
                    {
                        document.getElementById(pard_js.area).innerHTML = '<img src="/static/img/progress.gif">';
                    },
                    onComplete: function(resp)
                    {
                        document.getElementById(pard_js.area).innerHTML = resp.responseText;
                    }
                }
            )
        }

        function getSelection(text_el)
        {
            if(!!document.selection)
              return document.selection.createRange().text;
            else if(text_el.setSelectionRange)
              return text_el.value.substring(text_el.selectionStart,text_el.selectionEnd);
            else
              return false;
          }

        function autocomplete_query(pard_js)
        {
            //id_textarea: id of the target box
            params = "module=" + escape(pard_js.module);
            params += "&action=" + escape(pard_js.action);
            params += "&field_value=" + escape($(pard_js.id_textarea).value);
            // obj.selectionEnd works only with Gecko;
            // with IE use document.selection
            params += "&field_pos=" + escape($(pard_js.id_textarea).selectionEnd);

            req = new Ajax.Request
            (
                pard_js.appserver,
                {
                    method: 'post',
                    parameters: params,
                    onLoading: function()
                    {
                        //$(pard_js.id_textarea).value = '    --> calculating... <--\\n\\n' + $(pard_js.id_textarea).value;
                        $('progress_span').innerHTML = '<img src="/static/img/progress.gif"> ...in progress...';
                    },
                    onComplete: function(resp)
                    {
                        var json = new Function('return ' + resp.responseText)();
                        $(pard_js.id_textarea).value = json.query;
                        $(pard_js.id_textarea).selectionStart = json.pos;
                        $(pard_js.id_textarea).selectionEnd = json.pos;
                        $('progress_span').innerHTML = '&nbsp;';
                    }
                }
            )
        }


        </script>
        """
        % pard
    )

    return html


def test_keydown():
    """
    DESCRIZIONE: Test per l'evento onKeyDown che permette di trappare i caratteri digitati
                 ed effettuare opportune operazioni di conseguenza.
                 Caso classico e' intercettare la pressione del "return" in un campo di input
                 ed effettuare tramite ajax il submit e l'aggiornamento di qualcosa.

    PARAMETRI:   event (fisso)

    RITORNO:     0          se NON e' un tasto che restituisce un charcode
                 charCode   diversamente

                 Vengono messe anche a disposizione una serie di costanti che estendono quelle
                 di prototype per trappare un particolare evento.
                     Event.KEY_BACKSPACE      "8"         backspace    --> da prototype
                     Event.KEY_TAB            "9"         tab          --> da prototype
                     Event.KEY_RETURN         "13"        return       --> da prototype
                     Event.KEY_SHIFT          "16"        shift
                     Event.KEY_CTRL           "17"        control
                     Event.KEY_ALT            "18"        alt
                     Event.KEY_ESC            "27"        escape       --> da prototype
                     Event.KEY_SPACE          "32"        space
                     Event.KEY_PAGEUP         "33"        page up      --> da prototype
                     Event.KEY_PAGEDOWN       "34"        page down    --> da prototype
                     Event.KEY_END            "35"        end          --> da prototype
                     Event.KEY_HOME           "36"        home         --> da prototype
                     Event.KEY_LEFT           "37"        left         --> da prototype
                     Event.KEY_UP             "38"        up           --> da prototype
                     Event.KEY_RIGHT          "39"        right        --> da prototype
                     Event.KEY_DOWN           "40"        down         --> da prototype
                     Event.KEY_INSERT         "45"        ins          --> da prototype
                     Event.KEY_DELETE         "46"        cancel       --> da prototype
                     Event.KEY_0              "48"        0 (zero)
                     Event.KEY_1              "49"        1
                     Event.KEY_2              "50"        2
                     Event.KEY_3              "51"        3
                     Event.KEY_4              "52"        4
                     Event.KEY_5              "53"        5
                     Event.KEY_6              "54"        6
                     Event.KEY_7              "55"        7
                     Event.KEY_8              "56"        8
                     Event.KEY_9              "57"        9
                     Event.KEY_F1             "112"       F1
                     Event.KEY_F2             "113"       F2
                     Event.KEY_F3             "114"       F3
                     Event.KEY_F4             "115"       F4
                     Event.KEY_F5             "116"       F5
                     Event.KEY_F6             "117"       F6
                     Event.KEY_F7             "118"       F7
                     Event.KEY_F8             "119"       F8
                     Event.KEY_F9             "120"       F9
                     Event.KEY_F10            "121"       F10
                     Event.KEY_F11            "122"       F11
                     Event.KEY_F12            "123"       F12
                     Event.KEY_COMMA          "188"       ,
                     Event.KEY_PERIOD         "190"       .
                     Event.KEY_BACKSLASH      "220"       \


    ESEMPIO DI CHIAMATA:
        pard['javascript'] += js_lib.test_keydown()

        <input type="text" id="codice" name="codice" value="codice"
               onKeyDown='if (test_keydown(event) == Event.KEY_RETURN) {aggiorna_qualcosa(%(pard_js)s); return false;};'>
    """

    html = """
    <script type="text/javascript" language="javascript" charset="utf-8">
        Event.KEY_SHIFT = 16;
        Event.KEY_CTRL = 17;
        Event.KEY_ALT = 18;
        Event.KEY_SPACE = 32;
        Event.KEY_0 = 48;
        Event.KEY_1 = 49;
        Event.KEY_2 = 50;
        Event.KEY_3 = 51;
        Event.KEY_4 = 52;
        Event.KEY_5 = 53;
        Event.KEY_6 = 54;
        Event.KEY_7 = 55;
        Event.KEY_8 = 56;
        Event.KEY_9 = 57;
        Event.KEY_F1 = 112;
        Event.KEY_F2 = 113;
        Event.KEY_F3 = 114;
        Event.KEY_F4 = 115;
        Event.KEY_F5 = 116;
        Event.KEY_F6 = 117;
        Event.KEY_F7 = 118;
        Event.KEY_F8 = 119;
        Event.KEY_F9 = 120;
        Event.KEY_F10 = 121;
        Event.KEY_F11 = 122;
        Event.KEY_F12 = 123;
        Event.KEY_COMMA = 188;
        Event.KEY_PERIOD = 190;
        Event.KEY_BACKSLASH = 220;

        function test_keydown(evt)
        {
            evt = (evt) ? evt : ((window.event) ? event : null);
            if (evt)
            {
                var charcode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
                if(charcode == 63232){
                    charcode = 38;
                }
                if(charcode == 63233){
                    charcode = 40;
                }
                return charcode;
            }
            return 0;
        }
    </script>
    """
    return html


if __name__ == "__main__":
    from . import config

    pard = config.global_parameters({})
    pard["HOST"] = "localhost"
    pard["USER"] = "devel"
    pard["DB"] = "devel"
    # tools.dump(get_query_history(pard))
    # print js_navhistory(pard)
