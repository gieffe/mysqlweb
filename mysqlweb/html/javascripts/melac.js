const copy_to_clipboard = function (ele_id, msg_id) {
    var copyText = document.getElementById(ele_id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    navigator.clipboard.writeText(copyText.value);
    if (msg_id) {
        var resultMsg = document.getElementById(msg_id);
        resultMsg.innerHTML = "Copied to Clipboard";
        setTimeout(function(){
            resultMsg.innerHTML = '';
        }, 3000);
    };
}