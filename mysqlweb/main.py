import wsgian

from . import config
from . import tunnel
from . import qlite

## Config
from wsgian import cfg

pard = cfg.default_configuration()
pard = config.global_parameters(pard)

pard["CGI_ESCAPE"] = False
pard["AUTH_MODULE"] = "mysqlweb.users"
pard["LOGIN_MODULE"] = "mysqlweb.login"
pard["LOGIN_URL"] = "/login"
pard["LOGOUT_URL"] = "/logout"
pard["SIGNUP_URL"] = "/signup"

## URL
urls = [
    {"pattern": "/", "module": "mysqlweb.home"},
    {
        "pattern": "/not_authorized",
        "module": "mysqlweb.home",
        "action": "not_authorized",
    },
    {"pattern": "/prefs", "module": "mysqlweb.prefs", "login": True},
    {"pattern": "/tunnel", "module": "mysqlweb.tunnel_prefs", "login": "admin"},
    {"pattern": "/users", "module": "mysqlweb.users_prefs", "login": "admin"},
    {"pattern": "/qq/{alias}", "module": "mysqlweb.qq", "login": True},
    {"pattern": "/db/{alias}", "module": "mysqlweb.db_structure", "login": True},
    {"pattern": "/cv/{alias}", "module": "mysqlweb.cv", "login": True},
    {
        "pattern": "/cv/{alias}/filter_result",
        "module": "mysqlweb.cv",
        "action": "filter_result",
        "login": True,
    },
    {
        "pattern": "/cv/{alias}/display_sql",
        "module": "mysqlweb.cv",
        "action": "display_sql",
        "login": True,
    },
    {
        "pattern": "/cv/{alias}/{table_name}/{field_name}/update",
        "module": "mysqlweb.cv",
        "action": "update_field",
        "login": True,
    },
    {
        "pattern": "/cv/{alias}/{table_name}/delete_record",
        "module": "mysqlweb.cv",
        "action": "delete_record",
        "login": True,
    },
    {"pattern": "/login", "module": "mysqlweb.login"},
    {"pattern": "/logout", "module": "mysqlweb.login", "action": "logout"},
]

## The App
app = wsgian.App(urls, pard)


## Start and Stop functions
def on_start(pard):
    # config.check_dir_tree(pard)
    config.check_db(pard)
    pard["sqlite.conn"] = qlite.DB(pard["SQLITE_DB"])
    tunnel.close_all_open_tunnel(pard)
    pard["sqlite.conn"].close()


def on_stop(pard):
    pard["sqlite.conn"] = qlite.DB(pard["SQLITE_DB"])
    tunnel.close_all_open_tunnel(pard)
    pard["sqlite.conn"].close()


## Run (for test only)
def run():
    try:
        on_start(pard)
        wsgian.quickstart(app, pard["HTTP_ADDRESS"], pard["HTTP_PORT"])
    except KeyboardInterrupt:
        on_stop(pard)
        print("Esco")


# ----------------------------------------------------------------------------
if __name__ == "__main__":
    run()
