from html import escape
import time

from . import home
from . import tunnel
from . import tools


def main(pard):
    pard["action"] = pard.get("action", "start")
    pard["page_header"] = pard["TITLE"] + " - Tunnel Preferences"
    # pard['css'] = css(pard)
    pard["javascript"] = javascript(pard)
    pard["sidebar"] = ""
    pard["main_body"] = ""
    pard["msg"] = ""
    pard["error_msg"] = ""

    pard.setdefault("REMOTE_IP_ADDR", "localhost")
    pard.setdefault("mw_user", pard["REMOTE_IP_ADDR"])

    pard["userbar"] = home.render_userbar(pard)
    # --------------------------- start ----------------------------
    if pard["action"] == "start":
        result = tunnel.get_list(pard)
        pard["main_body"] = render_list(pard, result)

    # --------------------------- insert ----------------------------
    elif pard["action"] == "insert":
        rec = get_form(pard)
        pard["main_body"] = render_form(pard, rec)

    # --------------------------- edit ----------------------------
    elif pard["action"] == "edit":
        rec = tunnel.get_by_id(pard, {"id": pard["id"]})
        pard["main_body"] = render_form(pard, rec)

    # --------------------------- save_insert ----------------------------
    elif pard["action"] in ("save_insert", "save_update"):
        rec = get_form(pard)
        try:
            if pard["action"] == "save_insert":
                rec["id"] = tunnel.insert(pard, rec)
            else:
                tunnel.update(pard, rec)
            pard["msg"] = render_msg(
                pard, "`%(id)s - %(name)s` preferences saved successfully" % rec, "msg"
            )
            result = tunnel.get_list(pard, {"mw_user": pard["mw_user"]})
            pard["main_body"] = render_list(pard, result)
        except tunnel.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
            pard["main_body"] = render_form(pard, rec)

    # --------------------------- delete ----------------------------
    elif pard["action"] == "delete":
        rec = tunnel.get_by_id(pard, {"id": pard["id"]})
        if not rec:
            pard["msg"] = render_msg(pard, "Tunnel not found", "err")
        elif rec["status"] == "Connected":
            pard["msg"] = render_msg(pard, "Can't delete `Connected` Tunnel", "err")
        else:
            tunnel.delete(pard, {"id": rec["id"], "mw_user": pard["mw_user"]})
            pard["msg"] = render_msg(
                pard, "`%(id)s - %(name)s` deleted successfully" % rec, "msg"
            )
        result = tunnel.get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    # --------------------------- close ----------------------------
    elif pard["action"] == "close":
        try:
            tunnel.close_tunnel(pard, {"id": pard["id"]})
            time.sleep(1)
            pard["msg"] = render_msg(
                pard, "Tunnel `%(id)s` Closed successfully" % pard, "msg"
            )
        except tunnel.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
        result = tunnel.get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    # --------------------------- open ----------------------------
    elif pard["action"] == "open":
        try:
            tunnel.open_tunnel(pard, {"id": pard["id"]})
            pard["msg"] = render_msg(
                pard, "Tunnel `%(id)s` successfully open" % pard, "msg"
            )
        except tunnel.Error as err:
            pard["msg"] = render_msg(pard, str(err), "err")
        result = tunnel.get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    # ------------------------------------------------------------------- Log
    elif pard["action"] == "log":
        result = tunnel.get_log(pard, {"id": pard["id"]})
        btn_exit = """<input type="button" name="submit_form" value="Exit" class="btn" onclick="action.value='start'; form.submit();">"""  # noqa: E501
        pard[
            "main_body"
        ] = f"""
            <div class="titolo" style="text-align: left;">
                {pard["name"]}\'s tunnel log file
            </div>
            <pre style="text-align: left;">{result}</pre>
            <div style="text-align: left;">{btn_exit}</div>
            """

    # -------------------------------------------------------------- Download
    elif pard["action"] == "download":
        pard["html"] = tunnel.export_to_csv(pard)
        pard["header"] = [
            ("Content-type", "text/csv"),
            ("Content-Disposition", "attachment;filename=tunnel_preferences.csv"),
        ]
        return pard

    # ------------------------------------------------------- Load Preferences
    elif pard["action"] == "load_prefs":
        if "file_uploadd" in pard and pard["file_uploadd"].file:
            buf_list = pard["file_uploadd"].file.readlines()
            buf = b"".join(buf_list)
            buf = buf.decode("utf-8")
            try:
                result = tunnel.import_from_csv(pard, buf)
                pard["msg"] = render_msg(pard, result, "msg")
            except tunnel.Error as err:
                pard["msg"] = render_msg(pard, str(err), "err")
        result = tunnel.get_list(pard, {"mw_user": pard["mw_user"]})
        pard["main_body"] = render_list(pard, result)

    else:
        pard["msg"] = 'Function not available. ("%(action)s")' % pard
        pard["main_body"] = (
            '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'  # noqa: E501
        )

    pard["html"] = render_page(pard)
    return pard


def render_form(pard, rec):
    h = []
    if "id" in rec and rec["id"]:
        h.append('<input type="hidden" name="id" id="id" value="%(id)s">' % rec)

    h.append('<input type="hidden" name="status" value="%(status)s">' % rec)
    h.append('<input type="hidden" name="pid" value="%(pid)s">' % rec)
    h.append('<input type="hidden" name="cmdline" value="%(cmdline)s">' % rec)

    h.append('<table width="100%">')
    h.append("<tr>")
    if pard["action"] in ("insert", "save_insert"):
        rec["v_action"] = "New"
    else:
        rec["v_action"] = "Edit"
    h.append('<th colspan="2">%(v_action)s SSH Tunnel</th>' % rec)
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td width="150px"><b>Name</b></td>')
    h.append(
        '<td><input type="text" name="name" value="%(name)s" style="width: 98%%;">'
        % rec
    )
    h.append(
        '<div class="help_text">The Name of the tunnel. A name of your choice.</div>'
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>SSH Server</b></td>')
    h.append(
        '<td><input type="text" name="ssh_server" value="%(ssh_server)s" style="width: 98%%;">'  # noqa: E501
        % rec
    )
    h.append(
        """<div class="help_text">
                The name or ip address of the ssh server
                that has the capabilities to access mysql.<br>
                EG: mysql.mydomain.com or 192.168.111.57.<br><br>
                If the SSH server does not respond to the default port (22),
                specify the address in the form "name:port" or "address:port".<br>
                EG: mysql.mydomain.com:2222
                </div>"""
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>SSH User</b></td>')
    h.append(
        '<td><input type="text" name="ssh_user" value="%(ssh_user)s" style="width: 98%%;">'  # noqa: E501
        % rec
    )
    h.append('<div class="help_text">The user enabled to access the SSH Server</div>')
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>SSH Pkey Password</b></td>')
    h.append(
        '<td><input type="password" name="ssh_pkey_password" value="%(ssh_pkey_password)s" style="width: 98%%;">'  # noqa: E501
        % rec
    )
    h.append(
        """<div class="help_text">
                An optional password to use to decrypt ssh key, if it is encrypted.<br>
                This method is not recommended because the specified password
                will be recorded in the mysqlweb preferences and an attacker
                could have access to it.<br>
                It is preferable to use SSH Agent (See SSH Allow Agent)
                </div>"""
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>SSH Allow Agent</b></td>')
    h.append("<td>")
    if rec["ssh_allow_agent"] == "1":
        rec["yes"] = "checked"
        rec["no"] = ""
    else:
        rec["yes"] = ""
        rec["no"] = "checked"
    h.append(
        '<input type="radio" id="agent_allow" name="ssh_allow_agent" value="1" %(yes)s>'
        % rec
    )
    h.append('<label for="agent_allow">Yes</label>&nbsp;&nbsp;')
    h.append(
        '<input type="radio" id="agent_not_allow" name="ssh_allow_agent" value="" %(no)s>'  # noqa: E501
        % rec
    )
    h.append('<label for="agent_not_allow">No</label>&nbsp;&nbsp;')
    h.append(
        """<div class="help_text">
                MySQLweb can use private keys from an SSH agent
                running on the local machine.
                If an SSH agent is running, can be used to retrieve PKey and use
                it to authenticate to remote SSH servers.<br>
                To initialize a session with the local machine,
                simply connect at least once to the SSH server
                from the command line after the local machine is powered on.
                </div>"""
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Data Base Host</b></td>')
    h.append(
        '<td><input type="text" name="db_host" value="%(db_host)s" style="width: 98%%;">'  # noqa: E501
        % rec
    )
    h.append(
        """<div class="help_text">
                The name or IP address of mysql server.<br>
                If mysql is on the same machine of SSH server you can set this
                field to "localhost" or "127.0.0.1".<br>
                If mysql server is configured to serve on a port different from "3306",
                specify the address in the form "address:port".<br>
                EG: 127.0.0.1:6449
                </div>"""
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Local Bind Port</b></td>')
    h.append(
        """
        <td>
          <input type="text" name="local_bind_port"
                 value="%(local_bind_port)s" style="width: 98%%;"
          />
          <div class="help_text">
            It is the port on the local machine on which the connection
            to the ssh server is established.
            It can be a number greater than 5000 that is not being used
            by another tunnel or TCP/IP service that is concurrently running.<br>
            EG: 6449
          </div>
        </td>
        """
        % rec
    )
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Status</b></td>')
    h.append("<td>%(status)s" % rec)
    h.append('<div class="help_text"></div>')
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Pid</b></td>')
    h.append("<td>%(pid)s" % rec)
    h.append(
        '<div class="help_text">If the status is "Connected" contain the process id (pid) of the tunnel on the local machine</div>'  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append('<td class="align_top"><b>Tunnel Command Line</b></td>')
    h.append("<td>%(cmdline)s" % rec)
    h.append(
        '<div class="help_text">If status is "Connected" contain the command line used to establish connection to ssh server and open the port forwarding</div>'  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")

    ## Bottom Action
    h.append('<tr><td colspan="2"></td></tr>')
    h.append("<tr>")

    ## Delete Button
    if pard["action"] in ("insert", "save_insert"):
        h.append("<td>&nbsp;</td>")
    else:
        h.append(
            """<td><input type="button" name="submit_form" value="Delete" class="btn" onclick="confirm_action('delete');"></td>"""  # noqa: E501
        )

    ## Save Button
    h.append('<td align="right">')
    h.append(
        """<input type="button" name="submit_form" value="Cancel" class="btn" onclick="action.value='start'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
    )
    if pard["action"] in ("insert", "save_insert"):
        rec["action"] = "save_insert"
    else:
        rec["action"] = "save_update"
    h.append(
        """<input type="submit" name="submit_form" value="Save" class="btn" onclick="action.value='%(action)s'; form.submit();">&nbsp;&nbsp;&nbsp;&nbsp;"""  # noqa: E501
        % rec
    )
    h.append("</td>")

    h.append("</tr>")
    ## -------------

    h.append("</table>")
    return "\n".join(h)


def render_list(pard, result):
    h = []
    h.append('<table width="100%">')
    h.append("<tr>")
    h.append('<td align="right" colspan="10">')
    h.append('<span style="float: left;">')
    alias = pard.get("alias", "")
    h.append(
        f"""<input type="button" value="Exit" class="btn" onclick="location.href='/prefs?alias={alias}';">"""  # noqa: E501
    )
    h.append("&nbsp;" * 3)
    h.append(
        """
        <input type="button" value="Upload Preferences" class="btn" onclick="show_load_form();">
        &nbsp;&nbsp;&nbsp;
        <div id="load_form" class="html_title" style="display: none; padding: 6px; border: 2px solid #F0F0F0;">
        <table style="width: 100%;">
        <tr>
            <th>
                <img src="/static/img/uncheck.png" border="0" onClick="hide_load_form();">
                &nbsp;&nbsp;&nbsp;Load preferences from local file
            </th>
        </tr>
        <tr>
            <td>
                <input type="file" name="file_uploadd" accept="text/csv" size="30" />
                <input type="button" name="submit_form" value="Load" onclick="action.value='load_prefs'; form.submit();" />
            </td>
        </tr>
        </table>
        </div>
        """  # noqa: E501
    )

    h.append(
        """<input type="button" name="submit_form" value="Download Tunnel Preferences" class="btn" onclick="action.value='download'; form.submit();">&nbsp;&nbsp;&nbsp;"""  # noqa: E501
    )

    h.append("</span>")

    h.append(
        """<b><input type="button" name="submit_form" value="New" class="btn" onclick="action.value='insert'; form.submit();"></b>"""  # noqa: E501
    )
    h.append("</td>")
    h.append("</tr>")

    h.append("<tr>")
    h.append("<th>&nbsp;</th>")
    h.append("<th>Name</th>")
    h.append("<th>SSH Server</th>")
    h.append("<th>SSH User</th>")
    h.append("<th>SSH Pkey Password</th>")
    h.append("<th>SSH Allow Agent</th>")
    h.append("<th>Data Base Host</th>")
    h.append("<th>Local Bind Port</th>")
    h.append("<th>Status</th>")
    h.append("<th>pid</th>")
    h.append("</tr>")

    for rec in result:
        h.append("<tr>")

        rec[
            "link_edit"
        ] = f"""
            <a href="{pard['APPSERVER']}tunnel?action=edit&id={rec['id']}"
            title="Edit">
            <img src="/static/icons/application_form_edit.png" border="0"></a>
            """

        rec[
            "link_log"
        ] = f"""
            <a href="{pard['APPSERVER']}tunnel?action=log&id={rec['id']}&name={rec['name']}"
            title="View Log File">
            <img src="/static/icons/script.png" border="0"></a>
            """  # noqa: E501

        if rec["status"] == "Connected":
            rec[
                "link_open_close"
            ] = f"""
                <a href="{pard['APPSERVER']}tunnel?action=close&id={rec['id']}"
                title="Close Tunnel">
                <img src="/static/icons/disconnect.png" border="0"></a>
                """
        else:
            rec[
                "link_open_close"
            ] = f"""
                <a href="{pard['APPSERVER']}tunnel?action=open&id={rec['id']}"
                title="Open Tunnel">
                <img src="/static/icons/connect.png" border="0"></a>
                """

        h.append(
            '<td align="center">%(link_edit)s&nbsp;&nbsp;&nbsp;%(link_open_close)s&nbsp;&nbsp;&nbsp;%(link_log)s</td>'  # noqa: E501
            % rec
        )
        h.append("<td>%(id)s - %(name)s</td>" % rec)
        h.append("<td>%(ssh_server)s</td>" % rec)
        h.append("<td>%(ssh_user)s</td>" % rec)
        if rec["ssh_pkey_password"]:
            rec["v_passw"] = "*" * len(rec["ssh_pkey_password"])
        else:
            rec["v_passw"] = "&nbsp;"
        h.append("<td>%(v_passw)s</td>" % rec)
        h.append("<td>%(ssh_allow_agent)s</td>" % rec)
        h.append("<td>%(db_host)s</td>" % rec)
        h.append("<td>%(local_bind_port)s</td>" % rec)
        h.append("<td>%(status)s</td>" % rec)
        rec["cmdline"] = escape(rec["cmdline"])
        h.append('<td title="%(cmdline)s">%(pid)s</td>' % rec)
        h.append("</tr>")

    h.append("</table>")
    return "\n".join(h)


def get_form(pard):
    rec = {}
    rec["id"] = pard.get("id", "")
    rec["name"] = pard.get("name", "")
    if not rec["name"]:
        rec["name"] = tools.inventa_nome()
    rec["ssh_server"] = pard.get("ssh_server", "")
    rec["ssh_user"] = pard.get("ssh_user", "")
    rec["ssh_pkey_password"] = pard.get("ssh_pkey_password", "")
    rec["ssh_allow_agent"] = pard.get("ssh_allow_agent", "1")
    rec["db_host"] = pard.get("db_host", "")
    rec["local_bind_port"] = pard.get("local_bind_port", "")
    rec["status"] = pard.get("status", "")
    rec["pid"] = pard.get("pid", "")
    rec["cmdline"] = pard.get("cmdline", "")
    return rec


def render_msg(pard, msg, msg_class="err"):
    if isinstance(msg, str):
        msg = [msg]
    h = []
    for s in msg:
        h.append(f'<p class="{msg_class}">{s}</p>')
    return "\n".join(h)


def javascript(pard):
    javascript = """
    <script>

        let xhr;

        const confirm_action = function (action) {
            if (confirm('Confirm ' + action + '?')) {
                the_form = document.getElementById('theForm');
                the_action = document.getElementById('action');
                the_action.value = action;
                the_form.submit();
                }
        };

        const show_load_form = function () {
            var x = document.getElementById('load_form');
            x.style.display = "block";
        };

        const hide_load_form = function () {
            var x = document.getElementById('load_form');
            x.style.display = "none";
        };

    </script>
    """
    return javascript


def render_page(pard):
    html = (
        """
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="author" content="gianfranco" />
    <title>%(TITLE)s</title>

%(CSS)s
<link rel="icon" href="/static/favicon.ico">
</head>
<body>

<!-- header -->
<div id="userbar">%(userbar)s</div>
<div id="header">%(page_header)s</div>

<!-- Sidebar -->
<div id="sidebar" style="display: none;">
%(sidebar)s
</div>

<!-- Main Body -->
<div id="main_body" align="center">
<form action="%(APPSERVER)stunnel"
      method="post" name="theForm" id="theForm"
      autocomplete="off" spellcheck="false" enctype="multipart/form-data">
<!-- enctype="multipart/form-data" -->
<input type="hidden" id="action" name="action" value="%(action)s">
<input type="hidden" id="module" name="module" value="%(module)s">
%(msg)s
%(main_body)s
</form>
</div>

</body>
%(javascript)s
</html>
    """
        % pard
    )
    return html
